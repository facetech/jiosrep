﻿using IJOS.Application.Common.Exceptions;
using IJOS.Application.TodoItems.Commands.CreateTodoItem;
using IJOS.Application.TodoItems.Commands.UpdateTodoItem;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Domain.Entities;
using FluentAssertions;
using System.Threading.Tasks;
using NUnit.Framework;
using System;

namespace IJOS.Application.IntegrationTests.TodoItems.Commands
{
    using static Testing;

    public class UpdateTodoItemTests : TestBase
    {
        [Test]
        public void ShouldRequireValidTodoItemId()
        {
            var command = new UpdateTodoItemCommand
            {
                Id = 99,
                Title = "New Title"
            };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<NotFoundException>();
        }

        [Test]
        public async Task ShouldUpdateTodoItem()
        {
            var userId = await RunAsDefaultUserAsync();

            var listId = await SendAsync(new CreateTodoListCommand
            {
                Title = "New List"
            });

            var itemId = await SendAsync(new CreateTodoItemCommand
            {
                ListId = listId,
                Title = "New Item"
            });

            var command = new UpdateTodoItemCommand
            {
                Id = itemId,
                Title = "Updated Item Title"
            };

            await SendAsync(command);

            var item = await FindAsync<TodoItem>(itemId);

            item.Title.Should().Be(command.Title);
            item.Modified_by.Should().NotBeNull();
            item.Modified_by.Should().Be(userId);
            item.Modified_date.Should().NotBeNull();
            item.Modified_date.Should().BeCloseTo(DateTime.Now, 1000);
        }
    }
}
