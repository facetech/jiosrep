﻿using IJOS.Application.Common.Exceptions;
using IJOS.Application.TodoItems.Commands.CreateTodoItem;
using IJOS.Application.TodoItems.Commands.UpdateTodoItem;
using IJOS.Application.TodoItems.Commands.UpdateTodoItemDetail;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Domain.Entities;
using IJOS.Domain.Enums;
using FluentAssertions;
using System.Threading.Tasks;
using NUnit.Framework;
using System;

namespace IJOS.Application.IntegrationTests.TodoItems.Commands
{
    using static Testing;

    public class UpdateTodoItemDetailTests : TestBase
    {
        [Test]
        public void ShouldRequireValidTodoItemId()
        {
            var command = new UpdateTodoItemCommand
            {
                Id = 99,
                Title = "New Title"
            };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<NotFoundException>();
        }

        [Test]
        public async Task ShouldUpdateTodoItem()
        {
            var userId = await RunAsDefaultUserAsync();

            var listId = await SendAsync(new CreateTodoListCommand
            {
                Title = "New List"
            });

            var itemId = await SendAsync(new CreateTodoItemCommand
            {
                ListId = listId,
                Title = "New Item"
            });

            var command = new UpdateTodoItemDetailCommand
            {
                Id = itemId,
                ListId = listId,
                Note = "This is the note.",
                Priority = PriorityLevel.High
            };

            await SendAsync(command);

            var item = await FindAsync<TodoItem>(itemId);

            item.ListId.Should().Be(command.ListId);
            item.Note.Should().Be(command.Note);
            item.Priority.Should().Be(command.Priority);
            item.Modified_by.Should().NotBeNull();
            item.Modified_by.Should().Be(userId);
            item.Modified_date.Should().NotBeNull();
            item.Modified_date.Should().BeCloseTo(DateTime.Now, 10000);
        }
    }
}
