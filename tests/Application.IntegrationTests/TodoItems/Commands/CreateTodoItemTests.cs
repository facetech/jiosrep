﻿using IJOS.Application.Common.Exceptions;
using IJOS.Application.TodoItems.Commands.CreateTodoItem;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Domain.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace IJOS.Application.IntegrationTests.TodoItems.Commands
{
    using static Testing;

    public class CreateTodoItemTests : TestBase
    {
        [Test]
        public void ShouldRequireMinimumFields()
        {
            var command = new CreateTodoItemCommand();

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<ValidationException>();
        }

        [Test]
        public async Task ShouldCreateTodoItem()
        {
            var userId = await RunAsDefaultUserAsync();

            var listId = await SendAsync(new CreateTodoListCommand
            {
                Title = "New List"
            });

            var command = new CreateTodoItemCommand
            {
                ListId = listId,
                Title = "Tasks"
            };

            var itemId = await SendAsync(command);

            var item = await FindAsync<TodoItem>(itemId);

            item.Should().NotBeNull();
            item.ListId.Should().Be(command.ListId);
            item.Title.Should().Be(command.Title);
            item.Created_by.Should().Be(userId);
            item.Created_date.Should().BeCloseTo(DateTime.Now, 10000);
            item.Modified_by.Should().BeNull();
            item.Modified_by.Should().BeNull();
        }
    }
}
