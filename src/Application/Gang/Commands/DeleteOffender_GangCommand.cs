﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.Gang.Commands
{
    public class DeleteOffender_GangCommand : IRequest
    {
        public long Offender_Gang_Id { get; set; }
    }

    public class DeleteOffender_GangCommandHandler : IRequestHandler<DeleteOffender_GangCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DeleteOffender_GangCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteOffender_GangCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string sql_Offender_Gang = @" SELECT * FROM OFFENDER_GANG WHERE OFFENDER_GANG_ID = @Offender_Gang_Id";
                e.Offender_Gang entity_Offender_Gang = await _unitOfWork.Offender_GangRepository.QuerySingleAsync
                    (sql_Offender_Gang, new { Offender_Gang_Id = (long?)request.Offender_Gang_Id });
                _unitOfWork.Offender_GangRepository.Delete(entity_Offender_Gang);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
