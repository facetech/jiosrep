﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.Gang.Queries.GetGangs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;


namespace IJOS.Application.Gang.Commands
{
    public class CreateOffender_GangCommand : IRequest
    {
        public OffenderGangVM OffenderGangVM { get; set; }
    }

    public class CreateOffender_GangCommandHandler : IRequestHandler<CreateOffender_GangCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateOffender_GangCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreateOffender_GangCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.OffenderGangVM.Offender_GangDto.Created_date = System.DateTime.Now;
                var entityDbo_Offender_Gang = request.OffenderGangVM.Offender_GangDto;
                e.Offender_Gang entity_Offender_Gang = _mapper.Map<e.Offender_Gang>(entityDbo_Offender_Gang);
                await _unitOfWork.Offender_GangRepository.Insert(entity_Offender_Gang, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }


    }
}
