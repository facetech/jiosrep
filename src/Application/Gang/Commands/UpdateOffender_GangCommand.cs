﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using e = IJOS.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;
using System;
using IJOS.Application.Gang.Queries.GetGangs;

namespace IJOS.Application.Gang.Commands
{
    public class UpdateOffender_GangCommand : IRequest
    {
        public OffenderGangVM OffenderGangsVM { get; set; }
    }

    public class UpdateOffender_GangCommandHandler : IRequestHandler<UpdateOffender_GangCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateOffender_GangCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffender_GangCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.OffenderGangsVM.Offender_GangDto.Modified_date = DateTime.Now;
                var entityDbo_Offender_GangDto = request.OffenderGangsVM.Offender_GangDto;
                e.Offender_Gang entity_Offender_GangDto = _mapper.Map<e.Offender_Gang>(entityDbo_Offender_GangDto);
                await _unitOfWork.Offender_GangRepository.Update(entity_Offender_GangDto, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }
    }
}
