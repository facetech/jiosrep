﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.Gang.Queries.GetGangs
{
    public class Offender_GangDto : IMapFrom<e.Offender_Gang>
    {
        public long Offender_gang_id { get; set; }
        public long Sin { get; set; }
        public long Gang_number { get; set; }
        public DateTime? Join_date { get; set; }
        public DateTime? Quit_date { get; set; }
        public string Status { get; set; }
        public bool? Uses_gang_signs { get; set; }
        public string Initiated_by { get; set; }
        public string Rank { get; set; }
        public string Moniker { get; set; }

        public long Gang_name { get; set; }

        public bool? Is_Join_date { get; set; }
        public bool? Is_Quit_date { get; set; }

        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    }
}
