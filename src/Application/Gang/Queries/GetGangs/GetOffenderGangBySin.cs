﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Gang.Queries.GetGangs
{
    public class GetOffenderGangBySin : IRequest<OffenderGangVM>
    {
        public int Sin { get; set; }
        public int Gang_Number { get; set; }
    }

    public class GetOffenderGangBySinHandler : IRequestHandler<GetOffenderGangBySin, OffenderGangVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetOffenderGangBySinHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderGangVM> Handle(GetOffenderGangBySin request, CancellationToken cancellationToken)
        {
            try
            {
                List<GangOffenderDto> gangOffenderDtoList = new List<GangOffenderDto>();
                string sql_gangOffender = @" SELECT OG.*, G.GANG_NAME FROM OFFENDER_GANG OG, GANG G WHERE OG.GANG_NUMBER = G.GANG_NUMBER AND OG.SIN = @sin";
                var dbResult_sql_gangOffender = await _unitOfWork.GangOffenderRepository.QueryAsync
                    (sql_gangOffender, new { sin = (long?)request.Sin });
                gangOffenderDtoList = _mapper.Map<List<GangOffenderDto>>(dbResult_sql_gangOffender);

                return new OffenderGangVM
                {
                    GangOffenderDtoList = gangOffenderDtoList,
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
