﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.TodoLists.Queries.GetTodos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Gang.Queries.GetGangs
{
    public class GetOffenderGangByGangNumber : IRequest<OffenderGangVM>
    {
        public long Offender_Gang_Id { get; set; }
        public int Sin { get; set; }
    }

    public class GetOffenderGangByGangNumberHandler : IRequestHandler<GetOffenderGangByGangNumber, OffenderGangVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetOffenderGangByGangNumberHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderGangVM> Handle(GetOffenderGangByGangNumber request, CancellationToken cancellationToken)
        {
            try
            {
                GangDto gangDto = new GangDto();
                Offender_GangDto offender_GangDto = new Offender_GangDto();
                GangOffenderDto gangOffenderDto = new GangOffenderDto();

                List<GangDto> gangDtoList = new List<GangDto>();
                List<Offender_GangDto> offender_GangDtoList = new List<Offender_GangDto>();
                List<GangOffenderDto> gangOffenderDtoList = new List<GangOffenderDto>();
                List<Master_ColumnDto> master_ColumnDtoList = new List<Master_ColumnDto>();

                string sql_Offender = @"SELECT * FROM OFFENDER WHERE SIN =@sin";
                var dbResult_Offender = await _unitOfWork.OffenderRepository.
                    QuerySingleAsync(sql_Offender, new { sin = (long?)request.Sin });
                
                string sql_GangDtoList = @" SELECT * from GANG";
                var dbResults_GangDtoList = await _unitOfWork.GangRepository.QueryAsync(sql_GangDtoList);
                gangDtoList = _mapper.Map<List<GangDto>>(dbResults_GangDtoList);

                string sql_Master_ColumnDtoList = "select * from Master_Column WHERE Rank_OffenderGang !='' OR InitiatedBy_OffenderGang !='' OR Status_OffenderGang !=''";
                var dbResult_sql_Master_ColumnDtoList = await _unitOfWork.Master_ColumnRepository.QueryAsync(sql_Master_ColumnDtoList);
                master_ColumnDtoList = _mapper.Map<List<Master_ColumnDto>>(dbResult_sql_Master_ColumnDtoList);

                if (request.Offender_Gang_Id == 0)
                {
                    offender_GangDto.Sin = request.Sin;
                }
                else
                {
                    string sql_gangOffender = @" SELECT OG.*, G.GANG_NAME FROM OFFENDER_GANG OG, GANG G WHERE OG.GANG_NUMBER = G.GANG_NUMBER AND OG.OFFENDER_GANG_ID = @offender_gang_id";
                    var dbResult_sql_gangOffender = await _unitOfWork.GangOffenderRepository.
                        QuerySingleAsync(sql_gangOffender, new { offender_gang_id = (long?)request.Offender_Gang_Id });
                    gangOffenderDto = _mapper.Map<GangOffenderDto>(dbResult_sql_gangOffender);

                    string sql_offender_Gang = @" SELECT * FROM OFFENDER_GANG WHERE OFFENDER_GANG_ID = @offender_gang_id";
                    var dbResult_sql_offender_Gang = await _unitOfWork.Offender_GangRepository.
                        QuerySingleAsync(sql_offender_Gang, new { offender_gang_id = (long?)request.Offender_Gang_Id });
                    offender_GangDto = _mapper.Map<Offender_GangDto>(dbResult_sql_offender_Gang);

                    string sql_gang = @" SELECT * FROM GANG WHERE GANG_NUMBER = @gang_number";
                    var dbResult_sql_gang = await _unitOfWork.GangRepository.
                        QuerySingleAsync(sql_gang, new { gang_number = (long?)offender_GangDto.Gang_number });
                    gangDto = _mapper.Map<GangDto>(dbResult_sql_gang);
                }

                if (offender_GangDto.Join_date == null)
                    offender_GangDto.Is_Join_date = true;
                else
                    offender_GangDto.Is_Join_date = false;

                if (offender_GangDto.Quit_date == null)
                    offender_GangDto.Is_Quit_date = true;
                else
                    offender_GangDto.Is_Quit_date = false;


                return new OffenderGangVM
                {
                    Offender_FullName = dbResult_Offender.First_name + " " + " " + dbResult_Offender.Last_name,
                    InitiatedByList = master_ColumnDtoList.Select(o => o.Initiatedby_offendergang).Distinct().ToList(),
                    RankList = master_ColumnDtoList.Select(o => o.Rank_offendergang).Distinct().ToList(),
                    StatusList = master_ColumnDtoList.Select(o => o.Status_offendergang).Distinct().ToList(),
                    Offender_GangDto = offender_GangDto,
                    GangDtoList = gangDtoList,
                    GangDto = gangDto,
                    GangOffenderDto = gangOffenderDto,                 
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
