﻿using IJOS.Application.School_History.Queries.GetSchool_History;
using System.Collections.Generic;

namespace IJOS.Application.Gang.Queries.GetGangs
{
    public class OffenderGangVM
    {
        public Offender_GangDto Offender_GangDto { get; set; }
        public List<Offender_GangDto> Offender_GangDtoList { get; set; }


        public GangOffenderDto GangOffenderDto { get; set; }
        public List<GangOffenderDto> GangOffenderDtoList { get; set; }


        public GangDto GangDto { get; set; }
        public List<GangDto> GangDtoList { get; set; }


        public string Offender_FullName { get; set; }


        public List<string> RankList { get; set; }


        public List<string> InitiatedByList { get; set; }
        public List<string> StatusList { get; set; }





    }
}
