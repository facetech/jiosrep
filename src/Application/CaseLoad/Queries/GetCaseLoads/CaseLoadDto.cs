﻿using IJOS.Application.Common.Mappings;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application.CaseLoad.Queries.GetCaseLoads
{
    public class CaseLoadDto : IMapFrom<Case_load>
    {
        public long Case_load_number { get; set; }
        public DateTime Starting_date { get; set; }
        public DateTime? Ending_date { get; set; }
        public long Staff_key { get; set; }
        public string Agency_branch { get; set; }
        public string Agency_name { get; set; }
        public string Race { get; set; }
        public string Sex { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Name
        {
            get 
            {
               return !string.IsNullOrEmpty(Last_name) && !string.IsNullOrEmpty(Last_name) ? 
                    $"{ Last_name}, {First_name}" : string.Empty;
            }
        }

        public DateTime Date_of_birth { get; set; }
        public string Probation_status { get; set; }
        public long Sin { get; set; }
    }
}
