﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application.CaseLoad.Queries.GetCaseLoads
{
    public class CaseLoadVM
    {
        public List<CaseLoadDto> CaseLoads { get; set; }
    }
}
