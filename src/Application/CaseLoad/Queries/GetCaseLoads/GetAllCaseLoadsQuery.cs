﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.CaseLoad.Queries.GetCaseLoads
{
    public class GetAllCaseLoadsQuery : IRequest<CaseLoadVM>
    {
    }

    public class GetAllCaseLoadsHandler : IRequestHandler<GetAllCaseLoadsQuery, CaseLoadVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllCaseLoadsHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CaseLoadVM> Handle(GetAllCaseLoadsQuery request, CancellationToken cancellationToken)
        {
            var dbResults = await _unitOfWork.CaseLoadRepository.QueryAsync(
                "SELECT * FROM [IJOS].CASE_LOAD c " +
                "INNER JOIN[IJOS].OFFENDER o ON o.[SIN] = c.[SIN]");

            var caseLoadDtos = _mapper.Map<List<CaseLoadDto>>(dbResults);

            return new CaseLoadVM
            {
                CaseLoads = caseLoadDtos
            };
        }
    }
}