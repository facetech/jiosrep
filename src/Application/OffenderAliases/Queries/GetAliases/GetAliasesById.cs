﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetAliasesById : IRequest<AliasesVm>
    {
        public long Alias_number { get; set; }
        public int Sin { get; set; }
        //public long IndividualNumber { get; set; }
    }


    public class GetAliasesByIdHandler : IRequestHandler<GetAliasesById, AliasesVm>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        // private readonly IMapper _RelationshipMap;
        public GetAliasesByIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            //   _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<AliasesVm> Handle(GetAliasesById request, CancellationToken cancellationToken)
        {
            try
            {
                string sql1 = @" select *  from ijos.OFFENDER   where sin =@sin";
                var dbResults1 = await _unitOfWork.OffenderRepository.QuerySingleAsync
                    (sql1, new { sin = (long?)request.Sin });

                AliasesDto AliasesDto = new AliasesDto();
                if (request.Alias_number == 0)
                {
                    AliasesDto = new AliasesDto();
                    AliasesDto.Created_date = DateTime.Now;
                    AliasesDto.OffenderFullName = dbResults1.First_name + " " + " " + dbResults1.Last_name;
                    AliasesDto.Sin = request.Sin;

                }
                else
                {
                    string sql = @" select *  from ijos.Aliases   where Alias_number =@Alias_number";
                    var dbResults = await _unitOfWork.AliasesRepository.QuerySingleAsync
                        (sql, new { Alias_number = (long?)request.Alias_number });
                    AliasesDto = _mapper.Map<AliasesDto>(dbResults);
                    AliasesDto.Sin = request.Sin;
                    AliasesDto.OffenderFullName = dbResults1.First_name + " " + " " + dbResults1.Last_name;
                }
                return new AliasesVm
                {
                    AliasesDto = AliasesDto
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
