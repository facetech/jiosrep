﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetAliasesBySin : IRequest<List<AliasesDto>>
    {
        public int Sin { get; set; }
    }


    public class GetAliasesHandler : IRequestHandler<GetAliasesBySin, List<AliasesDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetAliasesHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<AliasesDto>> Handle(GetAliasesBySin request, CancellationToken cancellationToken)
        {
            try
            {
                List<AliasesDto> AliasesDtos = new List<AliasesDto>();
                string sql = @" select *  from ijos.Aliases where SIN =@sin ";
                var dbResults = await _unitOfWork.AliasesRepository.QueryAsync
                    (sql, new { sin = (long?)request.Sin });

                AliasesDtos = _mapper.Map<List<AliasesDto>>(dbResults);
                return AliasesDtos;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
