﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{



    public class AliasesDto : IMapFrom<e.Aliases>
    {
        public long Alias_number { get; set; }

        public long Sin { get; set; }
        public string OffenderFullName { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Middle_name { get; set; }
        public DateTime? Date_of_birth { get; set; }
        public string Alias_type { get; set; }
        public string Agency_name { get; set; }
        public string Status { get; set; }
        public string Created_by { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
    } // class ALIASES



}