﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class UpdateOffenderAliaseCommandValidator : AbstractValidator<UpdateOffenderAliaseCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateOffenderAliaseCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.AliasesVm.AliasesDto.First_name)
             .NotEmpty().NotNull().WithMessage(v => v.AliasesVm.AliasesDto.First_name + "First_name is required.");


        }




    }
}
