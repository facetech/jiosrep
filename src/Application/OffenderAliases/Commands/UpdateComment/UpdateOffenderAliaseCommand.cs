﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdateOffenderAliaseCommand : IRequest
    {
        public AliasesVm AliasesVm { get; set; }

    }

    public class UpdateOffenderAliaseCommandHandler : IRequestHandler<UpdateOffenderAliaseCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public UpdateOffenderAliaseCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffenderAliaseCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.AliasesVm.AliasesDto;
                e.Aliases entity = _mapper.Map<e.Aliases>(entityDbo);
                await _unitOfWork.AliasesRepository.Update(entity, cancellationToken);
                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
