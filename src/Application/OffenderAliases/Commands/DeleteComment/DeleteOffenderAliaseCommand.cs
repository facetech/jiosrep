﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class DeleteOffenderAliaseCommand : IRequest
    {

        public long Alias_number { get; set; }
    }

    public class DeleteOffenderAliaseCommandHandler : IRequestHandler<DeleteOffenderAliaseCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public DeleteOffenderAliaseCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteOffenderAliaseCommand request, CancellationToken cancellationToken)
        {
            try
            {

                string sql = @" select *  from ijos.Aliases   where Alias_number =@Alias_number";
                e.Aliases entity = await _unitOfWork.AliasesRepository.QuerySingleAsync
                    (sql, new { Alias_number = (long?)request.Alias_number });
             
                _unitOfWork.AliasesRepository.Delete(entity);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
