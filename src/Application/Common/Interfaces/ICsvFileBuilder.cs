﻿using IJOS.Application.TodoLists.Queries.ExportTodos;
using System.Collections.Generic;

namespace IJOS.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}
