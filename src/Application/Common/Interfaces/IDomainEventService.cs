﻿using IJOS.Domain.Common;
using System.Threading.Tasks;

namespace IJOS.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
