﻿using IJOS.Domain.Entities;
using e = IJOS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<TodoList> TodoLists { get; set; }

        DbSet<TodoItem> TodoItems { get; set; }

        DbSet<e.Offender> Offender { get; set; }

        public DbSet<OffenderAddress> Offender_Address { get; set; }
        public DbSet<OffenderAddressAudit> Offender_Address_Audit { get; set; }
        public DbSet<LivingArrangements> Living_Arrangements { get; set; }

        public DbSet<ZipCityStateCounty> Zip_City_State_County { get; set; }
        public DbSet<Photo> Photo { get; set; }
        public DbSet<Photo_types> Photo_types { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
