﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Persistence_Interfaces
{
    public interface IRepository<T> where T : AuditableEntity
    {
        T GetById(int id);
        IQueryable<T> GetAll();
        Task<T> GetById(long id);
        Task<IEnumerable<T>> QueryAsync(string query, object parameters = null);
        Task<T> QuerySingleAsync(string query, object parameters = null);
        void Delete(T entity);
        Task Update(T entity, CancellationToken cancellationToken = new CancellationToken());
        Task Insert(T entity, CancellationToken cancellationToken = new CancellationToken());
    }
}
