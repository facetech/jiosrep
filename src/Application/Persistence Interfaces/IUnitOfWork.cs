﻿using e = IJOS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application.Persistence_Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<e.IncidentOffenders> IncidentOffendersRepository { get; }
        IRepository<e.Jj_links> JJlinksRepository { get; }
        IRepository<e.Agency> AgencyRepository { get; }
        IRepository<e.Incident_Addendum> Incident_AddendumRepository { get; }
        IRepository<e.Incident> IncidentRepository { get; }
        IRepository<e.Offender_name_hist> Offender_name_histRepository { get; }
        IRepository<e.No_contact_order> No_contact_orderRepository { get; }
        IRepository<e.Scars_marks_tattoos> Scars_marks_tattoosRepository { get; }
        IRepository<e.Smt_codes> Smt_codesRepository { get; }
        IRepository<e.Aliases> AliasesRepository { get; }
        IRepository<e.Satellite_office> Satellite_officeRepository { get; }
        IRepository<e.Incident_detail> Incident_detailRepository { get; }
        IRepository<e.Incident_approvals> Incident_approvalsRepository { get; }
        IRepository<e.Ref_incident_type> Ref_incident_typeRepository { get; }
        IRepository<e.Ref_incident_type_category> Ref_incident_type_categoryRepository { get; }
        IRepository<e.Aspnetusers> AspnetusersRepository { get; }
        IRepository<e.Offender_comments> Offender_commentRepository { get; }
        IRepository<e.Offender_comment_history> Offender_comment_historyRepository { get; }
        IRepository<e.School_History> School_HistoryRepository { get; }
        IRepository<e.Schools> SchoolsRepository { get; }
        IRepository<e.Reason_Left_School> Reason_Left_SchoolRepository { get; }
        IRepository<e.OffenderSchool> OffenderSchoolRepository { get; }
        IRepository<e.OffenderOverview> OffenderOverviewRepository { get; }
        IRepository<e.Offender> OffenderRepository { get; }
        IRepository<e.Case_load> CaseLoadRepository { get; }
        IRepository<e.OffenderAddress> OffenderAddressRepository { get; }
        IRepository<e.Staff> StaffRepository { get; }
        IRepository<e.Relationships> RelationshipsRepository { get; }
        IRepository<e.OffenderAddressAudit> OffenderAddressAuditRepository { get; }
        IRepository<e.LivingArrangements> LivingArrangementRepository { get; }
        IRepository<e.ZipCityStateCounty> ZipCityStateCountyRepository { get; }
        IRepository<e.LanguageTypes> LanguageTypesRepository { get; }
        IRepository<e.DriversLicenseNumbers> DriversLicenseNumbersRepository { get; }
        IRepository<e.MaritalStatus> MaritalStatusRepository { get; }
        IRepository<e.OffenderPhysicalDescription> OffenderPhysicalDescriptionRepository { get; }
        IRepository<e.Photo> PhotoRepository { get; }
        IRepository<e.Photo_types> PhototypeRepository { get; }
        IRepository<e.Relationship_types> Relationship_typesRepository { get; }
        IRepository<e.Ed_ethnicity> EdEthnicityRepository { get; }
        IRepository<e.Race_types> RaceTypesRepository { get; }
        IRepository<e.Relationship_status> RelationshipStatusRepository { get; }
        IRepository<e.Relationship_linkage> Relationship_linkageRepository { get; }

 
        IRepository<e.Master_Column> Master_ColumnRepository { get; }
     

      
       
        IRepository<e.Gang> GangRepository { get; }
        IRepository<e.Offender_Gang> Offender_GangRepository { get; }
      
        IRepository<e.GangOffender> GangOffenderRepository { get; }
        IRepository<e.Juvenile_Alert> Juvenile_AlertRepository { get; }
        IRepository<e.Juvenile_Alert_Type> Juvenile_Alert_TypeRepository { get; }
    }
}
