﻿using IJOS.Application.TodoLists.Queries.GetTodos;
using IJOS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace IJOS.Application
{
    public class PhotosTypeVM
    {
       public string PhotoType { get; set; }
        public List<PhotoDto> photos { get; set; }
      

    }
}
