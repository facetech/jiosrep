﻿using AutoMapper;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetPhotoByTypeQuery : IRequest<PhotosTypeVM >
    {
        public int Sin { get; set; }
        public int Photo_type_id { get; set; }
        
    }


    public class GetPhotoByTypeQueryHandler : IRequestHandler<GetPhotoByTypeQuery, PhotosTypeVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPhotoByTypeQueryHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PhotosTypeVM> Handle(GetPhotoByTypeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var dbResults = _unitOfWork.PhotoRepository.GetAll().Where(o=>o.Sin==request.Sin && o.Photo_type_id == request.Photo_type_id).OrderByDescending(o => o.Date_taken);
                var photoDtos = _mapper.Map<List<PhotoDto>>(dbResults);
                var dbResultsphtype = _unitOfWork.PhototypeRepository.GetAll().Where(o=>o.Photo_type_id == request.Photo_type_id).SingleOrDefault();
                return new PhotosTypeVM
                {
                  PhotoType =  dbResultsphtype.Photo_type
                    ,
                    photos= photoDtos
                };

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
