﻿using IJOS.Application.TodoLists.Queries.GetTodos;
using IJOS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace IJOS.Application
{
    public class PhotoVM
    {
        public PhotoDto Photo { get; set; }
        public List<PhotoDto> photos { get; set; }
        public List<Photo_types> phototypes { get; set; }
        public object myfile { get; set; }

        public List<Satellite_office> Satellite_office { get; set; }
        public List<string> Cities { get; set; }

        




    }
}
