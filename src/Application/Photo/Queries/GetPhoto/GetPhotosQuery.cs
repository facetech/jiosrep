﻿using AutoMapper;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetPhotosQuery : IRequest<PhotoVM >
    {
        public int Sin { get; set; }
    }


    public class GetPhotosQueryHandler : IRequestHandler<GetPhotosQuery, PhotoVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPhotosQueryHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PhotoVM> Handle(GetPhotosQuery request, CancellationToken cancellationToken)
        {
            try
            {
                string sql = @" select *  from ijos.OFFENDER   where SIN =@sin";
                var offender = await _unitOfWork.OffenderRepository.QuerySingleAsync
                    (sql, new { sin = (long?)request.Sin });


                string dop = "";

                try
                {
                     dop = offender.Date_of_birth.ToString("MM/dd/yyyy");
                }
                catch { }

                var dbResults = _unitOfWork.PhotoRepository.GetAll().Where(o=>o.Sin==request.Sin).OrderByDescending(o => o.Date_taken);

                var photoDtos = _mapper.Map<List<PhotoDto>>(dbResults);


                List<PhotoDto> new_photoDtos = new List<PhotoDto>();
                foreach (PhotoDto item in photoDtos)
                {
                    item.Date_of_birth = dop;
                    if (new_photoDtos.Where(a => a.Photo_type_id == item.Photo_type_id).Count() < 2)
                        new_photoDtos.Add(item);
                }
                photoDtos = new_photoDtos;
                // List<Satellite_office> Satellite_offices  = _unitOfWork.Satellite_officeRepository.GetAll().ToList();


                string sql2 = @" select *  from ijos.Satellite_office  ";
                var RSatellite_offices = await _unitOfWork.Satellite_officeRepository.QueryAsync (sql2);
                List<Satellite_office> Satellite_offices = RSatellite_offices.ToList();

                var dbResultsphtype = _unitOfWork.PhototypeRepository.GetAll();

                var photodtypes = _mapper.Map<List<Photo_types>>(dbResultsphtype);

                var entityZipCityStateCounty = _unitOfWork.ZipCityStateCountyRepository.GetAll();


                var zipCityStateCounties = _mapper.Map<List<ZipCityStateCountyDto>>(entityZipCityStateCounty);
                List<string> cities = zipCityStateCounties.Select(o => o.City).Distinct().ToList();

                PhotoDto mainphoto = new PhotoDto();
                if (photoDtos.Where(a => a.Setasschoolid == true).Count() > 0)
                    mainphoto = photoDtos.Where(a => a.Setasschoolid == true).First();
                else
                    mainphoto = photoDtos[0];

                //mainphoto.Date_taken = DateTime.Now;
                return new PhotoVM
                {
                    Photo = mainphoto ,//new PhotoDto() { Sin=request.Sin , Date_of_birth  = dop ,Taken_by= mainphoto.Taken_by,  },
                    phototypes= photodtypes,
                    Cities= cities,
                    photos= photoDtos,
                    Satellite_office  = Satellite_offices
                };

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
