﻿using IJOS.Application.Common.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Domain.Entities
{
    public class PhotoDto : IMapFrom<e.Photo>
    {

       
        public int Photo_id { get; set; }
        public byte[] Photo_data { get; set; }
        public int? Photo_type_id { get; set; }
        public DateTime? Date_taken { get; set; }
        public DateTime? Date_edited { get; set; }
        
        public string Location_taken { get; set; }
        public int? Sin { get; set; }
        public string Login_name { get; set; }
        [Required]
        public string Taken_by { get; set; }
        public string Comments { get; set; }
        public string Login_created { get; set; }
        public string Login_edited { get; set; }
        public byte[] Photo_data_thumbnail { get; set; }
        public bool? Setasschoolid { get; set; }

        public DateTime? Created_date { get; set; }

        public string Created_by { get; set; }

        public DateTime? Modified_date { get; set; }

        public string Modified_by { get; set; }

        public string Date_of_birth { get; set; }

    } // class photo
}
