﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Common.Interfaces;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;


namespace IJOS.Application
{
    public class CreatePhotosCommand : IRequest<int>
    {
        public PhotoVM PhotoVM { get; set; }
    
    }

    public class CreatePhotosCommandHandler : IRequestHandler<CreatePhotosCommand,int>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IApplicationDbContext _context;

        public CreatePhotosCommandHandler(IApplicationDbContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> Handle(CreatePhotosCommand request, CancellationToken cancellationToken)
        {
            try
            {
              
                var inputEntity = request.PhotoVM.Photo;
                var entity = new e.Photo();
                var db = _mapper.Map(inputEntity,entity);
                _context.Photo.Add(db);
                await _context.SaveChangesAsync(cancellationToken);
                return db.Photo_id;
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }
    }
}
