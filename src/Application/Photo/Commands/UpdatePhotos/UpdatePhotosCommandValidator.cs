﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class UpdatePhotosCommandValidator : AbstractValidator<UpdatePhotosCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdatePhotosCommandValidator(IApplicationDbContext context)
        {
            _context = context;
        }
    }
}
