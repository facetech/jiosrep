﻿
using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class CreatePhotosCommandValidator : AbstractValidator<UpdatePhotosCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreatePhotosCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.PhotoVM.Photo.Taken_by)
             .NotEmpty().NotNull().WithMessage(v => v.PhotoVM.Photo.Taken_by + "Taken by is required.");


            RuleFor(v => v.PhotoVM.Photo.Photo_type_id)
             .NotEmpty().NotNull().WithMessage(v => v.PhotoVM.Photo.Photo_type_id + "Photo type is required.");


            RuleFor(v => v.PhotoVM.Photo.Date_taken)
             .NotEmpty().NotNull().WithMessage(v => v.PhotoVM.Photo.Date_taken + "Date taken is required.");


            RuleFor(v => v.PhotoVM.Photo.Location_taken)
             .NotEmpty().NotNull().WithMessage(v => v.PhotoVM.Photo.Location_taken + "Location taken is required.");
        }
    }
}
