﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdatePhotosCommand : IRequest
    {
        public PhotoVM PhotoVM { get; set; }
    }

    public class UpdatePhotosCommandHandler : IRequestHandler<UpdatePhotosCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdatePhotosCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdatePhotosCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var inputEntity = request.PhotoVM.Photo;
                var entity =  _unitOfWork.PhotoRepository.GetById(inputEntity.Photo_id);
              
                if (entity == null)
                {
                    throw new NotFoundException(nameof(e.OffenderOverview), inputEntity.Sin);
                }
                if (inputEntity.Photo_data==null)
                {
                    inputEntity.Photo_data = entity.Photo_data;
                }
                var db = _mapper.Map<PhotoDto, e.Photo>(inputEntity, entity);

                await _unitOfWork.PhotoRepository.Update(db, cancellationToken);
                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }
    }
}
