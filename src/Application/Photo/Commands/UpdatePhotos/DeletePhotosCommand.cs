﻿using IJOS.Application.Common.Exceptions;
using IJOS.Application.Common.Interfaces;
using IJOS.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.TodoLists.Commands.DeleteTodoList
{
    public class DeletePhotosCommand : IRequest
    {
        public int Id { get; set; }
    }

    public class DeletePhotosCommandHandler : IRequestHandler<DeletePhotosCommand>
    {
        private readonly IApplicationDbContext _context;

        public DeletePhotosCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeletePhotosCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Photo
                .Where(l => l.Photo_id == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(TodoList), request.Id);
            }

            _context.Photo.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
