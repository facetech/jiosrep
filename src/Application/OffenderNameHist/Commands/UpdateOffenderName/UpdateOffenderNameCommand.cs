﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdateOffenderNameCommand : IRequest
    {
        public Offender_name_histVm Offender_name_histVm { get; set; }

    }

    public class UpdateOffenderNameCommandHandler : IRequestHandler<UpdateOffenderNameCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public UpdateOffenderNameCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffenderNameCommand request, CancellationToken cancellationToken)
        {
            try
            {
                e.Offender offender = await _unitOfWork.OffenderRepository.GetById(request.Offender_name_histVm.Offender_name_histDto.Sin);

                var entityDbo = request.Offender_name_histVm.Offender_name_histDto;
                e.Offender_name_hist _entity = _mapper.Map<e.Offender_name_hist>(entityDbo);
                e.Offender_name_hist entity = new e.Offender_name_hist
                {
                    First_name = offender.First_name,
                    Last_name = offender.Last_name,
                    Middle_name = offender.Middle_name,
                    Suffix = offender.Suffix,
                    Sin = _entity.Sin,
                    Reason = _entity.Reason,
                };


                await _unitOfWork.Offender_name_histRepository.Insert(entity, cancellationToken);


                offender.First_name = request.Offender_name_histVm.Offender_name_histDto.First_name;
                offender.Last_name = request.Offender_name_histVm.Offender_name_histDto.Last_name;
                offender.Middle_name = request.Offender_name_histVm.Offender_name_histDto.Middle_name;
                offender.Suffix = request.Offender_name_histVm.Offender_name_histDto.Suffix;

                await _unitOfWork.OffenderRepository.Update(offender, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
