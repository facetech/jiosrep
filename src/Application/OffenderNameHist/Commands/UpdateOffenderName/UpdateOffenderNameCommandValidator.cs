﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class UpdateOffenderNameCommandValidator : AbstractValidator<UpdateOffenderNameCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateOffenderNameCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Offender_name_histVm.Offender_name_histDto.First_name)
             .NotEmpty().NotNull().WithMessage(v => v.Offender_name_histVm.Offender_name_histDto.First_name + "First_name is required.");

            RuleFor(v => v.Offender_name_histVm.Offender_name_histDto.Last_name)
                 .NotEmpty().NotNull().WithMessage(v => v.Offender_name_histVm.Offender_name_histDto.Last_name + "Last_name is required.");



        }




    }
}
