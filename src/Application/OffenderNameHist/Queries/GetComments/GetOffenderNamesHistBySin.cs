﻿using AutoMapper;
using IJOS.Application.Common.Interfaces;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetOffenderNamesHistBySin : IRequest<Offender_name_histVm>
    {
        public int Sin { get; set; }
    }


    public class GetOffenderNamesHistBySinHandler : IRequestHandler<GetOffenderNamesHistBySin, Offender_name_histVm>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetOffenderNamesHistBySinHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        private readonly ICurrentUserService _currentUserService;
        public async Task<Offender_name_histVm> Handle(GetOffenderNamesHistBySin request, CancellationToken cancellationToken)
        {
            try
            {
                List<Offender_name_histDto> Offender_name_histDtos = new List<Offender_name_histDto>();
                string sql = @" select *  from ijos.Offender_Name_Hist where SIN =@sin ";
                var dbResults = await _unitOfWork.Offender_name_histRepository.QueryAsync
                    (sql, new { sin = (long?)request.Sin });



                Offender_name_histDtos = _mapper.Map<List<Offender_name_histDto>>(dbResults);


                foreach (Offender_name_histDto item in Offender_name_histDtos)
                {
                    try
                    {
                        string sql2 = @" select *  from ijos.Aspnetusers where Id =@Id ";
                        var dbResults2 = await _unitOfWork.AspnetusersRepository.QuerySingleAsync
                            (sql2, new { Id = item.Created_by });

                        item.Created_by = dbResults2.Username;
                       
                    }
                    catch { }

                }

                return new Offender_name_histVm
                {
                    Offender_name_histDtos = Offender_name_histDtos,
                    Offender_name_histDto = new Offender_name_histDto() {Sin= request.Sin }
                } ;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
