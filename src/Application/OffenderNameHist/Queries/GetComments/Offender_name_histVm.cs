﻿using IJOS.Application;
using IJOS.Application.TodoLists.Queries.GetTodos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class Offender_name_histVm
    {
        public List< Offender_name_histDto> Offender_name_histDtos { get; set; }
        public Offender_name_histDto Offender_name_histDto { get; set; }

    }
}