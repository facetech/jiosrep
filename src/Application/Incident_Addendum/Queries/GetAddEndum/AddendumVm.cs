﻿using IJOS.Application;
using IJOS.Application.TodoLists.Queries.GetTodos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class AddendumVm
    {
        public Incident_AddendumDto Incident_AddendumDto { get; set; }

        public List<Incident_AddendumDto> Incident_AddendumDtos { get; set; }
    }
}