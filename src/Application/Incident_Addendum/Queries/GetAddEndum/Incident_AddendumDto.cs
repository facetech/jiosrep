﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{



    public class Incident_AddendumDto : IMapFrom<e.Incident_Addendum>
    {
        public long Addendum_id { get; set; }
        public long Incident_number { get; set; }
        public string Comments { get; set; }
        public long? Staff_key { get; set; }
        public DateTime Update_date { get; set; }
        public string Is_active { get; set; }
        public long? Group_addendum_id { get; set; }
        public DateTime? Create_date { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    } 


}