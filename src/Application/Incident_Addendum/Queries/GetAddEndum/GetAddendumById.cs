﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetAddendumById : IRequest<AddendumVm>
    {
        public long Addendum_id { get; set; }
       public long IncidentId { get; set; }

    }


    public class GetAddendumByIdHandler : IRequestHandler<GetAddendumById, AddendumVm>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        // private readonly IMapper _RelationshipMap;
        public GetAddendumByIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            //   _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<AddendumVm> Handle(GetAddendumById request, CancellationToken cancellationToken)
        {
            try
            {
                //string sql1 = @" select *  from ijos.INCIDENT_ADDENDUM    where ADDENDUM_ID =@Addendum_id";
                //Incident_Addendum dbResults1 = await _unitOfWork.Incident_AddendumRepository.QuerySingleAsync
                //    (sql1, new { Addendum_id = (long?)request.Addendum_id });

                Incident_AddendumDto AddendumDto = new Incident_AddendumDto();
                if (request.Addendum_id == 0)
                {
                    AddendumDto  = new Incident_AddendumDto();
                    AddendumDto.Created_date = DateTime.Now;
               AddendumDto.Incident_number = request.IncidentId;

                }
                else
                {
                    string sql = @" select *  from ijos.INCIDENT_ADDENDUM    where ADDENDUM_ID = @Addendum_id";
                    Incident_Addendum dbResults = await _unitOfWork.Incident_AddendumRepository.QuerySingleAsync
                        (sql, new { Addendum_id = (long?)request.Addendum_id });

                    AddendumDto = _mapper.Map<Incident_AddendumDto>(dbResults);
                
                    
                }
                return new AddendumVm
                {
                    Incident_AddendumDto = AddendumDto
                };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
