﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetAddendumBYIncidentId : IRequest<AddendumVm>
    {
        public long IncidentId { get; set; }
    }


    public class GetAddendumBYIncidentIdHandler : IRequestHandler<GetAddendumBYIncidentId, AddendumVm>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetAddendumBYIncidentIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<AddendumVm> Handle(GetAddendumBYIncidentId request, CancellationToken cancellationToken)
        {
            try
            {
                List<Incident_AddendumDto> AddendDtos = new List<Incident_AddendumDto>();
                string sql = @" select *  from ijos.INCIDENT_ADDENDUM where INCIDENT_NUMBER =@IncidentId ";
                var dbResults = await _unitOfWork.Incident_AddendumRepository.QueryAsync
                    (sql, new { IncidentId = (long?)request.IncidentId });


        

                AddendDtos = _mapper.Map<List<Incident_AddendumDto>>(dbResults);


                foreach (Incident_AddendumDto item in AddendDtos)
                {
                    try
                    {
                        string sql2 = @" select *  from ijos.Aspnetusers where Id =@Id ";
                        string by = item.Modified_by;

                        if (by == null)
                            by = item.Created_by;
                        var dbResults2 = await _unitOfWork.AspnetusersRepository.QuerySingleAsync
                            (sql2, new { Id = by });

                        item.Modified_by = dbResults2.Username;

                    }
                    catch { }

                    if (item.Modified_date == null)
                        item.Modified_date = item.Created_date;

                }

                return new AddendumVm
                {
                    Incident_AddendumDtos = AddendDtos
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
