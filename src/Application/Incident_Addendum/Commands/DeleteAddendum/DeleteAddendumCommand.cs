﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class DeleteAddendumCommand : IRequest
    {

        public long AddendumId { get; set; }
    }

    public class DeleteAddendumCommandHandler : IRequestHandler<DeleteAddendumCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public DeleteAddendumCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteAddendumCommand request, CancellationToken cancellationToken)
        {
            try
            {

                string sql = @" select *  from ijos.Incident_Addendum   where ADDENDUM_ID =@AddendumId";
                e.Incident_Addendum entity = await _unitOfWork.Incident_AddendumRepository.QuerySingleAsync
                    (sql, new { AddendumId = (long?)request.AddendumId });

                //e.Offender_comment_history Offender_comment_history = new e.Offender_comment_history { Comments = entity.Comments, Created_date = System.DateTime.Now, Sin = entity.Sin, Status = "Deleted" };
                //await _unitOfWork.Offender_comment_historyRepository.Insert(Offender_comment_history, cancellationToken );
             
                _unitOfWork.Incident_AddendumRepository.Delete(entity);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
