﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class CreateAddendumCommand : IRequest
    {
        public AddendumVm AddendumVm { get; set; }

    }

    public class CreateAddendumCommandHandler : IRequestHandler<CreateAddendumCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public CreateAddendumCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreateAddendumCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.AddendumVm.Incident_AddendumDto;
                e.Incident_Addendum entity = _mapper.Map<e.Incident_Addendum>(entityDbo);
                await _unitOfWork.Incident_AddendumRepository.Insert(entity, cancellationToken);
                
                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
