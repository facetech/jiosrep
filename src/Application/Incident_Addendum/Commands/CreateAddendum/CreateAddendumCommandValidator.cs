﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class CreateAddendumCommandValidator : AbstractValidator<CreateAddendumCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateAddendumCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.AddendumVm.Incident_AddendumDto.Comments)
             .NotEmpty().NotNull().WithMessage(v => v.AddendumVm.Incident_AddendumDto.Comments + "Comments is required.");


        }

     


    }
}
