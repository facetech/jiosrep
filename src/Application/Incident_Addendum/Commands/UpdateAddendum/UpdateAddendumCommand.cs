﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdateAddendumCommand : IRequest
    {
        public AddendumVm AddendumVm { get; set; }

    }

    public class UpdateAddendumCommandHandler : IRequestHandler<UpdateAddendumCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public UpdateAddendumCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateAddendumCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.AddendumVm.Incident_AddendumDto;
                e.Incident_Addendum entity = _mapper.Map<e.Incident_Addendum>(entityDbo);
          
                await _unitOfWork.Incident_AddendumRepository.Update(entity, cancellationToken);

                ////e.Offender_comment_history Offender_comment_history = new e.Offender_comment_history { Offender_comment_id = entity.Offender_comment_id, Comments = entity.Comments, Created_date = System.DateTime.Now, Sin = entity.Sin, Status = "Updated" };
                //await _unitOfWork.in.Insert(Offender_comment_history, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
