﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{


    public class Scars_marks_tattoosDto : IMapFrom<e.Scars_marks_tattoos>
    {
        public long Smt_number { get; set; }
        public long? Sin { get; set; }
        public string OffenderFullName { get; set; }
        public string Scar_mark_tattoo { get; set; }
        public string Description { get; set; }
        public long? Photo_id { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    } // class SCARS_MARKS_TATTOOS




}