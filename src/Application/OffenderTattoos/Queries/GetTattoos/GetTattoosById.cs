﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetTattoosById : IRequest<Scars_marks_tattoosVm>
    {
        public long Smt_number { get; set; }
        public int Sin { get; set; }
        //public long IndividualNumber { get; set; }
    }


    public class GetTattoosByIdHandler : IRequestHandler<GetTattoosById, Scars_marks_tattoosVm>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        // private readonly IMapper _RelationshipMap;
        public GetTattoosByIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            //   _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<Scars_marks_tattoosVm> Handle(GetTattoosById request, CancellationToken cancellationToken)
        {
            try
            {
                string sql_Smt_codes = @" select *  from ijos.Smt_codes ";
                var Smt_codes = await _unitOfWork.Smt_codesRepository.QueryAsync(sql_Smt_codes);
                List<string> TattoosTypes = Smt_codes.Select(o => o.Scar_mark_tattoo_2).Distinct().ToList();
              
                string sql1 = @" select *  from ijos.OFFENDER   where sin =@sin";
                var dbResults1 = await _unitOfWork.OffenderRepository.QuerySingleAsync
                    (sql1, new { sin = (long?)request.Sin });

                Scars_marks_tattoosDto Scars_marks_tattoosDto = new Scars_marks_tattoosDto();
                if (request.Smt_number == 0)
                {
                    Scars_marks_tattoosDto = new Scars_marks_tattoosDto();
                    Scars_marks_tattoosDto.Created_date = DateTime.Now;
                    Scars_marks_tattoosDto.Sin = request.Sin;
                    Scars_marks_tattoosDto.OffenderFullName = dbResults1.First_name + " " + " " + dbResults1.Last_name;

                }
                else
                {
                    string sql = @" select *  from ijos.Scars_marks_tattoos   where Smt_number =@Smt_number";
                    var dbResults = await _unitOfWork.Scars_marks_tattoosRepository.QuerySingleAsync
                        (sql, new { Smt_number = (long?)request.Smt_number });
                    Scars_marks_tattoosDto = _mapper.Map<Scars_marks_tattoosDto>(dbResults);
                    Scars_marks_tattoosDto.Sin = request.Sin;
                    Scars_marks_tattoosDto.OffenderFullName = dbResults1.First_name + " " + " " + dbResults1.Last_name;

                }
                return new Scars_marks_tattoosVm
                {
                    Scars_marks_tattoosDto = Scars_marks_tattoosDto,
                    Smt_codes = TattoosTypes
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
