﻿using IJOS.Application;
using IJOS.Application.TodoLists.Queries.GetTodos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class Scars_marks_tattoosVm
    {
        public Scars_marks_tattoosDto Scars_marks_tattoosDto { get; set; }

        public List<string> Smt_codes { get; set; }
    }
}