﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetTattoosBySin : IRequest<List<Scars_marks_tattoosDto>>
    {
        public int Sin { get; set; }
    }


    public class GetTattoosHandler : IRequestHandler<GetTattoosBySin, List<Scars_marks_tattoosDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetTattoosHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<Scars_marks_tattoosDto>> Handle(GetTattoosBySin request, CancellationToken cancellationToken)
        {
            try
            {
                List<Scars_marks_tattoosDto> Scars_marks_tattoosDto = new List<Scars_marks_tattoosDto>();
                string sql = @" select *  from ijos.Scars_marks_tattoos where SIN =@sin ";
                var dbResults = await _unitOfWork.Scars_marks_tattoosRepository.QueryAsync
                    (sql, new { sin = (long?)request.Sin });

                Scars_marks_tattoosDto = _mapper.Map<List<Scars_marks_tattoosDto>>(dbResults);
                return Scars_marks_tattoosDto;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
