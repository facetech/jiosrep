﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class UpdateOffenderTattoosCommandValidator : AbstractValidator<UpdateOffenderTattoosCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateOffenderTattoosCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Scars_marks_tattoosVm.Scars_marks_tattoosDto.Scar_mark_tattoo)
             .NotEmpty().NotNull().WithMessage(v => v.Scars_marks_tattoosVm.Scars_marks_tattoosDto.Scar_mark_tattoo + "tattoo type is required.");


        }




    }
}
