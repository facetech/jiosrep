﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdateOffenderTattoosCommand : IRequest
    {
        public Scars_marks_tattoosVm Scars_marks_tattoosVm { get; set; }

    }

    public class UpdateOffenderTattoosCommandHandler : IRequestHandler<UpdateOffenderTattoosCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public UpdateOffenderTattoosCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffenderTattoosCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.Scars_marks_tattoosVm.Scars_marks_tattoosDto;
                e.Scars_marks_tattoos entity = _mapper.Map<e.Scars_marks_tattoos>(entityDbo);
                await _unitOfWork.Scars_marks_tattoosRepository.Update(entity, cancellationToken);
                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
