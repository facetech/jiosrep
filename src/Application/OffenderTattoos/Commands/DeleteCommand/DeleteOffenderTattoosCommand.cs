﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class DeleteOffenderTattoosCommand : IRequest
    {

        public long Smt_number { get; set; }
    }

    public class DeleteOffenderTattoosCommandHandler : IRequestHandler<DeleteOffenderTattoosCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public DeleteOffenderTattoosCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteOffenderTattoosCommand request, CancellationToken cancellationToken)
        {
            try
            {

                string sql = @" select *  from ijos.Scars_marks_tattoos   where Smt_number =@Smt_number";
                e.Scars_marks_tattoos entity = await _unitOfWork.Scars_marks_tattoosRepository.QuerySingleAsync
                    (sql, new { Smt_number = (long?)request.Smt_number });
             
                _unitOfWork.Scars_marks_tattoosRepository.Delete(entity);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
