﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.Gangs.Queries.GetGangs;
using MediatR;
using e = IJOS.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;
using System;
namespace IJOS.Application.Gangs.Commands
{
    public class DeleteGang_RelationshipCommand : IRequest
    {
        public long Relationship_Number { get; set; }
    }

    public class DeleteGang_RelationshipCommandHandler : IRequestHandler<DeleteGang_RelationshipCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DeleteGang_RelationshipCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteGang_RelationshipCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string sqlGang_Monikers = @" SELECT * FROM GANG_MONIKERS where RELATIONSHIP_NUMBER = @relationship_number";
                e.Gang_Monikers entity_Gang_Monikers = await _unitOfWork.Gang_MonikersRepository.QuerySingleAsync
                    (sqlGang_Monikers, new { relationship_number = (long?)request.Relationship_Number });
                _unitOfWork.Gang_MonikersRepository.Delete(entity_Gang_Monikers);

                string sqlGang_Relationship = @" SELECT * FROM GANG_RELATIONSHIP WHERE RELATIONSHIP_NUMBER = @relationship_number";
                e.Gang_Relationship entity_Gang_Relationship = await _unitOfWork.Gang_RelationshipRepository.QuerySingleAsync
                    (sqlGang_Relationship, new { relationship_number = (long?)request.Relationship_Number });
                _unitOfWork.Gang_RelationshipRepository.Delete(entity_Gang_Relationship);

                return Unit.Value;

            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
