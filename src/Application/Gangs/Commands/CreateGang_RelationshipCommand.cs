﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.Gangs.Queries.GetGangs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.Gangs.Commands
{
    public class CreateGang_RelationshipCommand : IRequest
    {
        public OffenderGangsVM OffenderGangsVM { get; set; }
    }

    public class CreateGang_RelationshipCommandHandler : IRequestHandler<CreateGang_RelationshipCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateGang_RelationshipCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreateGang_RelationshipCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.OffenderGangsVM.Gang_RelationshipDto.Created_date = System.DateTime.Now;
                var entityDbo_Gang_Relationship = request.OffenderGangsVM.Gang_RelationshipDto;
                e.Gang_Relationship entity_Gang_Relationship = _mapper.Map<e.Gang_Relationship>(entityDbo_Gang_Relationship);
                await _unitOfWork.Gang_RelationshipRepository.Insert(entity_Gang_Relationship, cancellationToken);

                request.OffenderGangsVM.Gang_MonikersDto.Created_date = System.DateTime.Now;
                request.OffenderGangsVM.Gang_MonikersDto.Relationship_number = entity_Gang_Relationship.Relationship_number;
                var entityDbo_Gang_Monikers = request.OffenderGangsVM.Gang_MonikersDto;
                e.Gang_Monikers entity_Gang_Monikers = _mapper.Map<e.Gang_Monikers>(entityDbo_Gang_Monikers);
                await _unitOfWork.Gang_MonikersRepository.Insert(entity_Gang_Monikers, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }


    }
}
