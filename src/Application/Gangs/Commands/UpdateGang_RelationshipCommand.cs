﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.Gangs.Queries.GetGangs;
using MediatR;
using e = IJOS.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.Gangs.Commands
{
    public class UpdateGang_RelationshipCommand : IRequest
    {
        public OffenderGangsVM OffenderGangsVM { get; set; }
    }

    public class UpdateGang_RelationshipCommandHandler : IRequestHandler<UpdateGang_RelationshipCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateGang_RelationshipCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateGang_RelationshipCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.OffenderGangsVM.Gang_RelationshipDto.Modified_date = DateTime.Now;
                var entityDbo_Gang_Relationship = request.OffenderGangsVM.Gang_RelationshipDto;
                e.Gang_Relationship entity_Gang_Relationship = _mapper.Map<e.Gang_Relationship>(entityDbo_Gang_Relationship);
                await _unitOfWork.Gang_RelationshipRepository.Update(entity_Gang_Relationship, cancellationToken);

                request.OffenderGangsVM.Gang_MonikersDto.Modified_date = DateTime.Now;
                var entityDbo_Gang_Monikers = request.OffenderGangsVM.Gang_MonikersDto;
                e.Gang_Monikers entity_Gang_Monikers = _mapper.Map<e.Gang_Monikers>(entityDbo_Gang_Monikers);
                await _unitOfWork.Gang_MonikersRepository.Update(entity_Gang_Monikers, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }


    }
}
