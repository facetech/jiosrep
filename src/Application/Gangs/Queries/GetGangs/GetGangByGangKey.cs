﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.TodoLists.Queries.GetTodos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Gangs.Queries.GetGangs
{
    public class GetGangByGangKey : IRequest<OffenderGangsVM>
    {
        public long Relationship_Number { get; set; }
        public int Sin { get; set; }
    }

    public class GetGangByGangKeyHandler : IRequestHandler<GetGangByGangKey, OffenderGangsVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetGangByGangKeyHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderGangsVM> Handle(GetGangByGangKey request, CancellationToken cancellationToken)
        {
            try
            {
                Gang_RelationshipDto gang_RelationshipDto = new Gang_RelationshipDto();
                List<Gang_RelationshipDto> gang_RelationshipDtoList = new List<Gang_RelationshipDto>();

                GangsDto gangsDto = new GangsDto();
                List<GangsDto> gangDtoList = new List<GangsDto>();

                Gang_MonikersDto gang_MonikersDto = new Gang_MonikersDto();

                List<Master_ColumnDto> master_ColumnDtosList = new List<Master_ColumnDto>();
                
                List<string> rankList = null;
                List<string> colorsList = null;
                List<string> statusList = null;
                List<string> initiated_ByList = null;

                string sql_Offender = @"SELECT * FROM OFFENDER WHERE SIN =@sin";
                var dbResult_Offender = await _unitOfWork.OffenderGangRepository.
                    QuerySingleAsync(sql_Offender, new { sin = (long?)request.Sin });

                string sql_Master_ColumnDto = "SELECT * FROM MASTER_COLUMN";
                var dbResult_Master_ColumnDto = await _unitOfWork.Master_ColumnRepository.QueryAsync(sql_Master_ColumnDto);
                master_ColumnDtosList = _mapper.Map<List<Master_ColumnDto>>(dbResult_Master_ColumnDto);
                rankList = master_ColumnDtosList.Select(o => o.Rank_gangrelationship).Distinct().ToList();
                statusList = master_ColumnDtosList.Select(o => o.Status_gangrelationship).Distinct().ToList();
                initiated_ByList = master_ColumnDtosList.Select(o => o.Initiatedby_gangrelationship).Distinct().ToList();

                string sql_GangDtoList = @" SELECT DISTINCT GANG_NAME, * from GANGS";
                var dbResults_GangDtoList = await _unitOfWork.GangsRepository.QueryAsync(sql_GangDtoList);
                gangDtoList = _mapper.Map<List<GangsDto>>(dbResults_GangDtoList);
                colorsList = gangDtoList.Select(o => o.Colors).Distinct().ToList();

                if (request.Relationship_Number ==0)
                {
                    gang_RelationshipDto.Sin = request.Sin;
                }
                else
                {
                    string sql_Gang_RelationshipDto = "SELECT * FROM GANG_RELATIONSHIP WHERE RELATIONSHIP_NUMBER = @relationship_number";
                    var dbResult_Gang_RelationshipDto = await _unitOfWork.Gang_RelationshipRepository.
                        QuerySingleAsync(sql_Gang_RelationshipDto, new { relationship_number = (long?)request.Relationship_Number });
                    gang_RelationshipDto = _mapper.Map<Gang_RelationshipDto>(dbResult_Gang_RelationshipDto);

                    string sql_Gang_MonikersDto = "SELECT * FROM GANG_MONIKERS WHERE RELATIONSHIP_NUMBER = @relationship_number";
                    var dbResult_Gang_MonikersDto = await _unitOfWork.Gang_MonikersRepository.
                        QuerySingleAsync(sql_Gang_MonikersDto, new { relationship_number = (long?)request.Relationship_Number });
                    gang_MonikersDto = _mapper.Map<Gang_MonikersDto>(dbResult_Gang_MonikersDto);

                }


                return new OffenderGangsVM
                {
                    Offender_FullName = dbResult_Offender.First_name + " " + " " + dbResult_Offender.Last_name,
                    Gang_RelationshipDto = gang_RelationshipDto,
                    GangsDtoList = gangDtoList,
                    RankList = rankList,
                    ColorsList = colorsList,
                    StatusList = statusList,
                    Initiated_ByList = initiated_ByList,
                    Gang_MonikersDto = gang_MonikersDto,
                };
                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
