﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.Gangs.Queries.GetGangs
{
    public class GangsDto : IMapFrom<e.Gangs>
    {
        public long Gang_number { get; set; }
        public string Gang_name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County_name { get; set; }
        public string Location { get; set; }
        public string Gang_type { get; set; }
        public string Colors { get; set; }
        public string Is_active { get; set; }
        public string Last_user { get; set; }
        public DateTime? Last_update { get; set; }
        public string Created_by { get; set; }
        public DateTime? Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
    }
}
