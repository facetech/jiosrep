﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.Gangs.Queries.GetGangs
{
    public class Know_GangsDto : IMapFrom<e.Known_Gangs>
    {
        public long Gang_number { get; set; }
        public string Gang_name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Location { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    }
}
