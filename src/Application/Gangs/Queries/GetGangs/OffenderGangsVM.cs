﻿using System.Collections.Generic;

namespace IJOS.Application.Gangs.Queries.GetGangs
{
    public class OffenderGangsVM
    {
        public Gang_RelationshipDto Gang_RelationshipDto { get; set; }
        public GangsDto GangsDto { get; set; }
        public Know_GangsDto Know_GangsDto { get; set; }
        public OffenderGangDto OffenderGangDto { get; set; }
        public Gang_MonikersDto Gang_MonikersDto { get; set; }

        public List<Gang_RelationshipDto> Gang_RelationshipDtoList { get; set; }
        public List<GangsDto> GangsDtoList { get; set; }
        public List<Know_GangsDto> Know_GangsDtoList { get; set; }
        public List<OffenderGangDto> OffenderGangDtoList { get; set; }
        public List<Gang_MonikersDto> Gang_MonikersDtoList { get; set; }

        public string Offender_FullName { get; set; }
        public List<string> RankList { get; set; }
        public List<string> ColorsList { get; set; }
        public List<string> Initiated_ByList { get; set; }
        public List<string> StatusList { get; set; }
    }

}

