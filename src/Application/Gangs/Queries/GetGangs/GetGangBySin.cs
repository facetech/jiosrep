﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Gangs.Queries.GetGangs
{
    public class GetGangBySin : IRequest<OffenderGangsVM>
    {
        public int Sin { get; set; }
        public int Gang_Number { get; set; }
    }

    public class GetGangBySinHandler : IRequestHandler<GetGangBySin, OffenderGangsVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetGangBySinHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderGangsVM> Handle(GetGangBySin request, CancellationToken cancellationToken)
        {
            try
            {
                List<Gang_RelationshipDto> gang_RelationshipDtoList = new List<Gang_RelationshipDto>();
                string sqlGang_Relationship = @" SELECT * FROM GANG_RELATIONSHIP WHERE SIN = @sin";
                var dbResultsqlGang_Relationship = await _unitOfWork.Gang_RelationshipRepository.QueryAsync
                    (sqlGang_Relationship, new { sin = (long?)request.Sin });
                gang_RelationshipDtoList = _mapper.Map<List<Gang_RelationshipDto>>(dbResultsqlGang_Relationship);

                List<OffenderGangDto> offenderGangDtoList = new List<OffenderGangDto>();
                string sqlOffenderGang = @" SELECT G.*, GR.* FROM GANG_RELATIONSHIP GR, GANGS G WHERE GR.GANG_NUMBER = G.GANG_NUMBER AND GR.SIN =@sin";
                var dbResultsqlOffenderGang = await _unitOfWork.OffenderGangRepository.QueryAsync
                    (sqlOffenderGang, new { sin = (long?)request.Sin });
                offenderGangDtoList = _mapper.Map<List<OffenderGangDto>>(dbResultsqlOffenderGang);

                OffenderGangsVM offenderGangsVM = new OffenderGangsVM();
                offenderGangsVM.Gang_RelationshipDtoList = gang_RelationshipDtoList;
                offenderGangsVM.OffenderGangDtoList = offenderGangDtoList;

                return offenderGangsVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
