﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.Gangs.Queries.GetGangs
{
    public class Gang_MonikersDto : IMapFrom<e.Gang_Monikers>
    {
        public long Moniker_number { get; set; }
        public long? Relationship_number { get; set; }
        public string Moniker { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    }
}
