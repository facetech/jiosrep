﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class DeleteOffenderStaffCommand : IRequest
    {

        public long STAFF_KEY { get; set; }
    }

    public class DeleteOffenderStaffCommandHandler : IRequestHandler<DeleteOffenderStaffCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public DeleteOffenderStaffCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteOffenderStaffCommand request, CancellationToken cancellationToken)
        {
            try
            {

                string sql = @" select * from ijos.staff   where STAFF_KEY =@STAFF_KEY";
                e.Staff entity = await _unitOfWork.StaffRepository.QuerySingleAsync
                    (sql, new { STAFF_KEY = (long?)request.STAFF_KEY });

                //e.Offender_comment_history Offender_comment_history = new e.Offender_comment_history { Comments = entity.Comments, Created_date = System.DateTime.Now, Sin = entity.Sin, Status = "Deleted" };       
                //await _unitOfWork.Offender_comment_historyRepository.Insert(Offender_comment_history, cancellationToken );
                entity.Is_active = "0";
                await _unitOfWork.StaffRepository.Update(entity,cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
