﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdateOffenderStaffCommand : IRequest
    {
        public StaffVM StaffVm { get; set; }

    }

    public class UpdateOffenderStaffCommandHandler : IRequestHandler<UpdateOffenderStaffCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public UpdateOffenderStaffCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffenderStaffCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.StaffVm.StaffDto;
                e.Staff entity = _mapper.Map<e.Staff>(entityDbo);
                //entity.Modified_by = "";
                entity.Modified_date = System.DateTime.Now;

                await _unitOfWork.StaffRepository.Update(entity, cancellationToken);
                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
