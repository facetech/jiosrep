﻿    using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class UpdateOffenderStaffCommandValidator : AbstractValidator<UpdateOffenderStaffCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateOffenderStaffCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.StaffVm.StaffDto.Login_name)
             .NotEmpty().NotNull().WithMessage(v => v.StaffVm.StaffDto.Login_name + "Login Name is required.");

            RuleFor(v => v.StaffVm.StaffDto.County_name)
            .NotEmpty().NotNull().WithMessage(v => v.StaffVm.StaffDto.County_name + "County Name is required.");


            RuleFor(v => v.StaffVm.StaffDto.Agency_name)
           .NotEmpty().NotNull().WithMessage(v => v.StaffVm.StaffDto.Agency_name + "Agency Name is required.");

            RuleFor(v => v.StaffVm.StaffDto.First_name)
          .NotEmpty().NotNull().WithMessage(v => v.StaffVm.StaffDto.First_name + "First Name is required.");

            RuleFor(v => v.StaffVm.StaffDto.Last_name)
          .NotEmpty().NotNull().WithMessage(v => v.StaffVm.StaffDto.Last_name + "Last Name is required.");

            RuleFor(v => v.StaffVm.StaffDto.E_mail_address)
       .NotEmpty().NotNull().WithMessage(v => v.StaffVm.StaffDto.E_mail_address + "Email Address is required.");


        }




    }
}
