﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class CreateOffenderStaffCommand : IRequest
    {
        public StaffVM staffVm { get; set; }
        public long sin { get; set; }
    }

    public class CreateOffenderStaffCommandHandler : IRequestHandler<CreateOffenderStaffCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public CreateOffenderStaffCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            
        }

        public async Task<Unit> Handle(CreateOffenderStaffCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.staffVm.StaffDto;
                e.Staff entity = _mapper.Map<e.Staff>(entityDbo);
                entity.Created_date = System.DateTime.Now;

                await _unitOfWork.StaffRepository.Insert(entity, cancellationToken);


                e.Jj_links entityRl = new e.Jj_links();
                entityRl.Sin = request.sin;
                entityRl.Staff_key = entity.Staff_key;
                entityRl.Created_date = System.DateTime.Now;
                entityRl.Update_date = System.DateTime.Now;
                entityRl.Updated_by = "";
                entityRl.Relationship = "";

                //  entityRl.Relationship_to_offender = entityDbo.RELATIONSHIP_TO_OFFENDER;
                await _unitOfWork.JJlinksRepository.Insert(entityRl, cancellationToken);


                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
