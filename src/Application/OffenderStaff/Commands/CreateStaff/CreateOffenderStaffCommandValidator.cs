﻿    using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class CreateOffenderStaffCommandValidator : AbstractValidator<CreateOffenderStaffCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateOffenderStaffCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.staffVm.StaffDto.Login_name)
             .NotEmpty().NotNull().WithMessage(v => v.staffVm.StaffDto.Login_name + "Login Name is required.");

            RuleFor(v => v.staffVm.StaffDto.County_name)
            .NotEmpty().NotNull().WithMessage(v => v.staffVm.StaffDto.County_name + "County Name is required.");


            RuleFor(v => v.staffVm.StaffDto.Agency_name)
           .NotEmpty().NotNull().WithMessage(v => v.staffVm.StaffDto.Agency_name + "Agency Name is required.");

            RuleFor(v => v.staffVm.StaffDto.First_name)
          .NotEmpty().NotNull().WithMessage(v => v.staffVm.StaffDto.First_name + "First Name is required.");

            RuleFor(v => v.staffVm.StaffDto.Last_name)
          .NotEmpty().NotNull().WithMessage(v => v.staffVm.StaffDto.Last_name + "Last Name is required.");

            RuleFor(v => v.staffVm.StaffDto.E_mail_address)
       .NotEmpty().NotNull().WithMessage(v => v.staffVm.StaffDto.E_mail_address + "Email Address is required.");


        }




    }
}
