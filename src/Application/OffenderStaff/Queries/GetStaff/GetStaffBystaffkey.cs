﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetStaffBystaffkey : IRequest<StaffVM>
    {
        public int Sin { get; set; }
        public long staff_key { get; set; }
    }


    public class GetStaffBystaffkeyHandler : IRequestHandler<GetStaffBystaffkey, StaffVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetStaffBystaffkeyHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<StaffVM> Handle(GetStaffBystaffkey request, CancellationToken cancellationToken)
        {
            try
            {
                var entityZipCityStateCounty = _unitOfWork.ZipCityStateCountyRepository.GetAll();
                var zipCityStateCounties = _mapper.Map<List<ZipCityStateCountyDto>>(entityZipCityStateCounty);
                List<string> Countys = zipCityStateCounties.Select(o => o.County).Distinct().ToList();

                var entityagency = _unitOfWork.AgencyRepository.GetAll();
                var agencys = _mapper.Map<List<Agency>>(entityagency);
               List<string> agencyss = agencys.Select(o => o.Agency_name).Distinct().ToList();


                StaffDto _StaffDto = new StaffDto();
                if (request.staff_key == 0)
                {
                    _StaffDto = new StaffDto();
                    _StaffDto.Is_active = "1";
                }
                else
                {
                    string sql = @" select *  from ijos.STAFF   where STAFF_KEY =@STAFF_KEY";
                    var dbResults = await _unitOfWork.StaffRepository.QuerySingleAsync
                        (sql, new { STAFF_KEY = (long?)request.staff_key });
                    _StaffDto = _mapper.Map<StaffDto>(dbResults);
                   // _StaffDto.Sin = request.Sin;
                }
                return new StaffVM
                {
                    StaffDto = _StaffDto,
                    Tcounty = Countys,
                    Tagency= agencyss
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
