﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetStaffByOffenderId : IRequest<StaffVM>
    {
        public int Sin { get; set; }
    }


    public class GetStaffByOffenderIdHandler : IRequestHandler<GetStaffByOffenderId, StaffVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetStaffByOffenderIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<StaffVM> Handle(GetStaffByOffenderId request, CancellationToken cancellationToken)
        {
            try
            {

                string sql = @"select * from IJOS.staff 
                                where staff_key in 
                                        (select staff_key from IJOS.jj_links 
                                            where sin in 
                                                    (select sin from IJOS.offender where sin=@sin)) and isnull(IS_ACTIVE,'0')='1' ";

                var dbResults = await _unitOfWork.StaffRepository.QueryAsync
                    (sql, new { sin = (int?)request.Sin });

                var staffs = _mapper.Map< List<StaffDto>>(dbResults);

                foreach (StaffDto item in staffs)
                {
                    try
                    {

                        string by = item.Modified_by;
                        if (by == null)
                            by = item.Created_by;

                        string sql2 = @" select *  from ijos.Aspnetusers where Id =@Id ";
                        var dbResults2 = await _unitOfWork.AspnetusersRepository.QuerySingleAsync
                            (sql2, new { Id = by });

                        item.Modified_by = dbResults2.Username;

                    }
                    catch { }

                    if (item.Modified_date == null)
                        item.Modified_date = item.Created_date;

                }

                return new StaffVM
                {
                    Staffs = staffs
              };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
