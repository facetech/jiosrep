﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;


namespace IJOS.Application
{
        public class StaffDto : IMapFrom<e.Staff>
    {

        public long Staff_key { get; set; }
        public string Login_name { get; set; }
        public string County_name { get; set; }
        public string Agency_name { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Job_title { get; set; }
        public string Supervisor_name { get; set; }
        public string Phone_number { get; set; }
        public string Extension { get; set; }
        public string Fax_number { get; set; }
        public string E_mail_address { get; set; }
        public string Is_active { get; set; }
        public string Agency_branch { get; set; }
        public long? Pass_change_freq { get; set; }
        public long? Pass_change_grace { get; set; }
        public DateTime? Pass_change_date { get; set; }
        public string Mobile_number { get; set; }
        public string Home_number { get; set; }
        public string Account_status { get; set; }
        public string Nick_name { get; set; }
        public string Middle_name { get; set; }
        public long? Supervisor_key { get; set; }
        public string Ora_account { get; set; }

        public DateTime? Created_date { get; set; }

        public string Created_by { get; set; }

        public DateTime? Modified_date { get; set; }

        public string Modified_by { get; set; }

    } 

        
}

