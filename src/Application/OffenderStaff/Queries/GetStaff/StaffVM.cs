﻿using IJOS.Application.TodoLists.Queries.GetTodos;
using IJOS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace IJOS.Application
{
     public class StaffVM 
    {
        public StaffDto StaffDto { get; set; }
        public List<StaffDto> Staffs { get; set; }

      
        public List<string> Tcounty { get; set; }
        public List<string> Tagency { get; set; }

    } 

        
}

