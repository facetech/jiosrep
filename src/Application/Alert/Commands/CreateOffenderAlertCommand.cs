﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.Alert.Queries.GetAlerts;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.Alert.Commands
{
    public class CreateOffenderAlertCommand : IRequest
    {
        public OffenderAlertVM OffenderAlertVM { get; set; }
    }

    public class CreateOffenderAlertCommandHandler : IRequestHandler<CreateOffenderAlertCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateOffenderAlertCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreateOffenderAlertCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.OffenderAlertVM.Juvenile_AlertDto.Created_date = System.DateTime.Now;

                request.OffenderAlertVM.Juvenile_AlertDto.Alert_comment = "** " + System.DateTime.Now + "\n" + request.OffenderAlertVM.Juvenile_AlertDto.Alert_comment;

                var entityDbo_Juvenile_Alert = request.OffenderAlertVM.Juvenile_AlertDto;
                e.Juvenile_Alert entity_Juvenile_Alert = _mapper.Map<e.Juvenile_Alert>(entityDbo_Juvenile_Alert);
                await _unitOfWork.Juvenile_AlertRepository.Insert(entity_Juvenile_Alert, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }


    }
}
