﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using e = IJOS.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;
using System;
using IJOS.Application.Alert.Queries.GetAlerts;

namespace IJOS.Application.Alert.Commands
{
    public class UpdateOffenderAlertCommand : IRequest
    {
        public OffenderAlertVM OffenderAlertVM { get; set; }
    }

    public class UpdateOffenderAlertCommandHandler : IRequestHandler<UpdateOffenderAlertCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateOffenderAlertCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffenderAlertCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.OffenderAlertVM.Juvenile_AlertDto.Modified_date = DateTime.Now;

                request.OffenderAlertVM.Juvenile_AlertDto.Alert_comment = " ** " + System.DateTime.Now + "\n" + request.OffenderAlertVM.Juvenile_AlertDto.Alert_new_comment + "\n\n" + request.OffenderAlertVM.Juvenile_AlertDto.Alert_comment;

                var entityDbo_Juvenile_AlertDto = request.OffenderAlertVM.Juvenile_AlertDto;
                e.Juvenile_Alert entity_Juvenile_AlertDto = _mapper.Map<e.Juvenile_Alert>(entityDbo_Juvenile_AlertDto);
                await _unitOfWork.Juvenile_AlertRepository.Update(entity_Juvenile_AlertDto, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }
    }
}
