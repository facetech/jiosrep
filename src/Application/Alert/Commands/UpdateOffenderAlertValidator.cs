﻿using FluentValidation;
using IJOS.Application.Alert.Queries.GetAlerts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application.Alert.Commands
{
    public class UpdateOffenderAlertValidator : AbstractValidator<UpdateOffenderAlertCommand>
    {
        public UpdateOffenderAlertValidator()
        {
            RuleFor(v => v.OffenderAlertVM.Juvenile_AlertDto.Alert_new_comment)
                .NotEmpty().NotNull().WithMessage(v => "Comment is required.");
        }
    }
}
