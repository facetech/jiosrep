﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using e = IJOS.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;
using System;
using IJOS.Application.Alert.Queries.GetAlerts;
using IJOS.Application.Common.Exceptions;

namespace IJOS.Application.Alert.Commands
{
    public class DeleteOffenderAlertCommand : IRequest
    {
        public int id { get; set; }
    }

    public class DeleteOffenderAlertCommandHandler : IRequestHandler<DeleteOffenderAlertCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DeleteOffenderAlertCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteOffenderAlertCommand request, CancellationToken cancellationToken)
        {
            try
            {
                //select * from [IJOS].Juvenile_Alert where JUVENILE_ALERT_KEY = 

                string sql = @"select * from [IJOS].Juvenile_Alert where JUVENILE_ALERT_KEY =  @juvenile_alert_key";
                var entity = await _unitOfWork.Juvenile_AlertRepository.QuerySingleAsync
                    (sql, new { juvenile_alert_key = request.id });

                if (entity == null)
                {
                    throw new NotFoundException(nameof(e.Juvenile_Alert), entity.Sin);
                }
                _unitOfWork.Juvenile_AlertRepository.Delete(entity);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }
    }
}
