﻿using System;
using e = IJOS.Domain.Entities;
using IJOS.Application.Common.Mappings;

namespace IJOS.Application.Alert.Queries.GetAlerts
{
    public class Juvenile_AlertDto : IMapFrom<e.Juvenile_Alert>
    {
        public long Juvenile_alert_key { get; set; }
        public string Alert_type { get; set; }
        public long Sin { get; set; }
        public string Agency_name { get; set; }
        public string Alert_comment { get; set; }
        public string Alert_new_comment { get; set; }
        public string Additional_info { get; set; }
        public string Is_active { get; set; }
        public DateTime? Inactive_date { get; set; }
        public string Mark_delete { get; set; }
        public long? Staff_key { get; set; }
        public string Created_by { get; set; }
        public DateTime Created_date { get; set; }
        public string Modified_by { get; set; }
        public DateTime? Modified_date { get; set; }
    }
}
