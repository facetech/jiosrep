﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Alert.Queries.GetAlerts
{
    public class GetOffenderAlertBySin : IRequest<OffenderAlertVM>
    {
        public int Sin { get; set; }
        public int Juvenile_Alert_Key { get; set; }
    }

    public class GetOffenderAlertBySinHandler : IRequestHandler<GetOffenderAlertBySin, OffenderAlertVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetOffenderAlertBySinHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderAlertVM> Handle(GetOffenderAlertBySin request, CancellationToken cancellationToken)
        {
            try
            {
                string sql_Offender = @"SELECT * FROM OFFENDER WHERE SIN =@sin";
                var dbResult_Offender = await _unitOfWork.OffenderRepository.
                    QuerySingleAsync(sql_Offender, new { sin = (long?)request.Sin });

                List<Juvenile_AlertDto> juvenile_AlertDtoList = new List<Juvenile_AlertDto>();
                string sql_Juvenile_AlertDto = @" SELECT * FROM JUVENILE_ALERT WHERE SIN = @sin";
                var dbResult_Juvenile_AlertDto = await _unitOfWork.Juvenile_AlertRepository.QueryAsync
                    (sql_Juvenile_AlertDto, new { sin = (long?)request.Sin });
                juvenile_AlertDtoList = _mapper.Map<List<Juvenile_AlertDto>>(dbResult_Juvenile_AlertDto);

                return new OffenderAlertVM
                {
                    Offender_FullName = dbResult_Offender.First_name + " " + " " + dbResult_Offender.Last_name,
                    Juvenile_AlertDtoList = juvenile_AlertDtoList,
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
