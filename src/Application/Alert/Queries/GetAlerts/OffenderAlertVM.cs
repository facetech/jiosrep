﻿using System.Collections.Generic;

namespace IJOS.Application.Alert.Queries.GetAlerts
{
    public class OffenderAlertVM
    {
        public string Offender_FullName { get; set; }

        public Juvenile_AlertDto Juvenile_AlertDto { get; set; }
        public List<Juvenile_AlertDto> Juvenile_AlertDtoList { get; set; }

        public Juvenile_Alert_TypeDto Juvenile_Alert_TypeDto { get; set; }
        public List<Juvenile_Alert_TypeDto> Juvenile_Alert_TypeDtoList { get; set; }
    }
}
