﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Alert.Queries.GetAlerts
{
    public class GetAlertByOffenderAlertID : IRequest<OffenderAlertVM>
    {
        public long Juvenile_Alert_Key { get; set; }
        public int Sin { get; set; }
    }

    public class GetAlertByOffenderAlertIDHandler : IRequestHandler<GetAlertByOffenderAlertID, OffenderAlertVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetAlertByOffenderAlertIDHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderAlertVM> Handle(GetAlertByOffenderAlertID request, CancellationToken cancellationToken)
        {
            try
            {
                Juvenile_AlertDto juvenile_AlertDto = new Juvenile_AlertDto();
                List<Juvenile_Alert_TypeDto> juvenile_Alert_TypeDtoList = new List<Juvenile_Alert_TypeDto>();

                string sql_Offender = @"SELECT * FROM OFFENDER WHERE SIN =@sin";
                var dbResult_Offender = await _unitOfWork.OffenderRepository.
                    QuerySingleAsync(sql_Offender, new { sin = (long?)request.Sin });

                if(request.Juvenile_Alert_Key == 0)
                {
                    juvenile_AlertDto.Created_by = "SYSTEM";
                    juvenile_AlertDto.Created_date = System.DateTime.Now;
                    juvenile_AlertDto.Sin = request.Sin;

                    string sql_Juvenile_Alert_Type = @"SELECT * FROM JUVENILE_ALERT_TYPE";
                    var dbResult_Juvenile_Alert_Type = await _unitOfWork.Juvenile_Alert_TypeRepository.
                        QueryAsync(sql_Juvenile_Alert_Type, new { sin = (long?)request.Sin });
                    juvenile_Alert_TypeDtoList = _mapper.Map<List<Juvenile_Alert_TypeDto>>(dbResult_Juvenile_Alert_Type);
                }
                else
                { 
                   
                    string sql_juvenile_AlertDto = @" SELECT * FROM JUVENILE_ALERT WHERE JUVENILE_ALERT_KEY = @juvenile_alert_key";
                    var dbResult_juvenile_AlertDto = await _unitOfWork.Juvenile_AlertRepository.
                        QuerySingleAsync(sql_juvenile_AlertDto, new { juvenile_alert_key = (long?)request.Juvenile_Alert_Key });
                    juvenile_AlertDto = _mapper.Map<Juvenile_AlertDto>(dbResult_juvenile_AlertDto);
                }
                return new OffenderAlertVM
                {
                    Offender_FullName = dbResult_Offender.First_name + " " + " " + dbResult_Offender.Last_name,
                    Juvenile_AlertDto = juvenile_AlertDto, 
                    Juvenile_Alert_TypeDtoList = juvenile_Alert_TypeDtoList,
                };
                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
