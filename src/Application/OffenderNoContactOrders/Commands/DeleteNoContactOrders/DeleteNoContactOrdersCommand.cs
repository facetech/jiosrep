﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class DeleteNoContactOrdersCommand : IRequest
    {
        public long? INDIVIDUAL_NUMBER  { get; set; }
    }

    public class DeleteNoContactOrdersCommandHandler : IRequestHandler<DeleteNoContactOrdersCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public DeleteNoContactOrdersCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
          // _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<Unit> Handle(DeleteNoContactOrdersCommand request, CancellationToken cancellationToken)
        {
            try
            {

                string sql = @" select *  from ijos.RELATIONSHIPS   where RELATIONSHIP_ID =@individualnumber";

                var entity = await _unitOfWork.RelationshipsRepository.QuerySingleAsync
                    (sql, new { individualnumber = request.INDIVIDUAL_NUMBER });
                entity.Isdeleted = true;
                await _unitOfWork.RelationshipsRepository.Update(entity, cancellationToken);
                return Unit.Value;

                
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
