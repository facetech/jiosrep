﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class CreateNoContactOrdersCommand : IRequest
    {
        public RelationshipsVM RelationshipsVm { get; set; }

    }

    public class CreateNoContactOrdersCommandHandler : IRequestHandler<CreateNoContactOrdersCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public CreateNoContactOrdersCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
          // _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<Unit> Handle(CreateNoContactOrdersCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.RelationshipsVm.RelationshipsDto2;
                e.Relationships entity = _mapper.Map<e.Relationships>(entityDbo);

                await _unitOfWork.RelationshipsRepository.Insert(entity, cancellationToken);

                e.No_contact_order entityRl = new e.No_contact_order();
                entityRl.Sin = entityDbo.SIN;
                entityRl.Individual_number = entity.Relationship_id;
                entityRl.Created_date = System.DateTime.Now;
               
               entityRl.Relationship_to_offender = entityDbo.RELATIONSHIP_TO_OFFENDER;
                await _unitOfWork.No_contact_orderRepository.Insert(entityRl, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
