﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdateNoContactOrdersCommand : IRequest
    {
        public RelationshipsVM RelationshipsVm { get; set; }
    }

    public class UpdateNoContactOrdersCommandHandler : IRequestHandler<UpdateNoContactOrdersCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public UpdateNoContactOrdersCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
          // _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<Unit> Handle(UpdateNoContactOrdersCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.RelationshipsVm.RelationshipsDto2;
                e.Relationships entity = _mapper.Map<e.Relationships>(entityDbo);
                await _unitOfWork.RelationshipsRepository.Update(entity, cancellationToken);


                string sql2 = @" select *  from ijos.NO_CONTACT_ORDER   where INDIVIDUAL_NUMBER =@individualnumber and sin =@sin";

                var entityRl = await _unitOfWork.No_contact_orderRepository.QuerySingleAsync
           (sql2, new { individualnumber = entityDbo.Relationship_id, sin = entityDbo.SIN });
                
                entityRl.Modified_date = System.DateTime.Now;
                entityRl.Relationship_to_offender = entityDbo.RELATIONSHIP_TO_OFFENDER;

                await _unitOfWork.No_contact_orderRepository.Update(entityRl, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
