﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetNoContactOrdersById : IRequest<RelationshipsVM>
    {
        public int Sin { get; set; }
        public long IndividualNumber { get; set; }
    }


    public class GetNoContactOrdersByIdHandler : IRequestHandler<GetNoContactOrdersById, RelationshipsVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        // private readonly IMapper _RelationshipMap;
        public GetNoContactOrdersByIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            //   _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<RelationshipsVM> Handle(GetNoContactOrdersById request, CancellationToken cancellationToken)
        {
            try
            {


                //string sql = @"
                //                select distinct r.*,rt.RELATIONSHIP_TO_OFFENDER ,rt.RELATIONSHIP_TYPE ,rl.SIN  from ijos.RELATIONSHIPS r
                //                left join ijos.RELATIONSHIP_LINKAGE rl
                //                on rl.sin = @sin and rl.RELATIONSHIP_ID=r.RELATIONSHIP_ID
                //                left join IJOS.RELATIONSHIP_TYPES rt on rt.RELATIONSHIP_TO_OFFENDER = rl.RELATIONSHIP_TO_OFFENDER
                //                 where r.RELATIONSHIP_ID =@individualnumber";

                RelationshipsDto2 RelationshipsDto2 = new RelationshipsDto2();


                if (request.IndividualNumber == 0)
                {
                    RelationshipsDto2 = new RelationshipsDto2();

                    RelationshipsDto2.Isdeleted = false;
                    RelationshipsDto2.Interpreter = false;
                    RelationshipsDto2.Message_phone_flag = false;
                    RelationshipsDto2.Other_children_in_home = false;
                    RelationshipsDto2.Guardian_flag = false;
                    RelationshipsDto2.Parent_support_ordered = false;
                    RelationshipsDto2.First_name = "";
                    RelationshipsDto2.Last_name = "";
                    RelationshipsDto2.SIN = request.Sin;

                }
                else
                {
                    string sql = @" select *  from ijos.RELATIONSHIPS   where RELATIONSHIP_ID =@individualnumber";

                    var dbResults = await _unitOfWork.RelationshipsRepository.QuerySingleAsync
                        (sql, new { individualnumber = (long?)request.IndividualNumber });

                    RelationshipsDto2 = _mapper.Map<RelationshipsDto2>(dbResults);
                    RelationshipsDto2.SIN = request.Sin;
                }

                try
                {
                    string sql2 = @" select *  from ijos.NO_CONTACT_ORDER   where INDIVIDUAL_NUMBER =@individualnumber and sin =@sin";

                    var dbResults2 = await _unitOfWork.No_contact_orderRepository.QuerySingleAsync
               (sql2, new { individualnumber = (long?)request.IndividualNumber, sin = (int?)request.Sin });
                    RelationshipsDto2.RELATIONSHIP_TO_OFFENDER = dbResults2.Relationship_to_offender;
                }
                catch
                {
                    RelationshipsDto2.RELATIONSHIP_TO_OFFENDER = "";
                }


                var entityZipCityStateCounty = _unitOfWork.ZipCityStateCountyRepository.GetAll();
                var zipCityStateCounties = _mapper.Map<List<ZipCityStateCountyDto>>(entityZipCityStateCounty);

                List<string> cities = zipCityStateCounties.Select(o => o.City).Distinct().ToList();
                List<string> states = zipCityStateCounties.Select(o => o.State).Distinct().ToList();
                List<string> counties = zipCityStateCounties.Select(o => o.County).Distinct().ToList();


                var Relationship_types = _unitOfWork.Relationship_typesRepository.GetAll();
                List<string> sex = Relationship_types.Select(o => o.Sex).Distinct().ToList();
                var Relationship_typesDto = _mapper.Map<List<Relationship_typesDto>>(Relationship_types);
                try
                {

                    if (RelationshipsDto2.RELATIONSHIP_TYPE != null)
                        RelationshipsDto2.RELATIONSHIP_TYPE = Relationship_types.Where(a => a.Relationship_to_offender == RelationshipsDto2.RELATIONSHIP_TO_OFFENDER).SingleOrDefault().Relationship_type;
                }
                catch
                {
                    RelationshipsDto2.RELATIONSHIP_TYPE = "";
                }
                List<string> Ed_ethnicity = _unitOfWork.EdEthnicityRepository.GetAll().Select(o => o.Ethnicity).Distinct().ToList();
                List<string> Race_types = _unitOfWork.RaceTypesRepository.GetAll().Select(o => o.Race).Distinct().ToList();
                List<string> Relationship_status = _unitOfWork.RelationshipStatusRepository.GetAll().Select(o => o.Status).Distinct().ToList();
                List<string> MaritalStatus = _unitOfWork.MaritalStatusRepository.GetAll().Select(o => o.Status).Distinct().ToList();
                List<string> LanguageTypes = _unitOfWork.LanguageTypesRepository.GetAll().Select(o => o.Language).Distinct().ToList();

                return new RelationshipsVM
                {
                    RelationshipsDto2 = RelationshipsDto2,
                    ZipCityStateCounties = zipCityStateCounties,
                    Cities = cities,
                    States = states,
                    Counties = counties,

                    Ed_ethnicity = Ed_ethnicity,
                    LanguageTypes = LanguageTypes,
                    MaritalStatus = MaritalStatus,
                    Race_types = Race_types,
                    Relationship_status = Relationship_status,
                    Relationship_types = Relationship_typesDto,
                    sex = sex

                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
