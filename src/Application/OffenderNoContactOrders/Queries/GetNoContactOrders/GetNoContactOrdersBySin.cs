﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetNoContactOrdersBySin : IRequest<List<RelationshipsDto2>>
    {
        public int Sin { get; set; }
    }


    public class GetNoContactOrdersBySinHandler : IRequestHandler<GetNoContactOrdersBySin, List<RelationshipsDto2>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetNoContactOrdersBySinHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<RelationshipsDto2>> Handle(GetNoContactOrdersBySin request, CancellationToken cancellationToken)
        {
            try
            {
                List<RelationshipsDto2> relationshipsDto = new List<RelationshipsDto2>();
                string sql = @" select r.*  from ijos.RELATIONSHIPS r  
                                where isnull(Isdeleted,0) = 0 and  RELATIONSHIP_ID in
                                (select INDIVIDUAL_NUMBER from ijos.NO_CONTACT_ORDER where SIN =@sin  ) ";
                var dbResults = await _unitOfWork.RelationshipsRepository.QueryAsync
                    (sql, new { sin = (long?)request.Sin });

                relationshipsDto = _mapper.Map<List<RelationshipsDto2>>(dbResults);
                return relationshipsDto;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
