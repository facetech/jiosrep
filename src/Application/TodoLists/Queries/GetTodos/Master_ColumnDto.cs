﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.TodoLists.Queries.GetTodos
{
    public class Master_ColumnDto : IMapFrom<e.Master_Column>
    {
        public long Masterkey { get; set; }
        public string Rank_offendergang { get; set; }
        public string Initiatedby_offendergang { get; set; }
        public string Status_offendergang { get; set; }
        public int? Grade_schoolhistory { get; set; }
    }
}