﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class CreateOffenderCommentCommand : IRequest
    {
        public Offender_commentVm offender_commentVm { get; set; }

    }

    public class CreateOffenderCommentHandler : IRequestHandler<CreateOffenderCommentCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public CreateOffenderCommentHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreateOffenderCommentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.offender_commentVm.Offender_commentDto;
                e.Offender_comments entity = _mapper.Map<e.Offender_comments>(entityDbo);
                await _unitOfWork.Offender_commentRepository.Insert(entity, cancellationToken);
                
                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
