﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdateOffenderCommentCommand : IRequest
    {
        public Offender_commentVm offender_commentVm { get; set; }

    }

    public class UpdateOffenderCommentHandler : IRequestHandler<UpdateOffenderCommentCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public UpdateOffenderCommentHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffenderCommentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.offender_commentVm.Offender_commentDto;
                e.Offender_comments entity = _mapper.Map<e.Offender_comments>(entityDbo);
             

                await _unitOfWork.Offender_commentRepository.Update(entity, cancellationToken);

                e.Offender_comment_history Offender_comment_history = new e.Offender_comment_history { Offender_comment_id = entity.Offender_comment_id, Comments = entity.Comments, Created_date = System.DateTime.Now, Sin = entity.Sin, Status = "Updated" };


                await _unitOfWork.Offender_comment_historyRepository.Insert(Offender_comment_history, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
