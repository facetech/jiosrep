﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class DeleteOffenderCommentCommand : IRequest
    {

        public long Offender_comment_id { get; set; }
    }

    public class DeleteOffenderCommentCommandHandler : IRequestHandler<DeleteOffenderCommentCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public DeleteOffenderCommentCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteOffenderCommentCommand request, CancellationToken cancellationToken)
        {
            try
            {

                string sql = @" select *  from ijos.OFFENDER_comments   where Offender_comment_id =@Offender_comment_id";
                e.Offender_comments entity = await _unitOfWork.Offender_commentRepository.QuerySingleAsync
                    (sql, new { Offender_comment_id = (long?)request.Offender_comment_id });

                e.Offender_comment_history Offender_comment_history = new e.Offender_comment_history { Comments = entity.Comments, Created_date = System.DateTime.Now, Sin = entity.Sin, Status = "Deleted" };
               
                await _unitOfWork.Offender_comment_historyRepository.Insert(Offender_comment_history, cancellationToken );
             
                _unitOfWork.Offender_commentRepository.Delete(entity);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
