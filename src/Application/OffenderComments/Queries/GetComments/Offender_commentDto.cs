﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{

    public class Offender_commentDto : IMapFrom<e.Offender_comments>
    {
        public long Offender_comment_id { get; set; }
        public long Sin { get; set; }
        public string Comments { get; set; }
        public string Status { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    } 


}