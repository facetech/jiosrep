﻿using AutoMapper;
using IJOS.Application.Common.Interfaces;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetCommentsBySin : IRequest<Offender_commentVm>
    {
        public int Sin { get; set; }
    }


    public class GetCommentsHandler : IRequestHandler<GetCommentsBySin, Offender_commentVm>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetCommentsHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        private readonly ICurrentUserService _currentUserService;
        public async Task<Offender_commentVm> Handle(GetCommentsBySin request, CancellationToken cancellationToken)
        {
            try
            {
                List<Offender_commentDto> Offender_commentDtos = new List<Offender_commentDto>();
                string sql = @" select *  from ijos.OFFENDER_COMMENTS where SIN =@sin ";
                var dbResults = await _unitOfWork.Offender_commentRepository.QueryAsync
                    (sql, new { sin = (long?)request.Sin });

           

                Offender_commentDtos = _mapper.Map<List<Offender_commentDto>>(dbResults);


                foreach (Offender_commentDto item in Offender_commentDtos)
                {
                    try
                    {
                        string sql2 = @" select *  from ijos.Aspnetusers where Id =@Id ";
                        var dbResults2 = await _unitOfWork.AspnetusersRepository.QuerySingleAsync
                            (sql2, new { Id = item.Created_by });

                        item.Created_by = dbResults2.Username;
                    }
                    catch { }

                }

                return new Offender_commentVm {All_Offender_commentDto = Offender_commentDtos,Offender_commentDto = new Offender_commentDto() } ;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
