﻿using IJOS.Application;
using IJOS.Application.TodoLists.Queries.GetTodos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class Offender_commentVm
    {
        public Offender_commentDto Offender_commentDto { get; set; }

        public List<Offender_commentDto> All_Offender_commentDto { get; set; }
    }
}