﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetCommentsById : IRequest<Offender_commentVm>
    {
        public long Offender_comment_id { get; set; }
        public int Sin { get; set; }
        //public long IndividualNumber { get; set; }
    }


    public class GetCommentsByIdHandler : IRequestHandler<GetCommentsById, Offender_commentVm>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
       // private readonly IMapper _RelationshipMap;
        public GetCommentsByIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
         //   _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<Offender_commentVm> Handle(GetCommentsById request, CancellationToken cancellationToken)
        {
            try
            {

                Offender_commentDto Offender_commentDto = new Offender_commentDto();
                if (request.Offender_comment_id == 0)
                {
                    Offender_commentDto = new Offender_commentDto();
                    Offender_commentDto.Created_date = DateTime.Now;
                    Offender_commentDto.Sin = request.Sin;
                }
                else
                {
                    string sql = @" select *  from ijos.OFFENDER_comments   where Offender_comment_id =@Offender_comment_id";
                    var dbResults = await _unitOfWork.Offender_commentRepository.QuerySingleAsync
                        (sql, new { Offender_comment_id = (long?)request.Offender_comment_id });
                    Offender_commentDto = _mapper.Map<Offender_commentDto>(dbResults);
                    Offender_commentDto.Sin = request.Sin;
                }
                return new Offender_commentVm
                {
                    Offender_commentDto = Offender_commentDto
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
