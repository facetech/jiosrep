﻿using IJOS.Application.Common.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
  public  class MaritalStatusDto : IMapFrom<e.MaritalStatus>
    {
        public int? Maritalid { get; set; }
        public string Status { get; set; }
        public string Is_active { get; set; }
        public int? Order_by { get; set; }
    } // class Marital_Status
}

