﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using  e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class OffenderDto : IMapFrom<e.Offender>
    {
        public long Sin { get; set; }
        public string County_name { get; set; }
        public string Probation_status { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Middle_name { get; set; }
        public DateTime Date_of_birth { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public string Hair_color { get; set; }
        public string Eye_color { get; set; }
        public string Sex { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
        public string Place_of_birth { get; set; }
        public string Mothers_maiden_name { get; set; }
        public string Marital_status { get; set; }
        public bool? Isemployed { get; set; }
        public string Paper_file_location { get; set; }
        public bool? Citizen_flag { get; set; }
        public bool? School_access_flag { get; set; }
        public string Primary_language { get; set; }
        public bool? Sealed_flag { get; set; }
        public string Current_location { get; set; }
        public string Medicaid_number { get; set; }
        public string Suffix { get; set; }
        public string Ssn { get; set; }
        public string Countyoffendernumber { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    }
}
