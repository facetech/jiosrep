﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Domain.Entities
{
    public class LanguageTypesDto : IMapFrom<e.LanguageTypes>
    {
        public string Language { get; set; }
        public long? Order_by { get; set; }
        public DateTime created_date { get; set; }
        public DateTime? modified_date { get; set; }
        public string modified_by { get; set; }
        public string created_by { get; set; }

    } // class LANGUAGE_TYPES
}
