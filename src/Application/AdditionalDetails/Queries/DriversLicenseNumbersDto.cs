﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using  e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class DriversLicenseNumbersDto : IMapFrom<e.DriversLicenseNumbers>
    {
        public long Dln_id { get; set; }
        public long? Sin { get; set; }
        public string Drivers_license_number { get; set; }
        public string Drivers_license_state { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    }
}
