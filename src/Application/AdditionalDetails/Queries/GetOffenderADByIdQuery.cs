﻿using AutoMapper;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetOffenderADByIdQuery : IRequest<OffenderADVM >
    {
        public int Sin { get; set; }
    }


    public class GetOffenderADByIdHandler : IRequestHandler<GetOffenderADByIdQuery, OffenderADVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetOffenderADByIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderADVM> Handle(GetOffenderADByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {

                var dbResults = _unitOfWork.OffenderRepository.GetAll().Where(o=>o.Sin==request.Sin).FirstOrDefault();

                var offenderDto = _mapper.Map<OffenderDto>(dbResults);


                var dbResultsDLN = _unitOfWork.DriversLicenseNumbersRepository.GetAll().Where(o => o.Sin == request.Sin).FirstOrDefault();

                var dtoDLN = _mapper.Map<DriversLicenseNumbersDto>(dbResultsDLN);

                ////zipCityStateCounties

                var entityZipCityStateCounty = _unitOfWork.ZipCityStateCountyRepository.GetAll();
                var zipCityStateCounties = _mapper.Map<List<ZipCityStateCountyDto>>(entityZipCityStateCounty);
                List<string> cities = zipCityStateCounties.Select(o => o.City).Distinct().ToList();
                List<string> states = zipCityStateCounties.Select(o => o.State).Distinct().ToList();
                List<string> counties = zipCityStateCounties.Select(o => o.County).Distinct().ToList();

                // LanguageTypes
                var entityLanguageTypes = _unitOfWork.LanguageTypesRepository.GetAll().OrderBy(o => o.Order_by);
                var LanguageTypesList = _mapper.Map<List<LanguageTypesDto>>(entityLanguageTypes);

                var entityMaritalStatus = _unitOfWork.MaritalStatusRepository.GetAll().OrderBy(o => o.Order_by);
                var MaritalStatusList = _mapper.Map<List<MaritalStatusDto>>(entityMaritalStatus);



                return new OffenderADVM
                {
                    Offender = offenderDto,
                    ZipCityStateCounties = zipCityStateCounties,
                    Cities = cities,
                    States = states,
                    Counties = counties,
                    LanguageTypesList = LanguageTypesList,
                    DriversLicenseNumbers = dtoDLN,
                    MaritalStatus= MaritalStatusList,
                };

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
