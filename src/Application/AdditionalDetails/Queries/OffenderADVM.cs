﻿
using IJOS.Domain.Entities;
using System.Collections.Generic;

namespace IJOS.Application
{
    public class OffenderADVM
    {
        public OffenderDto Offender { get; set; }
        public DriversLicenseNumbersDto DriversLicenseNumbers { get; set; }
        public List<ZipCityStateCountyDto> ZipCityStateCounties { get; set; }
        public List<MaritalStatusDto> MaritalStatus { get; set; }        
        public List<string> Cities { get; set; }
        public List<string> States { get; set; }
        public List<string> Counties { get; set; }
        public List<LanguageTypesDto> LanguageTypesList { get; set; }
    }
}
