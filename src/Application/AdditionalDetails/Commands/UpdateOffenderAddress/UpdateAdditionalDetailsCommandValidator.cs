﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class UpdateAdditionalDetailsCommandValidator : AbstractValidator<UpdateAdditionalDetailsCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateAdditionalDetailsCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.offenderADVM.Offender.Ssn)
            .NotEmpty().NotNull()
            .WithMessage(" is required");
            RuleFor(v => v.offenderADVM.Offender.Ssn)                     
              .MaximumLength(9).MaximumLength(9)
              .MustAsync(checkType).WithMessage("Wrong format!");
            RuleFor(v => v.offenderADVM.DriversLicenseNumbers.Drivers_license_number)
             .NotEmpty().NotNull().WithMessage("Drivers License is required.");

            RuleFor(v => v.offenderADVM.Offender.Medicaid_number)
             .NotEmpty().NotNull().WithMessage("Address is required.");
            RuleFor(v => v.offenderADVM.Offender.Medicaid_number)
            .NotEmpty().NotNull().WithMessage("Address is required.");
            RuleFor(v => v.offenderADVM.Offender.Paper_file_location)
     .NotEmpty().NotNull().WithMessage("Address is required.");


            //RuleFor(v => v.OffenderAddressVM.OffenderAddress.city_2)
            //   .NotEmpty().NotNull().WithMessage("City is required.")
            //   .MustAsync(ChickCity)
            //   .WithMessage("City invalid.")

            //   ;
            //RuleFor(v => v.offenderADVM.Offender.SSN)
            //.NotEmpty().NotNull().WithMessage("State is required.")
            //.MustAsync(chickState)
            //       .WithMessage("State invalid.")


            //       ;
            //      RuleFor(v => v..OffenderAddress)
            //.NotEmpty().NotNull().WithMessage("Zip is required.")

            //.MustAsync(ChickZip)
            //       .WithMessage("Zip code invalid.")

            //.MustAsync(ChickMatched)
            //            .WithMessage("they are not matched")
            //;


        }

        private async Task<bool> ChickMatched(OffenderAddressDto offenderAddress, CancellationToken arg2)
        {
            var chk = false;
            if (long.TryParse(offenderAddress.Zip, out long _zip))
            {
                chk = await _context.Zip_City_State_County
        .AnyAsync(l => l.Zipcode == _zip
        && l.City == offenderAddress.City
        && l.County == offenderAddress.County_name
        && l.State == offenderAddress.State
        );
            }
            return chk;
        }

        private async Task<bool> ChickZip(OffenderAddressDto offenderAddress, CancellationToken arg2)
        {
            var chk=false;
            if (long.TryParse(offenderAddress.Zip, out long _zip))
            {
                chk = await _context.Zip_City_State_County
      .AnyAsync(l => l.Zipcode == _zip);
            }       
            return chk;
        }

        private async Task<bool> chickState(string state, CancellationToken arg2)
        {
            return await _context.Zip_City_State_County
          .AnyAsync(l => l.State == state);
        }

        private async Task<bool> checkType(string f, CancellationToken arg2)
        {
            if (long.TryParse(f,out long u))
            {
                if (f.Trim().Length==9)
                {
                    return true;
                }
                
            }
            return false;
        }
      
        public async Task<bool> BeUniqueTitle(string title, CancellationToken cancellationToken)
        {
            return await _context.Offender_Address
                .AllAsync(l => l.Address_line1 != title);
        }
    }
}
