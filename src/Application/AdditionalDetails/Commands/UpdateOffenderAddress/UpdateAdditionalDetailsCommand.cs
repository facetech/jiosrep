﻿using AutoMapper;
using IJOS.Application;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdateAdditionalDetailsCommand : IRequest
    {
        public OffenderADVM offenderADVM { get; set; }
    }

    public class UpdateAdditionalDetailsCommandHandler : IRequestHandler<UpdateAdditionalDetailsCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateAdditionalDetailsCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateAdditionalDetailsCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var inputEntityOffender = request.offenderADVM.Offender;
                var offenderdb = _mapper.Map<e.Offender>(inputEntityOffender);
                await _unitOfWork.OffenderRepository.Update(offenderdb, cancellationToken);

                var inputEntityDriversLicenseNumbers = request.offenderADVM.DriversLicenseNumbers;
                var driversLicenseNumbersdb = _mapper.Map<e.DriversLicenseNumbers>(inputEntityDriversLicenseNumbers);
                await _unitOfWork.DriversLicenseNumbersRepository.Update(driversLicenseNumbersdb, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }
    }
}
