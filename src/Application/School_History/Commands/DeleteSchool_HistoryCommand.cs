﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.School_History.Commands
{
    public class DeleteSchool_HistoryCommand : IRequest
    {
        public long School_History_Key { get; set; }
    }

    public class DeleteSchool_HistoryCommandHandler : IRequestHandler<DeleteSchool_HistoryCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;        

        public DeleteSchool_HistoryCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteSchool_HistoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string sql = @" select *  from IJOS.School_History where School_History_Key = @school_History_key";
                e.School_History entity = await _unitOfWork.School_HistoryRepository.QuerySingleAsync
                    (sql, new { school_History_key = (long?)request.School_History_Key });

                _unitOfWork.School_HistoryRepository.Delete(entity);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
