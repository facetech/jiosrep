﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.School_History.Queries.GetSchool_History;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.School_History.Commands
{
    public class CreateSchool_HistoryCommand : IRequest
    {
        public OffenderSchoolVM OffenderSchoolVM { get; set; }
    }

    public class CreateSchool_HistoryCommandHandler : IRequestHandler<CreateSchool_HistoryCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateSchool_HistoryCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreateSchool_HistoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo_School_History = request.OffenderSchoolVM.School_HistoryDto;
                e.School_History entity_School_History = _mapper.Map<e.School_History>(entityDbo_School_History);
                await _unitOfWork.School_HistoryRepository.Insert(entity_School_History, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }


    }
}
