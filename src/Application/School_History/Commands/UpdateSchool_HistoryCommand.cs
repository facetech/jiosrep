﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.School_History.Queries.GetSchool_History;
using MediatR;
using e = IJOS.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.School_History.Commands
{
    public class UpdateSchool_HistoryCommand : IRequest
    {
        public OffenderSchoolVM OffenderSchoolVM { get; set; }
    }

    public class UpdateSchool_HistoryCommandHandler : IRequestHandler<UpdateSchool_HistoryCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateSchool_HistoryCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateSchool_HistoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo_School_History = request.OffenderSchoolVM.School_HistoryDto;
                e.School_History entity_School_History = _mapper.Map<e.School_History>(entityDbo_School_History);
                await _unitOfWork.School_HistoryRepository.Update(entity_School_History, cancellationToken);                

                //var entityDbo_Schools = request.OffenderSchoolVM.SchoolsDto;
                //e.Schools entity_Schools = _mapper.Map<e.Schools>(entityDbo_Schools);
                //await _unitOfWork.SchoolsRepository.Update(entity_Schools, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }


    }
}
