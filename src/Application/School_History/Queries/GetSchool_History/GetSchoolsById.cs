﻿//using AutoMapper;
//using IJOS.Application.Persistence_Interfaces;
//using MediatR;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace IJOS.Application.School_History.Queries.GetSchool_History
//{
//    public class GetSchoolsById : IRequest<OffenderSchoolVM>
//    {
//    }
//    public class GetAddressesByUserIdHandler : IRequestHandler<GetSchoolsById, OffenderSchoolVM>
//    {
//        private readonly IUnitOfWork _unitOfWork;
//        private readonly IMapper _mapper;

//        public GetAddressesByUserIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
//        {
//            _unitOfWork = unitOfWork;
//            _mapper = mapper;
//        }

//        public async Task<OffenderAddressVM> Handle(GetOffenderAddressesByOffenderId request, CancellationToken cancellationToken)
//        {
//            try
//            {
//                OffenderSchoolVM offenderSchoolVM = new OffenderSchoolVM();

//                string sqlSchools = @"SELECT * FROM IJOS.Schools";
//                //var dbResultOffenderSchool = await _unitOfWork.OffenderSchoolRepository.QuerySingleAsync
//                //    (sqlOffenderSchool, new { sin = (long?)request.Sin });

//                string sql = @"SELECT * FROM IJOS.Schools";
//                var dbResults = _unitOfWork.SchoolsRepository.GetAll();

//                var offenderAddress = new OffenderAddressDto() { Sin = request.Sin };
//                if (dbResults != null)
//                {
//                    offenderAddress = _mapper.Map<OffenderAddressDto>(dbResults);
//                }


//                // offenderFullName
//                sql = @"SELECT * from IJOS.offender
//                                WHERE SIN = @sin";
//                var dbOffender = await _unitOfWork.OffenderOverviewRepository.QuerySingleAsync
//                    (sql, new { sin = (int?)request.Sin });


//                var offenderFullName = dbOffender.First_name + " " + dbOffender.Middle_name + " " + dbOffender.Last_name;

//                // Audit

//                sql = @"SELECT* FROM IJOS.Offender_Address_Audit 
//                        WHERE SIN = @sin
//                          order by CREATED_DATE desc
//                         ";
//                var dbResults_Audit = await _unitOfWork.OffenderAddressAuditRepository?.QueryAsync
//                   (sql
//                   , new { sin = (int?)request.Sin }
//                  );
//                var offenderAddressAudit = _mapper.Map<List<OffenderAddressAuditDto>>(dbResults_Audit);

//                // LivingArrangements
//                var dbResults_LivingArrangements = _unitOfWork.LivingArrangementRepository.GetAll();
//                var livingArrangements = _mapper.Map<List<LivingArrangementsDto>>(dbResults_LivingArrangements);

//                //zipCityStateCounties

//                var entityZipCityStateCounty = _unitOfWork.ZipCityStateCountyRepository.GetAll();
//                var zipCityStateCounties = _mapper.Map<List<ZipCityStateCountyDto>>(entityZipCityStateCounty);
//                List<string> cities = zipCityStateCounties.Select(o => o.City).Distinct().ToList();
//                List<string> states = zipCityStateCounties.Select(o => o.State).Distinct().ToList();
//                List<string> counties = zipCityStateCounties.Select(o => o.County).Distinct().ToList();
//                return new OffenderAddressVM
//                {
//                    fullname = offenderFullName,
//                    OffenderAddress = offenderAddress,
//                    OffenderAddressAudits = offenderAddressAudit,
//                    LivingArrangements = livingArrangements,
//                    ZipCityStateCounties = zipCityStateCounties,
//                    Cities = cities,
//                    States = states,
//                    Counties = counties,
//                };
//            }
//            catch (Exception ex)
//            {

//                throw ex;
//            }
//        }
//    }
//}
