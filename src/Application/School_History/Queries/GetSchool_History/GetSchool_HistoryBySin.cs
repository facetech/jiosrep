﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.School_History.Queries.GetSchool_History
{
    public class GetSchool_HistoryBySin : IRequest<OffenderSchoolVM>
    {
        public int School_History_Key { get; set; }
        public int School_Key { get; set; }
        public int Sin { get; set; }
    }

    public class GetSchool_HistoryHandler : IRequestHandler<GetSchool_HistoryBySin, OffenderSchoolVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetSchool_HistoryHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderSchoolVM> Handle(GetSchool_HistoryBySin request, CancellationToken cancellationToken)
        {
            try
            {
                List<School_HistoryDto> school_HistoryDto = new List<School_HistoryDto>();
                string sqlSchool_History = @" select School_Key,* from School_History where SIN = @sin";
                var dbResultsqlSchool_History = await _unitOfWork.School_HistoryRepository.QueryAsync
                    (sqlSchool_History, new { sin = (long?)request.Sin });
                school_HistoryDto = _mapper.Map<List<School_HistoryDto>>(dbResultsqlSchool_History);

                List<SchoolsDto> schoolsDto = new List<SchoolsDto>();
                string sqlSchools = @" select S.* from School_History SH, Schools S where SH.School_Key = S.School_Key and SH.SIN= @sin";
                var dbResultsqlSchools = await _unitOfWork.SchoolsRepository.QueryAsync
                    (sqlSchools, new { sin = (long?)request.Sin });
                schoolsDto = _mapper.Map<List<SchoolsDto>>(dbResultsqlSchools);

                List<OffenderSchoolDto> offenderSchoolDto = new List<OffenderSchoolDto>();
                string sqlOffenderSchoolDto = @" select S.*, SH.* from School_History SH, Schools S where SH.School_Key = S.School_Key and SH.SIN= @sin";
                var dbResultOffenderSchoolDto = await _unitOfWork.OffenderSchoolRepository.QueryAsync
                    (sqlOffenderSchoolDto, new { sin = (long?)request.Sin });
                offenderSchoolDto = _mapper.Map<List<OffenderSchoolDto>>(dbResultOffenderSchoolDto);

                OffenderSchoolVM offenderSchoolVM = new OffenderSchoolVM();
                offenderSchoolVM.School_HistoryDtoList = school_HistoryDto;
                offenderSchoolVM.SchoolsDtoList = schoolsDto;
                offenderSchoolVM.OffenderSchoolDtoList = offenderSchoolDto;

                return offenderSchoolVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}