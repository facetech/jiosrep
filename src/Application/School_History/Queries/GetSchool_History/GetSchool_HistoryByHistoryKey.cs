﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.TodoLists.Queries.GetTodos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.School_History.Queries.GetSchool_History
{
    public class GetSchool_HistoryByHistoryKey : IRequest<OffenderSchoolVM>
    {
        public long School_History_Key { get; set; }
        public int Sin { get; set; }
    }

    public class GetSchool_HistoryByHistoryKeyHandler : IRequestHandler<GetSchool_HistoryByHistoryKey, OffenderSchoolVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetSchool_HistoryByHistoryKeyHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderSchoolVM> Handle(GetSchool_HistoryByHistoryKey request, CancellationToken cancellationToken)
        {
            try
            {
                School_HistoryDto school_HistoryDto = new School_HistoryDto();
                List<School_HistoryDto> school_HistoryDtoList = new List<School_HistoryDto>();
                SchoolsDto schoolsDto = new SchoolsDto();
                List<SchoolsDto> schoolsDtoList = new List<SchoolsDto>();
                List<string> reason_For_LeavingList = null;
                List<int> gradeList = null;
                List<Master_ColumnDto> master_ColumnDtosList = new List<Master_ColumnDto>();

                string sqlOffenderSchool = @"select * from OFFENDER where SIN =@sin";
                var dbResultOffenderSchool = await _unitOfWork.OffenderSchoolRepository.
                    QuerySingleAsync(sqlOffenderSchool, new { sin = (long?)request.Sin });

                string sql_Master_ColumnDto = "SELECT * FROM MASTER_COLUMN";
                var dbResult_Master_ColumnDto = await _unitOfWork.Master_ColumnRepository.QueryAsync(sql_Master_ColumnDto);
                master_ColumnDtosList = _mapper.Map<List<Master_ColumnDto>>(dbResult_Master_ColumnDto);
                //gradeList = master_ColumnDtosList.Select(o => o.Grade_SchoolHistory).Distinct().ToList();
                

                string sqlSchool_HistoryDtoList = @"SELECT * FROM SCHOOL_HISTORY WHERE SIN = @sin";
                var dbResultsSchool_HistoryDtoList = await _unitOfWork.School_HistoryRepository.QueryAsync
                        (sqlSchool_HistoryDtoList, new { sin = (long?)request.Sin, });
                school_HistoryDtoList = _mapper.Map<List<School_HistoryDto>>(dbResultsSchool_HistoryDtoList);

                string sqlSchoolsDtoList = @" SELECT DISTINCT SCHOOL_NAME, * from SCHOOLS";
                var dbResultsSchoolsDtoList = await _unitOfWork.SchoolsRepository.QueryAsync(sqlSchoolsDtoList);
                schoolsDtoList = _mapper.Map<List<SchoolsDto>>(dbResultsSchoolsDtoList);

                var dbResults_Reason_For_LeavingList = _unitOfWork.Reason_Left_SchoolRepository.GetAll();
                var _reason_For_LeavingList = _mapper.Map<List<Reason_Left_SchoolDto>>(dbResults_Reason_For_LeavingList);
                reason_For_LeavingList = _reason_For_LeavingList.Select(o => o.Reason_for_leaving).Distinct().ToList();

                if (request.School_History_Key == 0)
                {
                    school_HistoryDto.Created_date = DateTime.Now;
                    school_HistoryDto.Sin = request.Sin;
                }
                else
                {
                    string sqlSchool_HistoryDto = @"SELECT * FROM SCHOOL_HISTORY WHERE SCHOOL_HISTORY_KEY = @school_history_key";
                    var dbResultsSchool_HistoryDto = await _unitOfWork.School_HistoryRepository.QuerySingleAsync
                            (sqlSchool_HistoryDto, new { school_history_key = (long?)request.School_History_Key });
                    school_HistoryDto = _mapper.Map<School_HistoryDto>(dbResultsSchool_HistoryDto);

                    string sqlSchoolsDto = @" SELECT * FROM SCHOOLS where SCHOOL_KEY IN 
                            (SELECT SCHOOL_KEY FROM SCHOOL_HISTORY WHERE SCHOOL_HISTORY_KEY = @school_history_key)";
                    var dbResultsSchoolsDto = await _unitOfWork.SchoolsRepository.QuerySingleAsync
                            (sqlSchoolsDto, new { school_history_key = (long?)request.School_History_Key });
                    schoolsDto = _mapper.Map<SchoolsDto>(dbResultsSchoolsDto);
                }

                return new OffenderSchoolVM
                {
                    Offender_FullName = dbResultOffenderSchool.First_name + " " + " " + dbResultOffenderSchool.Last_name,
                    School_HistoryDto = school_HistoryDto,
                    School_HistoryDtoList = school_HistoryDtoList,
                    SchoolsDto = schoolsDto,
                    SchoolsDtoList = schoolsDtoList,
                    Reason_For_LeavingList = reason_For_LeavingList,                   
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}



