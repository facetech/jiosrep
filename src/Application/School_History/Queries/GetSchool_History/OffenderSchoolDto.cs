﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.School_History.Queries.GetSchool_History
{
    public class OffenderSchoolDto : IMapFrom<e.OffenderSchool>
    {
        //IJOS.School_History       
        public long School_history_key { get; set; }
        public long? School_key { get; set; }
        public long? Sin { get; set; }
        public string Last_grade_completed { get; set; }
        public string Reason_for_leaving { get; set; }
        public string Current_enrollment { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
        public DateTime? Date_entered { get; set; }
        public DateTime? Date_left { get; set; }


        //IJOS.Schools
        public string School_name { get; set; }
        public string School_type { get; set; }
        public string Address_line { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Contact_name { get; set; }


        //IJOS.Offender
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Full_name { get; set; }



    }
}
