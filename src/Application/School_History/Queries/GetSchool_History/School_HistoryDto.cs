﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.School_History.Queries.GetSchool_History
{
    public class School_HistoryDto : IMapFrom<e.School_History>
    {
        //IJOS.School_History       
        public long School_history_key { get; set; }
        public long? School_key { get; set; }
        public long? Sin { get; set; }
        public string Last_grade_completed { get; set; }
        public string Reason_for_leaving { get; set; }
        public string Current_enrollment { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
        public DateTime? Date_entered { get; set; }
        public DateTime? Date_left { get; set; }
        public string School_Name { get; set; }



    }
}
