﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.School_History.Queries.GetSchool_History
{
    public class Reason_Left_SchoolDto : IMapFrom<e.Reason_Left_School>
    {
        public string Reason_for_leaving { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    }
}
