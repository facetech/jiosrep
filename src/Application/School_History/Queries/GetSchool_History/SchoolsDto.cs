﻿using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.School_History.Queries.GetSchool_History
{
    public class SchoolsDto : IMapFrom<e.Schools>
    {
        //IJOS.Schools       
        public long? School_key { get; set; }
        public string School_name { get; set; }
        public string School_type { get; set; }
        public string Address_line { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Contact_name { get; set; }
    }
}
