﻿using System.Collections.Generic;

namespace IJOS.Application.School_History.Queries.GetSchool_History
{
    public class OffenderSchoolVM
    {
        public School_HistoryDto School_HistoryDto { get; set; }
        public SchoolsDto SchoolsDto { get; set; }
        public OffenderSchoolDto OffenderSchoolDto { get; set; }

        public List<School_HistoryDto> School_HistoryDtoList { get; set; }
        public List<SchoolsDto> SchoolsDtoList { get; set; }
        public List<OffenderSchoolDto> OffenderSchoolDtoList { get; set; }

        public string Offender_FullName { get; set; }
        public List<string> Reason_For_LeavingList { get; set; }        

    }
}
