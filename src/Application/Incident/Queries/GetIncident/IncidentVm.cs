﻿using IJOS.Application;
using IJOS.Application.TodoLists.Queries.GetTodos;
using IJOS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class IncidentVm
    {
        
       public IncidentDto IncidentDto { get; set; }
        public List<Incident_detailDto> Incident_detailDtos { get; set; }

        public List<Ref_incident_type_category> Ref_incident_type_categories { get; set; }
        public List<Ref_incident_type> Ref_incident_types { get; set; }
        public List<CurrentCategoryTypesLto> AllCheckbox { get; set; }

        public List<CurrentCategoryTypesLto> currentcheckbox { get; set; }

        public List<IncidentOffendersDto> AllIncidentOffendersDto { get; set; }
        

    }
}