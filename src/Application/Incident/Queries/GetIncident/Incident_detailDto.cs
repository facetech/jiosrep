﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{

    public class Incident_detailDto : IMapFrom<e.Incident_detail>
    {
       public long Incident_detail_key { get; set; }
        public long? Incident_number { get; set; }
        public long? Incident_type_id { get; set; }
        public long? Category_id { get; set; }
        public string Description_1 { get; set; }
        public string Description_2 { get; set; }
        public long? Staff_key { get; set; }
        public string Status { get; set; }

        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    } 



}