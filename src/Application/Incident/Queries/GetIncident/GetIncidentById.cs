﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetIncidentById : IRequest<IncidentVm>
    {
        public long? INCIDENT_NUMBER { get; set; }

    }


    public class GetIncidentdetailByIdHandler : IRequestHandler<GetIncidentById, IncidentVm>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetIncidentdetailByIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IncidentVm> Handle(GetIncidentById request, CancellationToken cancellationToken)
        {
            try
            {
                string sq = "";
                List<Ref_incident_type_category> categories = new List<Ref_incident_type_category>();
                List<Ref_incident_type> types = new List<Ref_incident_type>();



                IncidentDto IncidentDto = new IncidentDto();
               List<Incident_detailDto> Incident_detailDtos = new List<Incident_detailDto>();
                List<IncidentOffendersDto> IncidentOffendersDtos = new List<IncidentOffendersDto>();

                sq = @" select *  from IJOS.Ref_incident_type_category where isnull(IS_ACTIVE,0) =1 ";
                var dbResults_cat = await _unitOfWork.Ref_incident_type_categoryRepository.QueryAsync(sq);
                categories = _mapper.Map<List<Ref_incident_type_category>>(dbResults_cat);

                sq = @" select *  from IJOS.Ref_incident_type ";
                var dbResults_typ = await _unitOfWork.Ref_incident_typeRepository.QueryAsync(sq);
                types = _mapper.Map<List<Ref_incident_type>>(dbResults_typ);

                if (request.INCIDENT_NUMBER == 0)
                {



                    IncidentDto = new IncidentDto();
                    IncidentDto.Created_date = DateTime.Now;
                    IncidentDto.Date_of_incident = DateTime.Now;
                    Incident_detailDtos = new List<Incident_detailDto>();



                    string sql2 = @" select convert(bit,0) as LogFor,*  from IJOS.IncidentOffenders ";
                    var dbResults2 = await _unitOfWork.IncidentOffendersRepository.QueryAsync(sql2);
                    IncidentOffendersDtos = _mapper.Map<List<IncidentOffendersDto>>(dbResults2);

                }
                else
                {
                //    sq = @" select *  from IJOS.Ref_incident_type_category  where TYPE_TO_CATEGORY_ID in
                //(select CATEGORY_ID from ijos.INCIDENT_DETAIL where  INCIDENT_NUMBER =@INCIDENT_NUMBER)";
                //    var dbResults_cat = await _unitOfWork.Ref_incident_type_categoryRepository.QueryAsync
                //    (sq, new { INCIDENT_NUMBER = (long?)request.INCIDENT_NUMBER });
                //    categories = _mapper.Map<List<Ref_incident_type_category>>(dbResults_cat);

                //    sq = @"  select *  from IJOS.Ref_incident_type  where INCIDENT_TYPE_ID in
                //(select INCIDENT_TYPE_ID from ijos.INCIDENT_DETAIL where  INCIDENT_NUMBER =@INCIDENT_NUMBER)";
                //    var dbResults_typ = await _unitOfWork.Ref_incident_typeRepository.QueryAsync(sq, new { INCIDENT_NUMBER = (long?)request.INCIDENT_NUMBER });
                //    types = _mapper.Map<List<Ref_incident_type>>(dbResults_typ);



                    // string sql = @" select *  from IJOS.INCIDENT_DETAIL   where INCIDENT_DETAIL_KEY =@Incident_detail_key";
                    string sql = @" select *  from IJOS.INCIDENT   where INCIDENT_NUMBER =@INCIDENT_NUMBER";
                    var dbResults = await _unitOfWork.IncidentRepository.QuerySingleAsync
                        (sql, new { INCIDENT_NUMBER = (long?)request.INCIDENT_NUMBER });
                    IncidentDto = _mapper.Map<IncidentDto>(dbResults);


                    string sql2 = @" select *  from IJOS.INCIDENT_DETAIL   where INCIDENT_NUMBER =@INCIDENT_NUMBER";
                    var dbResults2 = await _unitOfWork.Incident_detailRepository.QueryAsync
                    (sql2, new { INCIDENT_NUMBER = (long?)request.INCIDENT_NUMBER });
                    Incident_detailDtos = _mapper.Map<List<Incident_detailDto>>(dbResults2);

                    // Incident_detailDto.Sin = request.Sin;
                }
                List<CurrentCategoryTypesLto> AllCheckbox = new List<CurrentCategoryTypesLto>();
                foreach (var cat in categories)
                {
                    foreach (var typ in types)
                    {
                        if (cat.Type_to_category_id == typ.Type_to_Category_Id)
                        {
                            Boolean seletced = false;
                            if (Incident_detailDtos.Where(a => a.Category_id == cat.Type_to_category_id && a.Incident_type_id == typ.Incident_type_id).ToList().Count() > 0)
                                seletced = true;
                            AllCheckbox.Add(new CurrentCategoryTypesLto
                            {
                                Category_Id = cat.Type_to_category_id,
                                Category_Name = cat.Incident_category,
                                IsSelected = seletced,
                                Type_Desc = typ.Incident_description,
                                Type_Id = typ.Incident_type_id,
                                Type_Name = typ.Incident_type
                            });
                        }
                        
                    }
                }
                return new IncidentVm
                {
                    Incident_detailDtos = Incident_detailDtos
                    , IncidentDto = IncidentDto
                    , Ref_incident_types = types
                    , Ref_incident_type_categories = categories
                    , AllCheckbox = AllCheckbox                  
                    ,currentcheckbox = new List<CurrentCategoryTypesLto>()
                    ,AllIncidentOffendersDto=IncidentOffendersDtos
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
