﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class GetIncidentBySin : IRequest<List<IncidentDto>>
    {
        public int Sin { get; set; }
    }


    public class GetIncidentBySinHandler : IRequestHandler<GetIncidentBySin, List<IncidentDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetIncidentBySinHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<IncidentDto>> Handle(GetIncidentBySin request, CancellationToken cancellationToken)
        {
            try
            {
                List<IncidentDto> IncidentDtos = new List<IncidentDto>();
                string sql = @" select *  from ijos.INCIDENT where SIN =@sin ";
                var dbResults = await _unitOfWork.IncidentRepository.QueryAsync
                    (sql, new { sin = (long?)request.Sin });


                IncidentDtos = _mapper.Map<List<IncidentDto>>(dbResults);


                foreach (IncidentDto item in IncidentDtos)
                {
                    try
                    {
                       
                        string by = item.Modified_by;

                        if (by == null)
                            by = item.Created_by;

                        string sql2 = @" select *  from ijos.Aspnetusers where Id =@Id ";
                        var dbResults2 = await _unitOfWork.AspnetusersRepository.QuerySingleAsync
                            (sql2, new { Id = by });

                        item.Modified_by = dbResults2.Username;

                    }
                    catch { }

                    if (item.Modified_date == null)
                        item.Modified_date = item.Created_date;

                }

                //foreach (IncidentDto item in IncidentDtos)
                //{
                //    string sql2 = @" select *  from ijos.Aspnetusers where Id =@Id ";
                //    var dbResults2 = await _unitOfWork.AspnetusersRepository.QuerySingleAsync
                //        (sql2, new { Id = item.Updated_by });

                //    item.Updated_by = dbResults2.Username;
                //}

                return IncidentDtos;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
