﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{



    public class IncidentDto : IMapFrom<e.Incident>
    {
        public string Time_of_incident { get; set; }
        public string Staff_Involved { get; set; }

        public long Incident_number { get; set; }
        public DateTime Date_of_incident { get; set; }
        public long Sin { get; set; }
        public string Description { get; set; }
        public string Location_of_incident { get; set; }
        public string Incident_status { get; set; }
        public long? Group_incident_id { get; set; }
        public string Witnesses { get; set; }

        public DateTime? Created_date { get; set; }

        public string Created_by { get; set; }

        public DateTime? Modified_date { get; set; }

        public string Modified_by { get; set; }
    } 



}