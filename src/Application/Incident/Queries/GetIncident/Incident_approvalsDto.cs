﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{

    public class Incident_approvalsDto : IMapFrom<e.Incident_approvals>
    {
        public long Approval_number { get; set; }
        public long Incident_number { get; set; }
        public string Sent_to { get; set; }
        public string Sent_as { get; set; }
        public string Action_type { get; set; }
        public string Comments { get; set; }
        public DateTime? Created_date { get; set; }

        public string Created_by { get; set; }

        public DateTime? Modified_date { get; set; }

        public string Modified_by { get; set; }

    }



}