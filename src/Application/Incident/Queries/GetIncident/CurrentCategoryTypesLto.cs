﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{



    public class CurrentCategoryTypesLto
    {
        public Boolean IsSelected { get; set; }
        public long? Category_Id { get; set; }
        public string Category_Name { get; set; }
        public long? Type_Id { get; set; }
        public string Type_Name { get; set; }
        public string Type_Desc { get; set; }

    }

   


}