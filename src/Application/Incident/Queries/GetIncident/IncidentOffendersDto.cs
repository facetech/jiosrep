﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{



    public class IncidentOffendersDto : IMapFrom<e.IncidentOffenders>
    {

        public bool LogFor { get; set; }
        public string Room { get; set; }
        public string Group { get; set; }
        public string Juvenile { get; set; }
        public long Ijos { get; set; }
        public string Gender { get; set; }
        public DateTime Dob { get; set; }
        public decimal? Age { get; set; }

        public DateTime? Created_date { get; set; }

        public string Created_by { get; set; }

        public DateTime? Modified_date { get; set; }

        public string Modified_by { get; set; }
    } 



}