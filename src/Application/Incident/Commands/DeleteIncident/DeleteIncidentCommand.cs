﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class DeleteIncidentCommand : IRequest
    {

        public long INCIDENT_NUMBER { get; set; }
    }

    public class DeleteIncidentCommandHandler : IRequestHandler<DeleteIncidentCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DeleteIncidentCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteIncidentCommand request, CancellationToken cancellationToken)
        {
            try
            {

                string sql = @" select *  from ijos.INCIDENT_DETAIL   where INCIDENT_NUMBER =@INCIDENT_NUMBER";
                var entity_Dets = await _unitOfWork.Incident_detailRepository.QueryAsync
                (sql, new { INCIDENT_NUMBER = (long?)request.INCIDENT_NUMBER });


                foreach (var item in entity_Dets)
                {
                    _unitOfWork.Incident_detailRepository.Delete(item);
                }


                sql = @" select *  from ijos.INCIDENT   where INCIDENT_NUMBER =@INCIDENT_NUMBER";
                var entity = await _unitOfWork.IncidentRepository.QuerySingleAsync
                (sql, new { INCIDENT_NUMBER = (long?)request.INCIDENT_NUMBER });
                _unitOfWork.IncidentRepository.Delete(entity);



                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
