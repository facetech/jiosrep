﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;
using IJOS.Domain.Entities;
using System.Linq;
using System.Text;

namespace IJOS.Application
{
    public class UpdateIncidentCommand : IRequest
    {
        public IncidentVm IncidentVm { get; set; }

    }

    public class UpdateIncidentCommandHandler : IRequestHandler<UpdateIncidentCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UpdateIncidentCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateIncidentCommand request, CancellationToken cancellationToken)
        {
            try
            {

                var entityDbo = request.IncidentVm.IncidentDto;
                try
                {
                    DateTime dt = new DateTime(entityDbo.Date_of_incident.Year, entityDbo.Date_of_incident.Month, entityDbo.Date_of_incident.Day
                        , int.Parse(entityDbo.Time_of_incident.Substring(0, 2)), int.Parse(entityDbo.Time_of_incident.Substring(2, 2)), int.Parse(entityDbo.Time_of_incident.Substring(4, 2)));

                    entityDbo.Date_of_incident = dt;
                }
                catch { }

                e.Incident entity = _mapper.Map<e.Incident>(entityDbo);
                await _unitOfWork.IncidentRepository.Update(entity, cancellationToken);


                var entityDbo_Dets = request.IncidentVm.Incident_detailDtos;
                List<e.Incident_detail> entity_Dets = _mapper.Map<List<e.Incident_detail>>(entityDbo_Dets);

                foreach (var item in request.IncidentVm.AllCheckbox)
                {

                    //add
                    if (item.IsSelected == true)
                    {
                        if (entity_Dets.Where(a => a.Category_id == item.Category_Id && a.Incident_type_id == item.Type_Id).ToList().Count() <= 0)
                        {
                            Incident_detail i = new Incident_detail { Created_date = DateTime.Now, Category_id = item.Category_Id, Incident_type_id = item.Type_Id };
                            entity_Dets.Add(i);

                            i.Incident_number = entity.Incident_number;
                            await _unitOfWork.Incident_detailRepository.Insert(i, cancellationToken);
                        }
                    }
                    //delete
                    else if (item.IsSelected == false)
                    {
                        if (entity_Dets.Where(a => a.Category_id == item.Category_Id && a.Incident_type_id == item.Type_Id).ToList().Count() > 0)
                        {
                            Incident_detail i = entity_Dets.Where(a => a.Category_id == item.Category_Id && a.Incident_type_id == item.Type_Id).SingleOrDefault();
                            entity_Dets.Remove(i);
                            _unitOfWork.Incident_detailRepository.Delete(i);
                           
                        }
                    }

                }


  



                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
