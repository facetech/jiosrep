﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.PhysicalDescription.Queries.GetPhysicalDescription;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.PhysicalDescription.Commands.UpdateOffenderPhysicalDescription
{
    public class UpdateOffenderPhysicalDescriptionCommand : IRequest
    {
        public OffenderPhysicalDescriptionVM OffenderPhysicalDescriptionVM { get; set; }
    }

    public class UpdateTodoListCommandHandler : IRequestHandler<UpdateOffenderPhysicalDescriptionCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateTodoListCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffenderPhysicalDescriptionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var inputEntity = request.OffenderPhysicalDescriptionVM.OffenderPhysicalDescription;
                var entity = await _unitOfWork.OffenderPhysicalDescriptionRepository.GetById(inputEntity.Sin);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(e.Offender), inputEntity.Sin);
                }

                var offenderPhysicalDescription = _mapper.Map<OffenderPhysicalDescriptionDto, e.OffenderPhysicalDescription>(inputEntity, entity);

                await _unitOfWork.OffenderPhysicalDescriptionRepository.Update(offenderPhysicalDescription, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }
    }
}
