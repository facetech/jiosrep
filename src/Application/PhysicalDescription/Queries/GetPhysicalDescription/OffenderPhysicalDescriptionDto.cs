﻿using IJOS.Application.Common.Mappings;
using System;
using  e = IJOS.Domain.Entities;

namespace IJOS.Application.PhysicalDescription.Queries.GetPhysicalDescription
{
    public class OffenderPhysicalDescriptionDto : IMapFrom<e.OffenderPhysicalDescription>
    {
        public long Sin { get; set; }
        public DateTime Date_of_birth { get; set; }

        public string Eye_color { get; set; }
        public string County_name { get; set; }
        public string Probation_status { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
        public string Sex_3 { get; set; }
        public string Supervision { get; set; }
        public string Street_address { get; set; }
        public string City_2 { get; set; }
        public string State_2 { get; set; }
        public string Zip_2 { get; set; }
        public string Phone { get; set; }
        public string Height { get; set; }
        public string Hair_color { get; set; }
        public string Work_phone_2 { get; set; }
        public string email { get; set; }
        public string Weight { get; set; }
        public string City_state_zip
        {
            get
            {
                return !string.IsNullOrEmpty(City_2) && !string.IsNullOrEmpty(State_2) && !string.IsNullOrEmpty(Zip_2) ?
                     $"{ City_2}, {State_2} {Zip_2}" : string.Empty;
            }
        }

        public string Full_name
        {
            get
            {
                return !string.IsNullOrEmpty(Last_name) && !string.IsNullOrEmpty(Last_name) ?
                     $"{ Last_name}, {First_name}" : string.Empty;
            }
        }

        public int Age
        {
            get
            {
                DateTime now = DateTime.Today;

                int age = now.Year - Date_of_birth.Year;

                if (now.Month < Date_of_birth.Month || (now.Month == Date_of_birth.Month && now.Day < Date_of_birth.Day))
                    age--;

                return age;
            }
        }

    }
}
