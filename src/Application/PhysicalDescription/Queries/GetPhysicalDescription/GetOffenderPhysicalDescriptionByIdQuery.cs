﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.PhysicalDescription.Queries.GetPhysicalDescription
{
    public class GetOffenderPhysicalDescriptionByIdQuery : IRequest<OffenderPhysicalDescriptionVM>
    {
        public int Sin { get; set; }
    }


    public class GetOffenderPhysicalDescriptionByIdQueryHandler : IRequestHandler<GetOffenderPhysicalDescriptionByIdQuery, OffenderPhysicalDescriptionVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetOffenderPhysicalDescriptionByIdQueryHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderPhysicalDescriptionVM> Handle(GetOffenderPhysicalDescriptionByIdQuery request, CancellationToken cancellationToken)
        {
            var dbResults = await _unitOfWork.OffenderPhysicalDescriptionRepository.QuerySingleAsync
                    (@"SELECT O.* from[IJOS].OFFENDER O

                     WHERE O.SIN = @sin"
                , new { sin = (int?)request.Sin });

            // offenderFullName
            string sql = @"SELECT * from IJOS.offender
                                WHERE SIN = @sin";
            var dbOffender = await _unitOfWork.OffenderRepository.QuerySingleAsync
                (sql, new { sin = (int?)request.Sin });


            var offenderFullName = dbOffender.First_name + " " + dbOffender.Middle_name + " " + dbOffender.Last_name;

            var offenderPhysicalDescriptionDto = _mapper.Map<OffenderPhysicalDescriptionDto>(dbResults);

            return new OffenderPhysicalDescriptionVM
            {
                OffenderPhysicalDescription = offenderPhysicalDescriptionDto,
                fullname = offenderFullName
            };
        }
    }
}
