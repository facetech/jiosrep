﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;
namespace IJOS.Application
{


    public class Relationship_typesDto : IMapFrom<e.Relationship_types>
    {
        
        public string Relationship_to_offender { get; set; }
        public string Sex { get; set; }
        public string Includes { get; set; }
        public string Relationship_type { get; set; }
        public long? Order_by { get; set; }
    } // class RELATIONSHIP_TYPES

   
}