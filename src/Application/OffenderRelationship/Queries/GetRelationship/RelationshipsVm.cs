﻿using IJOS.Application;
using IJOS.Application.TodoLists.Queries.GetTodos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class RelationshipsVM
    {

        //??????????
       /// public long SIN { get; set; }
        //??????????
        public RelationshipsDto2 RelationshipsDto2 { get; set; }

        public List<ZipCityStateCountyDto> ZipCityStateCounties { get; set; }
        public List<string> Cities { get; set; }
        public List<string> States { get; set; }
        public List<string> Counties { get; set; }

        public List<Relationship_typesDto> Relationship_types { get; set; }
        public List<string> Ed_ethnicity { get; set; }
        public List<string> Race_types { get; set; }
        public List<string> Relationship_status { get; set; }
        public List<string> MaritalStatus { get; set; }
        public List<string> LanguageTypes { get; set; }
        public List<string> sex { get; set; }

    }
}