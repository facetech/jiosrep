﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{


    public class Race_typesDto : IMapFrom<e.Race_types>
    {
        public string Race { get; set; }
        public float Order_by { get; set; }
    } // class RACE_TYPES
}