﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{


    public class Relationship_statusDto : IMapFrom<e.Relationship_status>
    {
        public string Status { get; set; }
    } // class RELATIONSHIP_STATUS
}