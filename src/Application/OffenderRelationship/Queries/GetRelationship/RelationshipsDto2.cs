﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class RelationshipsDto2 : IMapFrom<e.Relationships>
    {
        public long Relationship_id { get; set; }
        public string Status { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Middle_name { get; set; }
        public DateTime? Date_of_birth { get; set; }
        public string Sex { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
        public string Address_line1 { get; set; }
        public string Address_line2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone_number { get; set; }
        public string Work_phone { get; set; }
        public string Contact_name { get; set; }
        public string Place_of_business { get; set; }
        public string Occupation { get; set; }
        public string Marital_status { get; set; }
        public bool? Parent_support_ordered { get; set; }
        public long? Parent_support_amount { get; set; }
        public bool? Guardian_flag { get; set; }
        public string E_mail_address { get; set; }
        public string Social_security_number { get; set; }
        public decimal? Monthly_income { get; set; }
        public bool? Other_children_in_home { get; set; }
        public string Work_phone_extension { get; set; }
        public bool? Message_phone_flag { get; set; }
        public string Suffix { get; set; }
        public string Cell_phone { get; set; }
        public string Mailing_address_line1 { get; set; }
        public string Mailing_address_line2 { get; set; }
        public string Mailing_city { get; set; }
        public string Mailing_state { get; set; }
        public string Mailing_zip { get; set; }
        public string Primary_language { get; set; }
        public bool? Interpreter { get; set; }
        public bool? Isdeleted { get; set; }
        public string Created_by { get; set; }
        public DateTime? Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }

    

        public long SIN { get; set; }
        public string RELATIONSHIP_TYPE { get; set; }
        public string RELATIONSHIP_TO_OFFENDER { get; set; }
        
 

       

    }
}