﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{


    public class Ed_ethnicityDto : IMapFrom<e.Ed_ethnicity>
    {
        public float Ethnicity_id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string Ethnicity { get; set; }
    } // class ED_ETHNICITY
}