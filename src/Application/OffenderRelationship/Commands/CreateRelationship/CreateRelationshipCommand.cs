﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class CreateRelationshipCommand : IRequest
    {
        public RelationshipsVM RelationshipsVm { get; set; }

    }

    public class CreateOffenderRelationshipCommandHandler : IRequestHandler<CreateRelationshipCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public CreateOffenderRelationshipCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
          // _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<Unit> Handle(CreateRelationshipCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.RelationshipsVm.RelationshipsDto2;
                e.Relationships entity = _mapper.Map<e.Relationships>(entityDbo);

                await _unitOfWork.RelationshipsRepository.Insert(entity, cancellationToken);

                e.Relationship_linkage entityRl = new e.Relationship_linkage();
                entityRl.Sin = entityDbo.SIN;
                entityRl.Relationship_id = entity.Relationship_id;
                entityRl.Created_date = System.DateTime.Now;
              
               entityRl.Relationship_to_offender = entityDbo.RELATIONSHIP_TO_OFFENDER;
                await _unitOfWork.Relationship_linkageRepository.Insert(entityRl, cancellationToken);

                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
