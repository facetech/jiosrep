﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class CreaterRelationshipCommandValidator : AbstractValidator<CreateRelationshipCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreaterRelationshipCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.Last_name)
             .NotEmpty().NotNull().WithMessage(v => v.RelationshipsVm.RelationshipsDto2.Last_name + "Last name is required.");

            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.First_name)
    .NotEmpty().NotNull().WithMessage(v => v.RelationshipsVm.RelationshipsDto2.First_name + "First name is required.");

            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.Date_of_birth)
.NotEmpty().NotNull().WithMessage(v => v.RelationshipsVm.RelationshipsDto2.Date_of_birth + "Date of birth is required.");

            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.Social_security_number)
.NotEmpty().NotNull().Length(9).WithMessage(v => v.RelationshipsVm.RelationshipsDto2.Social_security_number + "invalid Social security number ");


            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.E_mail_address)
.EmailAddress().WithMessage(v => v.RelationshipsVm.RelationshipsDto2.E_mail_address + "invalid Email address");


            //      RuleFor(v => v.RelationshipsVM.OffenderAddress.address_2)
            //          .NotEmpty().NotNull().WithMessage(v=>v.OffenderAddressVM.OffenderAddress.address_2+ "Address is required.")
            //            //.MaximumLength(1).WithMessage("Title must not exceed 200 characters.")
            //            //.MustAsync(BeUniqueTitle).WithMessage("The specified title already exists.")
            //            ;
            //      RuleFor(v => v.OffenderAddressVM.OffenderAddress.city_2)
            //         .NotEmpty().NotNull().WithMessage("City is required.")
            //         .MustAsync(ChickCity)
            //         .WithMessage("City invalid.")

            //         ;
            //      RuleFor(v => v.OffenderAddressVM.OffenderAddress.state_2)
            //      .NotEmpty().NotNull().WithMessage("State is required.")
            //      .MustAsync(chickState)
            //             .WithMessage("State invalid.")


            //             ;
            //      RuleFor(v => v.OffenderAddressVM.OffenderAddress)
            //.NotEmpty().NotNull().WithMessage("Zip is required.")

            //.MustAsync(ChickZip)
            //       .WithMessage("Zip code invalid.")

            //.MustAsync(ChickMatched)
            //            .WithMessage("they are not matched")
            ;


        }

     


    }
}
