﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class UpdateOffenderRelationshipCommandValidator : AbstractValidator<UpdateOffenderRelationshipCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateOffenderRelationshipCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.Last_name)
                .NotEmpty().NotNull().WithMessage(v => v.RelationshipsVm.RelationshipsDto2.Last_name + "Last name is required.");

            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.First_name)
    .NotEmpty().NotNull().WithMessage(v => v.RelationshipsVm.RelationshipsDto2.First_name + "First name is required.");

            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.Date_of_birth)
.NotEmpty().NotNull().WithMessage(v => v.RelationshipsVm.RelationshipsDto2.Date_of_birth + "Date of birth is required.");

            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.Social_security_number)
.NotEmpty().NotNull().Length(9).WithMessage(v => v.RelationshipsVm.RelationshipsDto2.Social_security_number + "invalid Social security number ");


            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.E_mail_address)
.EmailAddress().WithMessage(v => v.RelationshipsVm.RelationshipsDto2.E_mail_address + "invalid Email address");

//            RuleFor(v => v.RelationshipsVm.RelationshipsDto2.Phone_number).InclusiveBetween(1, 10000000000000000000000)
//.GreaterThan<string>(0).WithMessage(v => v.RelationshipsVm.RelationshipsDto2.Phone_number + "invalid Phone number");

            //RuleFor(v => v.RelationshipsVm.RelationshipsDto2.Phone_number)
            //   .Custom((v, context) =>
            //   {
            //       if ((!(int.TryParse(v, out int value)) || value < 0))
            //       {
            //           context.AddFailure($"{v} is not a valid number or less than 0");
            //       }
            //   });

            //.MaximumLength(1).WithMessage("Title must not exceed 200 characters.")
            //.MustAsync(BeUniqueTitle).WithMessage("The specified title already exists.")
            ;
            //      RuleFor(v => v.OffenderAddressVM.OffenderAddress.city_2)
            //         .NotEmpty().NotNull().WithMessage("City is required.")
            //         .MustAsync(ChickCity)
            //         .WithMessage("City invalid.")

            //         ;
            //      RuleFor(v => v.OffenderAddressVM.OffenderAddress.state_2)
            //      .NotEmpty().NotNull().WithMessage("State is required.")
            //      .MustAsync(chickState)
            //             .WithMessage("State invalid.")


            //             ;
            //      RuleFor(v => v.OffenderAddressVM.OffenderAddress)
            //.NotEmpty().NotNull().WithMessage("Zip is required.")

            //.MustAsync(ChickZip)
            //       .WithMessage("Zip code invalid.")

            //.MustAsync(ChickMatched)
            //            .WithMessage("they are not matched")
            ;


        }

        private async Task<bool> ChickMatched(RelationshipsDto2 Relationships, CancellationToken arg2)
        {
            var chk = false;
            if (long.TryParse(Relationships.Zip, out long _zip))
            {
                chk = await _context.Zip_City_State_County
        .AnyAsync(l => l.Zipcode == _zip
        && l.City == Relationships.City
        //&& l.County == Relationships.country
        && l.State == Relationships.State
        );
            }
            return chk;
        }

        private async Task<bool> ChickZip(  RelationshipsDto2 Relationships, CancellationToken arg2)
        {
            var chk=false;
            if (long.TryParse(Relationships.Zip, out long _zip))
            {
                chk = await _context.Zip_City_State_County
      .AnyAsync(l => l.Zipcode == _zip);
            }       
            return chk;
        }

        private async Task<bool> chickState(string state, CancellationToken arg2)
        {
            return await _context.Zip_City_State_County
          .AnyAsync(l => l.State == state);
        }

        private async Task<bool> ChickCity(string city, CancellationToken arg2)
        {
            bool chk = await _context.Zip_City_State_County
           .AnyAsync(l => l.City == city);
            return  chk;
        }

 
    }
}
