﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class DeleteOffenderRelationshipCommand : IRequest
    {
        public RelationshipsVM RelationshipsVm { get; set; }
    }

    public class DeleteOffenderRelationshipCommandHandler : IRequestHandler<DeleteOffenderRelationshipCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public DeleteOffenderRelationshipCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
          // _RelationshipMap = OffenderRelationship_MapProfile.config.CreateMapper();
        }

        public async Task<Unit> Handle(DeleteOffenderRelationshipCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.RelationshipsVm.RelationshipsDto2;

                
                e.Relationships entity = _mapper.Map<e.Relationships>(entityDbo);
              await  _unitOfWork.RelationshipsRepository.Update(entity);
                _unitOfWork.RelationshipsRepository.Delete(entity);
                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
