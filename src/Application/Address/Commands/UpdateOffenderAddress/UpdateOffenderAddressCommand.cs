﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class UpdateOffenderAddressCommand : IRequest
    {
        public OffenderAddressVM OffenderAddressVM { get; set; }
    }

    public class UpdateOffenderAddressCommandHandler : IRequestHandler<UpdateOffenderAddressCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateOffenderAddressCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffenderAddressCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var inputEntity = request.OffenderAddressVM.OffenderAddress;
                var offenderAddressdb = _mapper.Map<e.OffenderAddress>(inputEntity);
                await _unitOfWork.OffenderAddressRepository.Update(offenderAddressdb, cancellationToken);
                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }
    }
}
