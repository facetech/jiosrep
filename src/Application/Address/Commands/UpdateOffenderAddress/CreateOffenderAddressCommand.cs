﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Common.Interfaces;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class CreateOffenderAddressCommand : IRequest<long>
    {
        public OffenderAddressVM OffenderAddressVM { get; set; }
    }

    public class CreateOffenderAddressCommandHandler : IRequestHandler<CreateOffenderAddressCommand,long>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public CreateOffenderAddressCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<long> Handle(CreateOffenderAddressCommand request, CancellationToken cancellationToken)
        {
            try
            {

                var inputEntity = request.OffenderAddressVM.OffenderAddress;
                var entity = new e.OffenderAddress();
                var db = _mapper.Map(inputEntity, entity);
                _context.Offender_Address.Add(db);
                await _context.SaveChangesAsync(cancellationToken);
                return db.Offender_address_number;
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }
    }
}
