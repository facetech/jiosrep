﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class UpdateOffenderAddressCommandValidator : AbstractValidator<UpdateOffenderAddressCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateOffenderAddressCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.OffenderAddressVM.OffenderAddress.Address_line1)
                .NotEmpty().NotNull().WithMessage( "Address is required.")
                  //.MaximumLength(1).WithMessage("Title must not exceed 200 characters.")
                  //.MustAsync(BeUniqueTitle).WithMessage("The specified title already exists.")
                  ;
            RuleFor(v => v.OffenderAddressVM.OffenderAddress.City)
               .NotEmpty().NotNull().WithMessage("City is required.")
               .MustAsync(ChickCity)
               .WithMessage("City invalid.")

               ;
            RuleFor(v => v.OffenderAddressVM.OffenderAddress.State)
            .NotEmpty().NotNull().WithMessage("State is required.")
            .MustAsync(chickState)
                   .WithMessage("State invalid.")


                   ;
            RuleFor(v => v.OffenderAddressVM.OffenderAddress)
      .NotEmpty().NotNull().WithMessage("Zip is required.")
      
      .MustAsync(ChickZip)
             .WithMessage("Zip code invalid.")

 //.MustAsync(ChickMatched)
 //            .WithMessage("they are not matched")
      ;


        }

        private async Task<bool> ChickMatched(OffenderAddressDto offenderAddress, CancellationToken arg2)
        {
            var chk = false;
            if (long.TryParse(offenderAddress.Zip, out long _zip))
            {
                chk = await _context.Zip_City_State_County
        .AnyAsync(l => l.Zipcode == _zip
        && l.City == offenderAddress.City
        && l.County == offenderAddress.County_name
        && l.State == offenderAddress.State
        );
            }
            return chk;
        }

        private async Task<bool> ChickZip(OffenderAddressDto offenderAddress, CancellationToken arg2)
        {
            var chk=false;
            if (long.TryParse(offenderAddress.Zip, out long _zip))
            {
                chk = await _context.Zip_City_State_County
      .AnyAsync(l => l.Zipcode == _zip);
            }       
            return chk;
        }

        private async Task<bool> chickState(string state, CancellationToken arg2)
        {
            return await _context.Zip_City_State_County
          .AnyAsync(l => l.State == state);
        }

        private async Task<bool> ChickCity(string city, CancellationToken arg2)
        {
            bool chk = await _context.Zip_City_State_County
           .AnyAsync(l => l.City == city);
            return  chk;
        }

        public async Task<bool> BeUniqueTitle(string title, CancellationToken cancellationToken)
        {
            return await _context.Offender_Address
                .AllAsync(l => l.Address_line1 != title);
        }
    }
}
