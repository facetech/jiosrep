﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using  e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class LivingArrangementsDto : IMapFrom<e.LivingArrangements>
    {
        public string living_arrangement { get; set; }
        public long? order_by { get; set; }
        public DateTime created_date { get; set; }
        public DateTime? modified_date { get; set; }
        public string modified_by { get; set; }
        public string created_by { get; set; }
    } // class OFFENDER_ADDRESS
}
