﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using  e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class OffenderAddressAuditDto : IMapFrom<e.OffenderAddressAudit>
    {
        public long Offender_address_audit_id { get; set; }
        public long Offender_address_number { get; set; }
        public long? Sin { get; set; }
        public string County_name { get; set; }
        public string Address_line1 { get; set; }
        public string Address_line2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public DateTime? Date_of_entry { get; set; }
        public bool? Judicial_district { get; set; }
        public bool? Current_address_flag { get; set; }
        public string Work_phone { get; set; }
        public string Work_phone_extention { get; set; }
        public bool? Message_phone_flag { get; set; }
        public string Employer { get; set; }
        public string Living_arrangement { get; set; }
        public string Cell_number { get; set; }
        public string Mailing_addr_line1 { get; set; }
        public string Mailing_addr_line2 { get; set; }
        public string Mailing_city { get; set; }
        public string Mailing_st { get; set; }
        public string Mailing_zip { get; set; }
        public string Comments { get; set; }
        public long? Audit_key { get; set; }
        public string Created_by { get; set; }
        public DateTime? Created_date { get; set; }
        public string Modified_by { get; set; }
        public DateTime? Modified_date { get; set; }
    } // class OFFENDER_ADDRESS_AUDIT

}
