﻿using IJOS.Application.TodoLists.Queries.GetTodos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application
{
    public class OffenderAddressVM
    {
        public string fullname { get; set; }
        public OffenderAddressDto OffenderAddress { get; set; }
        public List<OffenderAddressAuditDto> OffenderAddressAudits { get; set; }
        public List<LivingArrangementsDto> LivingArrangements { get; set; }
        public List<ZipCityStateCountyDto> ZipCityStateCounties { get; set; }
        public List<string> Cities { get; set; }
        public List<string> States { get; set; }
        public List<string> Counties { get; set; }


    }
}
