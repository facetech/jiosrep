﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.TodoLists.Queries.GetTodos;
using IJOS.Domain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Offender.Queries.GetOffender
{
    public class GetOffendersSearch : IRequest<OffenderSearchVM>
    {
        public string firstName { get; set; }
        public string lastName { get; set; }

        public string searchType { get; set; }
    }

    public class GetOffendersSearchHandler : IRequestHandler<GetOffendersSearch, OffenderSearchVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetOffendersSearchHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderSearchVM> Handle(GetOffendersSearch request, CancellationToken cancellationToken)
        {
            if (request.firstName == null)
                request.firstName = "";
            if (request.lastName == null)
                request.lastName = "";
            if (request.searchType == null)
                request.searchType = "1";

            string fn = request.firstName;
            string ln = request.lastName;

            if (fn == "" && ln != "")
                fn = "--------";

            if (fn != "" && ln == "")
                ln = "--------";

            var dbResults = await _unitOfWork.OffenderOverviewRepository.QueryAsync(
                @"SELECT * FROM IJOS.OFFENDER where FIRST_NAME like '%' + @firstName + '%' or last_NAME like '%' + @lastName+ '%'  "
                , new { firstName = fn.Trim(), lastName = ln.Trim() });

            var offenderDtos = _mapper.Map<List<OffenderDto>>(dbResults);

            return new OffenderSearchVM
            {
                First_name = request.firstName.Trim(),
                Last_name = request.lastName.Trim(),
                SearchType = request.searchType.Trim(),
                Offenders = offenderDtos
            };
        }
    }
}
