﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using  e = IJOS.Domain.Entities;

namespace IJOS.Application.Offender.Queries.GetOffender
{
    public class RelationshipsDto : IMapFrom<e.Relationships>
    {
        public long Relationship_id { get; set; }
        public string Status { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Middle_name { get; set; }
        public DateTime? Date_of_birth { get; set; }
        public string Sex { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
        public string Address_line1 { get; set; }
        public string Address_line2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone_number { get; set; }
        public string Work_phone { get; set; }
        public string Contact_name { get; set; }
        public string Place_of_business { get; set; }
        public string Occupation { get; set; }
        public string Marital_status { get; set; }
        public string Parent_support_ordered { get; set; }
        public long? Parent_support_amount { get; set; }
        public string Guardian_flag { get; set; }
        public string E_mail_address { get; set; }
        public string Social_security_number { get; set; }
        public decimal? Monthly_income { get; set; }
        public string Other_children_in_home { get; set; }
        public string Work_phone_extension { get; set; }
        public string Message_phone_flag { get; set; }
        public string Suffix { get; set; }
        public string Cell_phone { get; set; }
        public string Mailing_address_line1 { get; set; }
        public string Mailing_address_line2 { get; set; }
        public string Mailing_city { get; set; }
        public string Mailing_state { get; set; }
        public string Mailing_zip { get; set; }
        public string Primary_language { get; set; }
        public string Interpreter { get; set; }
        public string Created_by { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Full_name
        {
            get
            {
                return !string.IsNullOrEmpty(Last_name) && !string.IsNullOrEmpty(First_name) ?
                     $"{ Last_name}, {First_name} {Middle_name}" : string.Empty;
            }
        }

    }
}
