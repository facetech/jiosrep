﻿using IJOS.Application.TodoLists.Queries.GetTodos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application.Offender.Queries.GetOffender
{
    public class OffenderVM
    {
        public OffenderOverviewDto Offender { get; set; }
        public List<RelationshipsDto> Relationships { get; set; }
    }
}
