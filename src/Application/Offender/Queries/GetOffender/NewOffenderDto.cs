﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using System.ComponentModel.DataAnnotations;
using  e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class NewOffenderDto : IMapFrom<e.Offender>
    {
        public long Sin { get; set; }

        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Middle_name { get; set; }
        public DateTime Date_of_birth { get; set; }
        public string Sex { get; set; }
        public string Ethnicity { get; set; }
        public string Suffix { get; set; }

    }
}
