﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application.Offender.Queries.GetOffender
{
    public class OffenderSearchVM
    {
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string SearchType { get; set; }
        public List<OffenderDto> Offenders { get; set; }
    }
}
