﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.TodoLists.Queries.GetTodos;
using IJOS.Domain.Entities;
using IJOS.Domain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Offender.Queries.GetOffender
{
    public class GetOffenderByIdQuery : IRequest<OffenderVM>
    {
        public int Sin { get; set; }
    }


    public class GetOffenderByIdHandler : IRequestHandler<GetOffenderByIdQuery, OffenderVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetOffenderByIdHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderVM> Handle(GetOffenderByIdQuery request, CancellationToken cancellationToken)
        {

            try
            {

                var dbResults = await _unitOfWork.OffenderOverviewRepository.QuerySingleAsync
                        (@"     
                
           SELECT O.*, S.COUNTY_NAME as  STAFF_COUNTY_NAME,
                S.LAST_NAME as  STAFF_LAST_NAME,
                S.AGENCY_NAME as  STAFF_AGENCY_NAME,
                S.FIRST_NAME as  STAFF_FIRST_NAME,
                S.JOB_TITLE as  STAFF_JOB_TITLE,
                OA.address_line1 as STREET_ADDRESS, OA.CITY,
                OA.STATE, OA.ZIP, OA.WORK_PHONE,
                EmailContact.EMAIL_ADDRESS, 
                PhoneContact.PHONE, 
                DL.DRIVERS_LICENSE_NUMBER,
                DL.DRIVERS_LICENSE_STATE,
                JPO.LAST_NAME as JPO_LAST_NAME, JPO.FIRST_NAME as JPO_FIRST_NAME,
                JPO.AGENCY_NAME as JPO_AGENCY_NAME, JPO.PHONE_NUMBER as JPO_PHONE_NUMBER,JPO.SUPERVISION_LEVEL as SUPERVISION_LEVEL,

                JSC.LAST_NAME as JSC_LAST_NAME,JSC.FIRST_NAME as JSC_FIRST_NAME,
                JSC.AGENCY_NAME as JSC_AGENCY_NAME, JSC.PHONE_NUMBER as JSC_PHONE_NUMBER,

                GL.LAST_NAME as GL_LAST_NAME, GL.FIRST_NAME as GL_FIRST_NAME, GL.FACILITY_NAME as GL_AGENCY_NAME,
                GL.GROUP_NAME as GL_GROUP_NAME, u.UserName

                from IJOS.OFFENDER O
                    LEFT JOIN (
                        SELECT TOP 1 l.SIN,s.STAFF_KEY, s.LAST_NAME, s.FIRST_NAME, s.JOB_TITLE, s.PHONE_NUMBER,
                        s.E_MAIL_ADDRESS, s.AGENCY_NAME, l.SUPERVISION_LEVEL
                        FROM IJOS.STAFF s 
                        INNER JOIN IJOS.JJ_LINKS l on l.STAFF_KEY = s.STAFF_KEY
                        WHERE s.JOB_TITLE = 'JPO'
                    ) JPO on JPO.SIN = O.SIN
                    LEFT JOIN (

                    SELECT TOP 1 l.SIN,s.STAFF_KEY, s.LAST_NAME, s.FIRST_NAME, s.JOB_TITLE, s.PHONE_NUMBER,
                        s.E_MAIL_address, s.AGENCY_NAME
                        FROM IJOS.STAFF s 
                        INNER JOIN IJOS.JJ_LINKS l on l.STAFF_KEY = s.STAFF_KEY
                        WHERE s.JOB_TITLE = 'JSC'
                    ) JSC on JSC.SIN = O.SIN
                    LEFT JOIN (

                    SELECT TOP 1 pg.PLACEMENT_NUMBER, st.FIRST_NAME, st.LAST_NAME, pg.GROUP_NAME, p.SIN, p.FACILITY_NAME FROM IJOS.PLACEMENT_PLAN p
						LEFT JOIN IJOS.PLACEMENT_GROUP_DETAILS pg on pg.PLACEMENT_NUMBER = p.PLACEMENT_NUMBER
						LEFT JOIN IJOS.STAFF st on st.STAFF_KEY = pg.GROUP_LEADER
						ORDER BY PLANNED_END_DATE

                    ) GL on GL.SIN = O.SIN
					LEFT JOIN (
					
						SELECT  TOP 1 CM.METHOD_VALUE as EMAIL_ADDRESS, CL.SIN FROM IJOS.contact_method_linkage CL
						INNER JOIN IJOS.contact_method CM ON CM.CONTACT_METHOD_KEY = CL.CONTACT_METHOD_KEY
						WHERE CM.METHOD_TYPE like 'Email'
						ORDER BY CL.MODIFIED_BY, CL.CREATED_BY
					) EmailContact on  EmailContact.SIN = O.SIN
										LEFT JOIN (
					
						SELECT  TOP 1 CM.METHOD_VALUE as PHONE, CL.SIN FROM IJOS.contact_method_linkage CL
						INNER JOIN IJOS.contact_method CM ON CM.CONTACT_METHOD_KEY = CL.CONTACT_METHOD_KEY
						WHERE CM.METHOD_TYPE like 'Phone'
						ORDER BY CL.MODIFIED_BY, CL.CREATED_BY
					) PhoneContact on  PhoneContact.SIN = O.SIN
					LEFT JOIN ijos.DRIVERS_LICENSE_NUMBERS DL on DL.SIN = O.SIN
                    LEFT JOIN IJOS.CASE_LOAD C on C.SIN = O.SIN
                    LEFT JOIN IJOS.STAFF S on C.STAFF_KEY = S.STAFF_KEY
                    LEFT JOIN[IJOS].AspNetUsers u on u.Id = O.MODIFIED_By
                    LEFT JOIN IJOS.OFFENDER_ADDRESS OA ON OA.SIN = O.SIN
                    LEFT JOIN IJOS.OFFENDER_ADDRESS OA2 ON(OA2.SIN = O.SIN

                        AND OA.CREATED_DATE < OA2.CREATED_DATE OR(OA.CREATED_DATE = OA2.CREATED_DATE AND OA2.OFFENDER_ADDRESS_NUMBER < OA.OFFENDER_ADDRESS_NUMBER)
                    ) WHERE OA2.OFFENDER_ADDRESS_NUMBER is null AND O.SIN = @sin"
                    , new { sin = (int?)request.Sin });

                var offenderDto = _mapper.Map<OffenderOverviewDto>(dbResults);


                Photo photo = await  _unitOfWork.PhotoRepository.QuerySingleAsync
            (@"  select top 1  * from ijos.Photo where Photo.SIN =@sin order by Date_Taken desc"
                    , new { sin = (int?)request.Sin });
                PhotoDto mainphoto = new PhotoDto();
                try
                {
                    mainphoto = _mapper.Map<PhotoDto>(photo);
                    offenderDto.MainPhoto = mainphoto.Photo_data;
                }
                catch(Exception ex) { }


                var dbRelationships = await _unitOfWork.RelationshipsRepository.QueryAsync
                (@"     
                SELECT r.RELATIONSHIP_ID, r.LAST_NAME, r.FIRST_NAME, r.MIDDLE_NAME, rl.SIN, rl.STATUS, rl.RELATIONSHIP_TO_OFFENDER, r.SEX, r.DATE_OF_BIRTH FROM IJOS.RELATIONSHIPS r
                    INNER JOIN IJOS.RELATIONSHIP_LINKAGE rl on rl.RELATIONSHIP_ID = r.RELATIONSHIP_ID
                    WHERE rl.RELATIONSHIP_TO_OFFENDER <> 'SELF' AND rl.SIN = @sin and isnull(r.ISDeleted,0) =0"
                        , new { sin = (int?)request.Sin });


                var relationshipListDto = _mapper.Map<List<RelationshipsDto>>(dbRelationships);

                return new OffenderVM
                {
                    Offender = offenderDto,
                    Relationships = relationshipListDto
                };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
