﻿using AutoMapper;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Application.TodoLists.Queries.GetTodos;
using IJOS.Domain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Application.Offender.Queries.GetOffender
{
    public class GetOffendersQuery : IRequest<OffenderListVM>
    {
    }

    public class GetOffendersHandler : IRequestHandler<GetOffendersQuery, OffenderListVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetOffendersHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<OffenderListVM> Handle(GetOffendersQuery request, CancellationToken cancellationToken)
        {
            var dbResults = await _unitOfWork.OffenderOverviewRepository.QueryAsync("SELECT * FROM IJOS.OFFENDER");

            var offenderDtos = _mapper.Map<List<OffenderDto>>(dbResults);

            return new OffenderListVM
            {
                Offenders = offenderDtos
            };
        }
    }
}
