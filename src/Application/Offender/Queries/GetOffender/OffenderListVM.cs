﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Application.Offender.Queries.GetOffender
{
    public class OffenderListVM
    {
        public List<OffenderDto> Offenders { get; set; }
    }
}
