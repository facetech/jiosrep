﻿using AutoMapper;
using IJOS.Application.Common.Mappings;
using System;
using  e = IJOS.Domain.Entities;

namespace IJOS.Application.Offender.Queries.GetOffender
{
    public class OffenderOverviewDto : IMapFrom<e.OffenderOverview>
    {
        public long Sin { get; set; }
        public DateTime Date_of_birth { get; set; }

        public byte[] MainPhoto { get; set; }
        public string Eye_color { get; set; }
        public string County_name { get; set; }
        public string Probation_status { get; set; }
        public string Staff_county_name { get; set; }
        public string Staff_first_name { get; set; }
        public string Staff_last_name { get; set; }
        public string Staff_job_title { get; set; }
        public string Staff_agency_name { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
        public string Sex { get; set; }      
        public string Place_of_birth { get; set; }
        public string Mothers_maiden_name { get; set; }
        public string School_access_flag { get; set; }
        public string Sealed_flag { get; set; }
        public string Current_location { get; set; }
        public string Suffix { get; set; }
        public string Countyoffendernumber { get; set; }
        public string LAST_USER { get; set; }

       
        public string Supervision { get; set; }
        public string Supervision_level { get; set; }
        public string Street_address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string SSN { get; set; }
        public string Paper_file_location { get; set; }
        public decimal Height { get; set; }
        public string Hair_color { get; set; }
        public string Work_phone{ get; set; }

        public string email_address { get; set; }
        public string Weight { get; set; }
        public bool? Citizen_flag { get; set; }
        public string Primary_language { get; set; }
        public string Marital_status { get; set; }
        public bool? Isemployed { get; set; }
        public string Medicaid_number { get; set; }
        public string Drivers_license_number { get; set; }
        public string Drivers_license_state { get; set; }
        public string UserName { get; set; }

        public string Jpo_last_name { get; set; }
        public string Jpo_first_name { get; set; }
        public string Jpo_agency_name { get; set; }
        public string Jpo_phone_number { get; set; }

        public string Jsc_last_name { get; set; }
        public string Jsc_first_name { get; set; }
        public string Jsc_agency_name { get; set; }
        public string Jsc_phone_number{ get; set; }

        public string Gl_last_name { get; set; }
        public string Gl_first_name { get; set; }
        public string Gl_agency_name { get; set; }
        public string Gl_group_name { get; set; }

        public DateTime? Created_date { get; set; }

        public string Created_by { get; set; }

        public DateTime? Modified_date { get; set; }

        public string Modified_by { get; set; }

        public string City_state_zip
        {
            get
            {
                return !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(Zip) ?
                     $"{ City}, {State} {Zip}" : string.Empty;
            }
        }

        public string Full_name
        {
            get
            {
                return !string.IsNullOrEmpty(Last_name) && !string.IsNullOrEmpty(First_name) ?
                     $"{ Last_name}, {First_name}" : string.Empty;
            }
        }

        public int Age
        {
            get
            {
                DateTime now = DateTime.Today;

                int age = now.Year - Date_of_birth.Year;

                if (now.Month < Date_of_birth.Month || (now.Month == Date_of_birth.Month && now.Day < Date_of_birth.Day))
                    age--;

                return age;
            }
        }

        public string Height_string
        {
            get
            {
                if (Height == 0) return string.Empty;
                var ft = (int)Height;
                var inc = (int)((Height - (int)Height) * 100);
                if (inc == 0) return $"{ft} ft";
                return $"{ft} ft {inc} in";
            }
        }

    }
}
