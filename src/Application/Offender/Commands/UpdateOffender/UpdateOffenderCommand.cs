﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application.Offender.Commands.UpdateOffender
{
    public class UpdateOffenderCommand : IRequest
    {
        public OffenderVM OffenderVM { get; set; }
    }

    public class UpdateTodoListCommandHandler : IRequestHandler<UpdateOffenderCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateTodoListCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateOffenderCommand request, CancellationToken cancellationToken)
        {
            var inputEntity = request.OffenderVM.Offender;            
            var entity = await _unitOfWork.OffenderOverviewRepository.GetById(inputEntity.Sin);

            if (entity == null)
            {
                throw new NotFoundException(nameof(e.OffenderOverview), inputEntity.Sin);
            }

            var offender = _mapper.Map<OffenderOverviewDto, e.OffenderOverview>(inputEntity, entity);

            await _unitOfWork.OffenderOverviewRepository.Update(offender, cancellationToken);

            return Unit.Value;
        }
    }
}
