﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using e = IJOS.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;
using IJOS.Application.Common.Interfaces;
using System;

namespace IJOS.Application.Offender.Commands.UpdateOffender
{
    public class CreateOffenderCommand : IRequest<long>
    {
        public OffenderVM OffenderVM { get; set; }
    }
    public class CreateOffenderCommandHandler : IRequestHandler<CreateOffenderCommand, long>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;

        public CreateOffenderCommandHandler(IMapper mapper, ICurrentUserService currentUserService, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }

        public async Task<long> Handle(CreateOffenderCommand request, CancellationToken cancellationToken)
        {
            var inputEntity = request.OffenderVM.Offender;
            var entity = new e.OffenderOverview();

            var offender = _mapper.Map(inputEntity, entity);
            var usr = _currentUserService.UserId.PadLeft(8);


            await _unitOfWork.OffenderOverviewRepository.Insert(offender, cancellationToken);

            return offender.Sin;
        }
    }
}
