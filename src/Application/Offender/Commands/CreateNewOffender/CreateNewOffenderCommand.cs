﻿using AutoMapper;
using IJOS.Application.Common.Exceptions;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.Persistence_Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using e = IJOS.Domain.Entities;

namespace IJOS.Application
{
    public class CreateNewOffenderCommand : IRequest
    {
        public NewOffenderDto OffenderDto { get; set; }

    }

    public class CreateNewOffenderCommandHandler : IRequestHandler<CreateNewOffenderCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        //private readonly IMapper _RelationshipMap;

        public CreateNewOffenderCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreateNewOffenderCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entityDbo = request.OffenderDto;
                entityDbo.Ethnicity = "";
                e.Offender entity = _mapper.Map<e.Offender>(entityDbo);
                await _unitOfWork.OffenderRepository.Insert(entity, cancellationToken);
                
                return Unit.Value;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }


    }
}
