﻿using IJOS.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace IJOS.Application.TodoLists.Commands.CreateTodoList
{
    public class CreateNewOffenderCommandValidator : AbstractValidator<OffenderDto>
    {
        private readonly IApplicationDbContext _context;

        public CreateNewOffenderCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.First_name)
                .NotEmpty().NotNull().WithMessage(v => v.First_name + "First name is required.");

            RuleFor(v => v.Last_name)
                .NotEmpty().NotNull().WithMessage(v => v.Last_name + "Last name is required.");

            RuleFor(v => v.Date_of_birth)
                .NotEmpty().NotNull().WithMessage(v => v.Date_of_birth + "Date of birth is required.");


        }




    }
}
