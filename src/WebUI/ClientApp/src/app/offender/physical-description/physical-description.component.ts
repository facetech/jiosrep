import { Component, Inject } from '@angular/core';
import { PhysicalDescriptionClient, OffenderPhysicalDescriptionVM, UpdateOffenderPhysicalDescriptionCommand } from '../../web-api-client';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  id: number;
}

@Component({
  selector: 'app-physical-description',
  templateUrl: './physical-description.component.html',
  styleUrls: ['./physical-description.component.scss']
})
export class PhysicalDescriptionComponent{

  ShowPhysicalDescription: boolean = false;
  newListEditor: any = {};
  vm: OffenderPhysicalDescriptionVM;
  private uiCommonService: UiCommonService;
  //private router: Router;

  constructor(private physicalDescriptionClient: PhysicalDescriptionClient, uiCommonService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<PhysicalDescriptionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    this.uiCommonService = uiCommonService;
    this.LoadGrid(data.id);
  }

  ngOnInit(): void {
  }

  onSubmit(item: OffenderPhysicalDescriptionVM) {
    const updateOffenderPhysicalDescriptionCommand = UpdateOffenderPhysicalDescriptionCommand.fromJS(item);
    updateOffenderPhysicalDescriptionCommand.offenderPhysicalDescriptionVM = item;
    
    this.physicalDescriptionClient.update(item.offenderPhysicalDescription.sin, updateOffenderPhysicalDescriptionCommand)
      .subscribe(
        () => this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS),
        
        error => console.error(error)
    );
    this.ShowPhysicalDescription = false;
  }

  LoadGrid(id) {
    this.physicalDescriptionClient.get(id).subscribe(
      result => {      
        this.vm = result;        
      },
      error => console.error(error)
    );
  }

  PhysicalDescriptionPopup(): void {
    this.ShowPhysicalDescription = true;
  }

  ClosePopup(): void {
    this.ShowPhysicalDescription = false;
  }
  
}


