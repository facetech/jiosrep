import { Component, Inject, OnInit } from '@angular/core';
import { UiCommonService } from "../../ui-common/ui-common-service";
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { AdditionalDetailsClient, OffenderADVM, PhotoVM, UpdateAdditionalDetailsCommand } from '../../web-api-client';


export interface DialogData {
  fullname: string;
  id: number;
  Mythis: any;
}
@Component({
  selector: 'app-additional-details',
  templateUrl: './additional-details.component.html',
  styleUrls: ['./additional-details.component.scss']
})
export class AdditionalDetailsComponent implements OnInit {

  vm: PhotoVM;
  newListEditor: any = {};
  sin: number;
  fullname: string;
  Parent: any;
  private uiCommonService: UiCommonService;

  constructor(private additionalDetailsClient: AdditionalDetailsClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<AdditionalDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.uiCommonService = uiService;
    console.log('----------------- id is: ' + data.id);
    this.sin = data.id;
    this.fullname = data.fullname;
    this.LoadGrid(data.id);
  }
  LoadGrid(id: number) {
    this.additionalDetailsClient.get(id).subscribe(
      result => {
        debugger;

        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }

  ngOnInit(): void {
  }
  onSubmit(contactForm) {
    console.log(contactForm.value);
  }

  updateItem(item: OffenderADVM): void {

    debugger;
    this.newListEditor = [];
    const updateAdditionalDetailsCommand = UpdateAdditionalDetailsCommand.fromJS(item);
    updateAdditionalDetailsCommand.offenderADVM = item;
    this.additionalDetailsClient.update(updateAdditionalDetailsCommand)
      .subscribe(
     
        result => {
          debugger;
         

          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          this.ParentOnInit();
         // window.location.reload();
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);

          if (errors) {
            if (errors.errors["offenderADVM.Offender.Ssn"]) {
              this.newListEditor.ssn = errors.errors["offenderADVM.Offender.Ssn"][0];
            }
            if (errors.errors["offenderADVM.DriversLicenseNumbers.Drivers_license_number"]) {
              this.newListEditor.drivers_license_number = errors.errors["offenderADVM.DriversLicenseNumbers.Drivers_license_number"][0];
            }
            if (errors.errors["offenderADVM.Offender.Medicaid_number"]) {
              this.newListEditor.medicaid_number = errors.errors["offenderADVM.Offender.Medicaid_number"][0];
            }
            if (errors.errors["offenderADVM.Offender.Paper_file_location"]) {
              this.newListEditor.medicaid_number = errors.errors["offenderADVM.Offender.Paper_file_location"][0];
            }
          }

         // setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(item.offender.sin);
        }
          
      );
  }
  ParentOnInit(): void {
    debugger

    this.data.Mythis.offenderClient.get2(this.sin).subscribe(
      result => {
        debugger;
        this.data.Mythis.vm = result;
        this.data.Mythis.fullName = result.offender.first_name + " " + result.offender.last_name;
      },
      error => console.error(error)
    );

  }
}
