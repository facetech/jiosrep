import { AfterViewInit, Component, Inject, OnInit, ViewChild, ViewEncapsulation, ElementRef } from '@angular/core';
import { UiCommonService } from "../../ui-common/ui-common-service";
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FileParameter, PhotoDto, PhotosClient, PhotosTypeVM, PhotoVM } from '../../web-api-client';
import { DomSanitizer } from '@angular/platform-browser'
import { Validators } from '@angular/forms';

import { NgxPrintModule } from 'ngx-print';
import { DatePipe, getLocaleDateTimeFormat } from '@angular/common';
import { AuthorizeService } from '../../../api-authorization/authorize.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { waitForAsync } from '@angular/core/testing';
/*import { async } from '@angular/core/testing';*/
/*import { getCookie, setCookie } from 'typescript-cookie'*/
/*import { async } from '@angular/core/testing';*/

export interface DialogData {
  fullname: string;
  id: number;
  Mythis: any;

}
@Component({
  selector: 'app-photos-details',
  templateUrl: './photos-details.component.html',
  styleUrls: ['./photos-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PhotosDetailsComponent implements AfterViewInit, OnInit {

  public CuserName: any;
  datepipe: DatePipe = new DatePipe('en-US')
  htmlToAdd: any;
  datenow: any = "";
  htmlToAddHead: any;
  ShowModle: boolean = false;
  ShowPrintPlate: boolean = false;
  ShowPhotoType: boolean = false;
  vm: PhotoVM;
  currentType: any;

  typevm: PhotosTypeVM;
  fullName: string;
  sin: number;
  public files: File[];
  public _fileParameters: FileParameter[];
  newListEditor: any = {};
  private uiCommonService: UiCommonService;
  @ViewChild('myname') public input: ElementRef;
  constructor(private _Client: PhotosClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<PhotosDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private sanitizer: DomSanitizer, private authorizeService: AuthorizeService) {
    this.uiCommonService = uiService;
    console.log('----------------- id is: ' + data.id);
    this.fullName = data.fullname;
    this.sin = data.id;
    debugger;

  }
  ngOnInit(): void {
    this.LoadGrid(this.sin);

    this.authorizeService.getUser()
      .subscribe(data => {
        debugger;
        this.CuserName = data.name;
        // console.log(data); //You will get all your user related information in this field
      });


    debugger;
    this.datenow = this.datepipe.transform(new Date().toLocaleDateString(), 'MM/dd/YYYY')
  }
  ngAfterViewInit(): void {



  }

  fireImage(photoId: number): void {
    debugger;
    this.newListEditor = [];
    this.vm.photo = this.vm.photos.filter(o => o.photo_id == photoId)[0];
    this.imgURL = this.sanitizer
      .bypassSecurityTrustResourceUrl(`data:image/png;base64, ${this.vm.photo.photo_data}`);

    this.files = [];
    this.ShowModle = true;
  }

  fireImageDet(photoId: number): void {
    debugger;
    this.newListEditor = [];
    this.vm.photo = this.typevm.photos.filter(o => o.photo_id == photoId)[0];
    this.imgURL = this.sanitizer
      .bypassSecurityTrustResourceUrl(`data:image/png;base64, ${this.vm.photo.photo_data}`);

    this.files = [];
   
    this.ShowModle = true;
  }


  LoadGrid(id: number) {
    
    this._Client.get(id).subscribe(
      result => {
        debugger;

        this.vm = result;
      

        if (this.ShowPhotoType == true) {
          this.showimages(this.currentType);
        }
        else
          this.buildPhotos();

       

      },
      error => console.error(error)
    );
  }

  showimages(typeid): void {
   
    this._Client.getByType(this.data.id, typeid).subscribe(
        result => {
          this.typevm = result;
        //this.buildPhotos();

        if (this.typevm != undefined && this.typevm != null)
          if (this.typevm.photos.length == 0)
            this.buildPhotos();

        if (result.photos.length > 0) {

          this.ShowPhotoType = true;

          this.currentType = typeid;
        } else
          this.ShowPhotoType = false;



   
        debugger;
        },
        error => console.error(error)
    );

  }

  buildPhotos(): void {
    let result = this.vm;
    let Headimage = "";
    debugger;
    let photoHead = result.photos.filter(o => o.photo_type_id == 7 || o.photo_type_id == 19 );
    if (photoHead.length > 0) {
      Headimage = ' <img class="innerimgHeader _myimg" value=' + photoHead[0].photo_id + '  src="data:image/png;base64,' + photoHead[0].photo_data + '"><script>alert(1)</script> <br>';
    }


    let GrbImg = "";
    var Start_Flg = 1;
    for (var i = 0; i < result.phototypes.length; i++) {
      let _photo = result.photos.filter(o => o.photo_type_id == result.phototypes[i].photo_type_id);//&& o.setasschoolid != true
      if (_photo.length > 0) {
        //row start
        if (Start_Flg == 1)
          GrbImg += " <div class='row'>";

    
        
        debugger;
        GrbImg += `<div class="col-md-6 border GrbImg "  >   <button class="_BTN" value= "` + result.phototypes[i].photo_type_id + `" style ="ackground-color: Transparent;    border-block-style: none;    font-family: 'Arial-BoldMT', 'Arial Bold', 'Arial';    font-weight: 600;    font-style: oblique;    color: #169BD5;">` + result.phototypes[i].photo_type + `</button>   <br>`;
       
        for (var j = 0; j < _photo.length; j++) {
          GrbImg += `<div class="defimg"  >
                           <img class="innerimg _myimg" value=` + _photo[j].photo_id + `  src="data:image/png;base64,` + _photo[j].photo_data + `">
                         </div>`;
        }
        GrbImg += "</div>";

        //row end
        if (Start_Flg == 0) {
          GrbImg += " </div>";
          Start_Flg = 1;
        }
        else
          Start_Flg = 0;


      }
    }

    if (Start_Flg == 0)
      GrbImg += " </div>";
    this.htmlToAddHead = Headimage;
    this.htmlToAdd = this.sanitizer.bypassSecurityTrustHtml(GrbImg);
    setTimeout(() => {


      var _innerimg = document.getElementsByClassName("_myimg");
      for (var i = 0; i < _innerimg.length; i++) {
        let photoId = Number(_innerimg[i].attributes[1].nodeValue);
        _innerimg[i].addEventListener("click", (e: Event) => this.fireImage(photoId));

      }
    }, 1000);
    
    setTimeout(() => {
      debugger;

      var _innerBTN = document.getElementsByClassName("_BTN");
      for (var i = 0; i < _innerBTN.length; i++) {
        let phototypeId = Number(_innerBTN[i].attributes[1].nodeValue);
        _innerBTN[i].addEventListener("click", (e: Event) => this.showimages(phototypeId));

      }
    }, 1000);

  }
  public imagePath;
  imgURL: any;
  public message: string;

  preview(files, inx) {
    debugger;
    if (files.length === 0)
      return;

    this.files = files;
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;

    }
  }

  onSubmit(contactForm) {
    console.log(contactForm.value);
  }


  NewPhotoPopup(): void {
    this.ShowModle = true;
    let _photo = new PhotoDto();
    _photo.sin = this.sin;
    _photo.photo_id = 0;
    _photo.date_taken = new Date();
    this.vm.photo = _photo;
    this.imgURL = "../../../assets/Images/Portrait_Placeholder.png"
    this.files = [];


    this.newListEditor = [];
  }

  PrintPlate(): void {
    this.ShowPrintPlate = true;
    //let _photo = new PhotoDto();
    //_photo.sin = this.sin;
    //_photo.photo_id = 0;
    //this.vm.photo = _photo;
    //this.imgURL = "../../../assets/Images/Portrait_Placeholder.png"
    //this.files = [];
    this.newListEditor = [];
  }
  ClosePrintPlate(): void {
    this.ShowPrintPlate = false;

  }
  CloseShowPhotoType(): void {
    this.ShowPhotoType = false;

  }

  ClosePopup(): void {
    this.ShowModle = false;


  }
  Validation(_photo: PhotoDto): Boolean {
    debugger;
    let flg = true;
    if (_photo == undefined || (_photo.photo_id == 0 && this.files.length == 0)) {
      alert("Photo Is Required!");
      this.newListEditor.file = "Photo Is Required!";
      return false;
    }
    if (_photo.taken_by == undefined || _photo.taken_by.trim() == "") {

      this.newListEditor.taken_by = "Taken by Is Required!"
      flg = false;
    }

    if (_photo.date_taken == undefined || _photo.date_taken.toString().trim() == "") {

      this.newListEditor.date_taken = "Date taken Is Required!"
      flg = false;
    }
    if (_photo.location_taken == undefined || _photo.location_taken.trim() == "") {

      this.newListEditor.location_taken = "Location taken Is Required!"
      flg = false;
    }
    if (_photo.photo_type_id == undefined || _photo.photo_type_id.toString().trim() == "") {

      this.newListEditor.photo_type_id = "Photo type Is Required!"
      flg = false;
    }
    if (_photo.location_taken == undefined || _photo.location_taken.trim() == "") {

      this.newListEditor.location_taken = "Location taken Is Required!"
      flg = false;
    }
    return flg;
  }
  CreateItem(_photo: PhotoDto): void {

    debugger;
    if (!this.Validation(_photo)) {
      return;
    }
    this.newListEditor = [];

    this._fileParameters = [];
    if (this.files != undefined) {
      for (var i = 0; i < this.files.length; i++) {
        let fileParameter: FileParameter = {
          data: this.files[i],
          fileName: this.files[i].name,

        };
        this._fileParameters.push(fileParameter);
      }
    }



    this._Client.create(
      _photo.photo_id,
      _photo.photo_type_id
      , _photo.date_taken
      , _photo.location_taken
      , _photo.sin
      , _photo.login_name
      , _photo.taken_by
      , _photo.comments
      , _photo.login_created
      , _photo.login_edited
      , _photo.setasschoolid
      , this._fileParameters)
      .subscribe(
        _result => {
          debugger;
          this.newListEditor = [];
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          this.data.Mythis.ngOnInit();
          this.ClosePopup();

        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);

          if (errors) {
            if (errors.errors["PhotoVM.Photo.Taken_by"]) {
              this.newListEditor.ssn = errors.errors["PhotoVM.Photo.Taken_by"][0];
            }
            if (errors.errors["PhotoVM.Photo.Photo_type_id"]) {
              this.newListEditor.drivers_license_number = errors.errors["PhotoVM.Photo.Photo_type_id"][0];
            }
            if (errors.errors["PhotoVM.Photo.Date_taken"]) {
              this.newListEditor.medicaid_number = errors.errors["PhotoVM.Photo.Date_taken"][0];
            }
            if (errors.errors["PhotoVM.Photo.Location_taken"]) {
              this.newListEditor.medicaid_number = errors.errors["PhotoVM.Photo.Location_taken"][0];
            }
          }

          // setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(this.sin);


        }

      );

    debugger;
    

  }


  deleteListConfirmed(): void {
    if (confirm("Are you sure to delete this photo")) {

      this._Client.delete(this.vm.photo.photo_id).subscribe(
        () => {
          debugger;
          this.newListEditor = [];
          this.uiCommonService.toastNotification('Delete succeeded', DialogLayoutDisplay.SUCCESS);
          this.ClosePopup();
          //this.deleteListModalRef.hide();
          //this.vm.lists = this.vm.lists.filter(t => t.id != this.selectedList.id)
          //this.selectedList = this.vm.lists.length ? this.vm.lists[0] : null;
        },
        error => console.error(error)
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(this.sin);
        }
      );

    }
  }

}


