import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OffenderLayoutComponent } from '../../offender-layout/offender-layout.component';
import { OffenderAddressComponent } from '../../offender-address/offender-address.component';
import { OffenderClient, OffenderVM, RelationshipsClient } from '../../web-api-client';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AdditionalDetailsComponent } from '../additional-details/additional-details.component';
import { PhysicalDescriptionComponent } from '../physical-description/physical-description.component';
import { PhotosDetailsComponent } from '../photos-details/photos-details.component';
import { OffenderstaffComponent } from '../../offenderstaff/offender-staff.component';
import { OffenderrelationshipComponent } from '../../offenderrelationship/offenderrelationship.component';
import { OffenderCommentsComponent } from '../../offender-comments/offender-comments.component';



@Component({
  selector: 'app-juvenile-overview',
  templateUrl: './juvenile-overview.component.html',
  styleUrls: ['./juvenile-overview.component.scss']
})
export class JuvenileOverviewComponent implements OnInit {

  @Input() currentSin: number;
  @Output() countChanged: EventEmitter<number> = new EventEmitter();

  private router: Router;
  MainPhotoDiv: any;
  public vm: OffenderVM;
  id: number;
  fullName: string;
  offenderLayoutComponent = new OffenderLayoutComponent(this.dialog); //currentSin

  openModal() {//Address
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '94%';
    //dialogConfig.width = "600px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { id: this.id, Mythis: this };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(OffenderAddressComponent, dialogConfig);
  }

  openRelationshipModal(number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '910px';
    dialogConfig.width = '850px';

    debugger;
    dialogConfig.data = { sin: this.id, individual_number: number, Mythis: this, fullname: this.fullName };
    this.dialog.open(OffenderrelationshipComponent, dialogConfig);

  }


  private deleteRelationship(individual_number_ToDelete): void {
    if (confirm("Are you sure to delete this Relationship")) {



      this.RelationshipsClient.delete(individual_number_ToDelete)
        .subscribe(

          result => {
            debugger;
            this.ngOnInit();
            //this.IsDeleteflag = true;
            //this.uiCommonService.toastNotification('Delete succeeded', DialogLayoutDisplay.SUCCESS);
            //this.ClosePopup();
            // window.location.reload();
          },
          error => {
            debugger;
            let errors = JSON.parse(error.response);

          }
          ,
          () => {

            // this.LoadGrid(item.relationshipsDto2.sin, item.relationshipsDto2.individuaL_NUMBER);
          }

        );

    }
  }


  openCommentModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '500px';
    dialogConfig.width = '700px';
    //dialogConfig.width = "600px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { id: this.id, fullname: this.fullName };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(OffenderCommentsComponent, dialogConfig);
  }

  openStaffModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    //dialogConfig.height = '700px';
    //dialogConfig.width = "950px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { id: this.id, fullname: this.fullName };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(OffenderstaffComponent, dialogConfig);
  }

  openPhysicalDescription() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '55%';
    //dialogConfig.width = "600px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { id: this.id };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(PhysicalDescriptionComponent, dialogConfig);
  }


  openAdditionalDetailsModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '55%';
    //dialogConfig.width = "600px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { id: this.id, fullname: this.fullName, Mythis: this };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(AdditionalDetailsComponent, dialogConfig);
  }
  openModalPhoto() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '600px';
    dialogConfig.width = "700px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { id: this.id, fullname: this.fullName, Mythis: this };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(PhotosDetailsComponent, dialogConfig);
  }

  constructor(private offenderClient: OffenderClient, private RelationshipsClient: RelationshipsClient, private route: ActivatedRoute, public dialog: MatDialog) {

  }

  public ngOnInit(): void {
    // debugger;

    const param = this.route.snapshot.paramMap.get('id');

    if (param) {
      this.id = parseInt(param, 10);
      //  debugger;
      this.countChanged.emit(this.id);
      this.offenderClient.get2(this.id).subscribe(
        result => {
          this.vm = result;
          OffenderLayoutComponent.offender = result;
          this.fullName = result.offender.first_name + " " + result.offender.last_name;
          debugger;
          if (this.vm.offender.mainPhoto != null && this.vm.offender.mainPhoto != '')
            this.MainPhotoDiv = 'data:image/png;base64,' + this.vm.offender.mainPhoto + '';
          else
            this.MainPhotoDiv = '../../../assets/Images/Portrait_Placeholder.png';
          // document.getElementById('MainPhoto'). = "data:image/png;base64,' + this.vm.offender.mainPhoto + '";
          // this.MainPhotoDiv = ' <img class="photo-placeholder"   src="data:image/png;base64,' + this.vm.offender.mainPhoto + '">';

        },
        error => console.error(error)
      );
    }
  }

}
