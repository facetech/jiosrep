import { Component, OnInit } from '@angular/core';
import {
  OffenderVM, OffenderDto, OffenderClient, UpdateOffenderCommand, OffenderOverviewDto
} from '../web-api-client';
import { faPlus, faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import {  BsModalRef } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute } from "@angular/router";

import { UiCommonService } from "../ui-common/ui-common-service";


import {
  DialogLayoutDisplay,
} from '@costlydeveloper/ngx-awesome-popup';


@Component({
  selector: 'app-offender-component',
  templateUrl: './offender.component.html',
  styleUrls: ['./offender.component.scss']
})
export class OffenderComponent implements OnInit {

  debug = false;
  private router: Router;
  vm: OffenderVM;

  selectedItem: OffenderDto;

  newListEditor: any = {};
  listOptionsEditor: any = {};
  itemDetailsEditor: any = {};

  newListModalRef: BsModalRef;
  listOptionsModalRef: BsModalRef;
  deleteListModalRef: BsModalRef;
  itemDetailsModalRef: BsModalRef;

  faPlus = faPlus;
  faEllipsisH = faEllipsisH;
  private uiCommonService: UiCommonService;
 
  constructor(private offenderClient: OffenderClient, uiService: UiCommonService, private route: ActivatedRoute) {
    this.uiCommonService = uiService;
    const param = this.route.snapshot.paramMap.get('id');
    if (param) {
      const id = parseInt(param, 10);

      offenderClient.get2(id).subscribe(
        result => {
          this.vm = result;
        },
        error => console.error(error)
      );
    } else {
      this.vm = new OffenderVM();
      this.vm.offender = new OffenderOverviewDto();
    }
  }

  onSubmit(contactForm) {
    console.log(contactForm.value);
  }

  updateItem(item: OffenderVM): void {
    const updateOffenderCommand = UpdateOffenderCommand.fromJS(item);
    updateOffenderCommand.offenderVM = item;
    this.offenderClient.update(item.offender.sin, updateOffenderCommand)
      .subscribe(
        () => this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS),
        error => console.error(error)
      );
  }

  ngOnInit() {
    
  }
}
