import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AddressClient, CreateOffenderAddressCommand, CreateOffenderCommand, OffenderAddressVM, UpdateOffenderAddressCommand
} from '../web-api-client';
import { UiCommonService } from "../ui-common/ui-common-service";
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';



export interface DialogData {
  id: number;
  Mythis: any;
}


@Component({
  selector: 'app-offender-address',
  templateUrl: './offender-address.component.html',
  styleUrls: ['./offender-address.component.scss']
})
export class OffenderAddressComponent{

  ShowAddress: boolean = false;
  ShowMail: boolean = false;
  sin: number;
  newListEditor: any = {};
  
  vm: OffenderAddressVM;
  private uiCommonService: UiCommonService;
  constructor(private addressClient: AddressClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderAddressComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
 
    this.uiCommonService = uiService;
    console.log('----------------- id is: ' + data.id);
    this.LoadGrid(data.id);
  }
  onSubmit(contactForm) {
    console.log(contactForm.value);
  }
  LoadGrid(id) {
    this.addressClient.get(id).subscribe(
      result => {
        debugger;

        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }
  AddressPopup(): void {

      this.ShowAddress = true;    
  }
  ClosePopup(): void {
    this.ShowAddress = false;
    this.ShowMail = false;
  }
  MailPopup(): void {
    this.loadCityState(this.vm.zipCityStateCounties);
      this.ShowMail = true;    
  }
  stateChanged(objch): void {
    this.newListEditor = [];
    const crrentState=  this.vm.offenderAddress.state ;
    debugger;
    const someStateCity = this.vm.zipCityStateCounties.filter(o => o.state == crrentState);

    const someCity = someStateCity.map(o => o.city);
    this.vm.cities = this.vm.zipCityStateCounties.filter(o => someCity.some(t => t == o.city))
      .map(o => o.city).filter((value, index, self) => self.indexOf(value) === index);
    this.vm.offenderAddress.city = this.vm.cities[0];

    const someCounty = someStateCity.map(o => o.county);
    this.vm.counties = this.vm.zipCityStateCounties.filter(o => someCounty.some(t => t == o.county))
      .map(o => o.county).filter((value, index, self) => self.indexOf(value) === index);
    this.vm.offenderAddress.county_name = this.vm.counties[0];

    const someZip = someStateCity.map(o => o.zipcode);
    this.vm.offenderAddress.zip = this.vm.zipCityStateCounties.filter(o => someZip.some(t => t == o.zipcode))
      .map(o => o.zipcode).filter((value, index, self) => self.indexOf(value) === index)[0].toString();

  }
  cityChanged(objch): void {
    this.newListEditor = [];
    const crrentcity = this.vm.offenderAddress.city;
    debugger;
    const someStateCity = this.vm.zipCityStateCounties.filter(o => o.city == crrentcity);

    const someState = someStateCity.map(o => o.state);
    this.vm.states = this.vm.zipCityStateCounties.filter(o => someState.some(t => t == o.state))
      .map(o => o.state).filter((value, index, self) => self.indexOf(value) === index);
    this.vm.offenderAddress.state = this.vm.states[0];

    const someCounty = someStateCity.map(o => o.county);
    this.vm.counties = this.vm.zipCityStateCounties.filter(o => someCounty.some(t => t == o.county))
      .map(o => o.county).filter((value, index, self) => self.indexOf(value) === index);
    this.vm.offenderAddress.county_name = this.vm.counties[0];


    const someZip = someStateCity.map(o => o.zipcode);
    this.vm.offenderAddress.zip = this.vm.zipCityStateCounties.filter(o => someZip.some(t => t == o.zipcode))
      .map(o => o.zipcode).filter((value, index, self) => self.indexOf(value) === index)[0].toString();

  }
  zipChanged(objch): void {
    debugger;
    this.newListEditor = [];
    let someStateCity;
    const crrentzip = this.vm.offenderAddress.zip;
    if (this.vm.offenderAddress.zip == "" || this.vm.offenderAddress.zip == null) {
      someStateCity = this.vm.zipCityStateCounties;
    } else {
      someStateCity= this.vm.zipCityStateCounties.filter(o => o.zipcode == Number(crrentzip))
    }
     
    this.loadCityState(someStateCity);

    //const someZip = someStateCity.map(o => o.zipcode);
    //this.vm.offenderAddress.zip_2 = this.vm.zipCityStateCounties.filter(o => someZip.some(t => t == o.zipcode))
    //  .map(o => o.zipcode).filter((value, index, self) => self.indexOf(value) === index)[0].toString();
  }
  loadCityState(someStateCity): void  {

    const someState = someStateCity.map(o => o.state);
    this.vm.states = this.vm.zipCityStateCounties.filter(o => someState.some(t => t == o.state))
      .map(o => o.state).filter((value, index, self) => self.indexOf(value) === index);
    this.vm.offenderAddress.state = this.vm.states[0];

    const someCity = someStateCity.map(o => o.city);
    this.vm.cities = this.vm.zipCityStateCounties.filter(o => someCity.some(t => t == o.city))
      .map(o => o.city).filter((value, index, self) => self.indexOf(value) === index);
    this.vm.offenderAddress.city = this.vm.cities[0];

    const someCounty = someStateCity.map(o => o.county);
    this.vm.counties = this.vm.zipCityStateCounties.filter(o => someCounty.some(t => t == o.county))
      .map(o => o.county).filter((value, index, self) => self.indexOf(value) === index);
    this.vm.offenderAddress.county_name = this.vm.counties[0];
  }
  updateItem(item: OffenderAddressVM): void {
    debugger;
    this.newListEditor = [];

    if (item.offenderAddress.offender_address_number == undefined || item.offenderAddress.offender_address_number == 0 || item.offenderAddress.offender_address_number == null) {
      const _Command = CreateOffenderAddressCommand.fromJS(item);
      debugger;
      _Command.offenderAddressVM= item;
      this.addressClient.create(_Command)
        .subscribe(

          result => {
            debugger;
            this.Success(result);
         
          },
          error => {
            debugger;
            this.Fail(error);

            // setTimeout(() => document.getElementById("title").focus(), 250);
          }
          ,
          () => {
            // 'onCompleted' callback.
            // No errors, route to new page here
            this.Complete();
        
          }

        );
    }
    else {
      const updateOffenderAddressCommand = UpdateOffenderAddressCommand.fromJS(item);
      updateOffenderAddressCommand.offenderAddressVM = item;
      this.addressClient.update(updateOffenderAddressCommand)
        .subscribe(

          result => {
            debugger;
            this.Success(result);

          },
          error => {
            debugger;
            this.Fail(error);
           
            // setTimeout(() => document.getElementById("title").focus(), 250);
          }
          ,
          () => {
            // 'onCompleted' callback.
            // No errors, route to new page here
            this.Complete();
          }

        );
    }
  }
  Complete() {
    this.LoadGrid(this.data.id);
  }
    Fail(error: any) {
      let errors = JSON.parse(error.response);

      if (errors) {
        if (errors.errors["OffenderAddressVM.OffenderAddress.address_2"]) {
          this.newListEditor.address = errors.errors["OffenderAddressVM.OffenderAddress.address_2"][0].replace("'Offender Address V M. Offender Address.address_2' ", "");
        }
        if (errors.errors["OffenderAddressVM.OffenderAddress.city_2"]) {
          this.newListEditor.city = errors.errors["OffenderAddressVM.OffenderAddress.city_2"][0];
        }
        if (errors.errors["OffenderAddressVM.OffenderAddress.state_2"]) {
          this.newListEditor.state = errors.errors["OffenderAddressVM.OffenderAddress.state_2"][0];
        }
        if (errors.errors["OffenderAddressVM.OffenderAddress"]) {
          this.newListEditor.zip = errors.errors["OffenderAddressVM.OffenderAddress"][0];
        }

      }

    }
    Success(result:any) {
      this.ShowAddress = this.ShowMail = false;
      this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
      this.ParentOnInit();
    }
  ParentOnInit(): void {
    debugger

    this.data.Mythis.offenderClient.get2(this.data.id).subscribe(
      result => {
        debugger;
        this.data.Mythis.vm = result;
        this.data.Mythis.fullName = result.offender.first_name + " " + result.offender.last_name;
      },
      error => console.error(error)
    );

  }
}
