import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../ui-common/ui-common-service';
import { OffendernamehistClient, Offender_name_histDto, Offender_name_histVm, UpdateOffenderNameCommand } from '../web-api-client';



export interface DialogData {
  id: number;
  fullname: string;
}


@Component({
  selector: 'app-offender-name-hist',
  templateUrl: './offender-name-hist.component.html',
  styleUrls: ['./offender-name-hist.component.scss']
})
export class OffenderNameHistComponent implements OnInit {

  newListEditor: any = {};
  ShowItem: boolean = false;
  vm: Offender_name_histVm;
  datepipe: DatePipe = new DatePipe('en-US')
  private uiCommonService: UiCommonService;

  constructor(private OffendernamehistClient: OffendernamehistClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderNameHistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData)
  {
    this.uiCommonService = uiService;
   
    this.LoadGrid(data.id);
  }

  ngOnInit(): void {
  }


  LoadGrid(id) {
    debugger;
    this.OffendernamehistClient.getAll(id).subscribe(
      result => {
        debugger;

        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }

  getitem() {
   
   
        this.vm.offender_name_histDto = new Offender_name_histDto();
        this.vm.offender_name_histDto.sin = this.data.id;
        this.ShowItem = true;
  
  }

  ClosePopup(): void {
    this.ShowItem = false;
    this.LoadGrid(this.data.id);

  }

  UpdateItem(item: Offender_name_histVm): void {
    debugger;
    this.newListEditor = [];
    const updateOffenderNameCommand = UpdateOffenderNameCommand.fromJS(item);
    updateOffenderNameCommand.offender_name_histVm = item;
    this.OffendernamehistClient.update(updateOffenderNameCommand)
      .subscribe(

        result => {
          debugger;

          //this.IsDeleteflag =  false;
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          // this.ParentOnInit();
          //this.dialogRef.close();
          this.ShowItem = false;
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors) {
            debugger;
            if (errors.errors["Offender_name_histVm.Offender_name_histDto.Last_name"]) {
              this.newListEditor.last_name = errors.errors["Offender_name_histVm.Offender_name_histDto.Last_name"][0];
            }



            if (errors.errors["Offender_name_histVm.Offender_name_histDto.First_name"]) {
              this.newListEditor.first_name = errors.errors["Offender_name_histVm.Offender_name_histDto.First_name"][0];
            }


          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(item.offender_name_histDto.sin);
        }

      );
  }


  openitem(item: Offender_name_histDto): void {
    debugger;
    this.vm.offender_name_histDto = item;
    this.ShowItem = true;

  }



}
