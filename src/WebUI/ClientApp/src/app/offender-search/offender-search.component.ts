import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { OffenderClient, OffenderDto, OffenderSearchVM } from '../web-api-client';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NewOffenderComponent } from './new-offender/new-offender.component';




@Component({
  selector: 'app-offender-search',
  templateUrl: './offender-search.component.html',
  styleUrls: ['./offender-search.component.scss']
})
export class OffenderSearchComponent implements AfterViewInit, OnInit {
  /*  newListEditor: any = {};*/
  vm: OffenderSearchVM;
  showNewOffender: boolean;

  displayedColumns: string[] = ['first_name', 'AKA', 'date_of_birth', 'race', 'Level', 'sin', 'county_name', 'Agencies'];
  dataSource: MatTableDataSource<OffenderDto>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private offenderClient: OffenderClient,public dialog: MatDialog) {
    this.vm = new OffenderSearchVM();
    this.vm.first_name = ' ';
    this.vm.last_name = ' ';
    this.vm.searchType = '1';

    this.showNewOffender = false;
  }



  ngOnInit(): void {
    debugger;
    this.dataSource = new MatTableDataSource(this.vm.offenders);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  onSubmit(formValue) {
    console.log(formValue)
  }

  ngAfterViewInit() {
    debugger;
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
  }

  newOffender(vm_) {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '500px';
    dialogConfig.width = '700px';
    debugger;
    dialogConfig.data = { firstName: vm_.first_name, lastName: vm_.last_name };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(NewOffenderComponent, dialogConfig);

  }


  Search(vm_) {
    debugger;
    if (vm_.first_name == undefined || vm_.first_name == '')
      vm_.first_name = ' ';

    if (vm_.last_name == undefined || vm_.last_name == '')
      vm_.last_name = ' ';

    if (vm_.searchType == undefined || vm_.searchType == '')
      vm_.searchType = '1';

    this.offenderClient.search(vm_.first_name, vm_.last_name, vm_.searchType).subscribe(
      result => {
        debugger;

        this.vm = result;


        this.dataSource = new MatTableDataSource(this.vm.offenders);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;


        if (result.offenders.length <= 0) {
          debugger;
          alert('No results');
          this.showNewOffender = true;
        }


      },

      error => console.error(error)
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
