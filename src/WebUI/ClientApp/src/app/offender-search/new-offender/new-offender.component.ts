import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { CreateNewOffenderCommand, NewOffenderDto, OffenderClient, OffenderDto } from '../../web-api-client';

export interface DialogData {
  firstName: string;
  lastName: string;

}

@Component({
  selector: 'app-new-offender',
  templateUrl: './new-offender.component.html',
  styleUrls: ['./new-offender.component.scss']
})


export class NewOffenderComponent implements OnInit {

  vm: NewOffenderDto;
  newListEditor: any = {};
  private uiCommonService: UiCommonService;

  constructor(private offenderClient: OffenderClient, uiService: UiCommonService,
    public dialogRef: MatDialogRef<NewOffenderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private route: ActivatedRoute) {

    this.Load();
    this.uiCommonService = uiService;
  }

  createItem(vm_) {
    debugger;

    const createNewOffenderCommand = CreateNewOffenderCommand.fromJS(vm_);
    createNewOffenderCommand.offenderDto = vm_;
 

    this.offenderClient.createNewOffender(createNewOffenderCommand)
      .subscribe(

        result => {
          debugger;
          // this.IsDeleteflag = false;
          this.uiCommonService.toastNotification('created succeeded', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();

        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors.errors["offenderDto.Last_name"]) {
            this.newListEditor.last_name = errors.errors["offenderDto.Last_name"][0];
          }
          if (errors.errors["offenderDto.First_name"]) {
            this.newListEditor.first_name = errors.errors["offenderDto.First_name"][0];
          }

          if (errors.errors["offenderDto.Date_of_birth"]) {
            this.newListEditor.date_of_birth = errors.errors["offenderDto.Date_of_birth"][0];
          }


        

          // setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => { }
          // 'onCompleted' callback.
          // No errors, route to new page here
        

    );


  }

  Load() {
    //this.vm = {
    //  first_name : this.data.firstName,
    //  last_name : this.data.lastName
      
    //};

    //

    this.vm = new NewOffenderDto();
    this.vm.first_name = this.data.firstName;
    this.vm.last_name = this.data.lastName;
    this.vm.sin = 0;
    this.vm.ethnicity = "";
      
  }
  ngOnInit(): void {
  }

}
