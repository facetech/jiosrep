import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { AddendumComponent } from '../addendum/addendum.component';
import { IncidentClient, IncidentDto } from '../web-api-client';
import { IncidentDetailsComponent } from './incident-details/incident-details.component';

@Component({
  selector: 'app-incident',
  templateUrl: './incident.component.html',
  styleUrls: ['./incident.component.scss']
})
export class IncidentComponent implements OnInit {

  _sin: string;
  vm: IncidentDto[];

  constructor(private route: ActivatedRoute, private incidentClient: IncidentClient, public dialog: MatDialog) {
    this._sin = this.route.snapshot.paramMap.get('id');
      debugger;
    this.LoadGrid(this._sin);
  }

  public LoadGrid(id) {
    this.incidentClient.getAll(id).subscribe(
      result => {
        ////debugger;
        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }


  ngOnInit(): void {
  }


  DeleteIncidentItem(Incident_number): void {
    if (confirm("Are you sure to delete this incident")) {

      this.incidentClient.delete(Incident_number)
        .subscribe(

          result => {
            debugger;
            this.LoadGrid(this._sin);
           
          },
          error => {
            debugger;
            let errors = JSON.parse(error.response);

          }
          ,
          () => {

            // this.LoadGrid(item.relationshipsDto2.sin, item.relationshipsDto2.individuaL_NUMBER);
          }

        );

    }
  }


  OpenIncidentItem(incideID) {//Address
    debugger;
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '800px';
    //dialogConfig.width = "600px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { Incident_number: incideID, Mythis: this, offenderFullName:"", sin: this._sin };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(IncidentDetailsComponent, dialogConfig);
  }

  OpenItem(incideID) {//Address
    debugger;
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '600px';
    //dialogConfig.width = "600px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { Sin: this._sin, incident_number: incideID };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(AddendumComponent, dialogConfig);
  }

}
