import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { each } from 'jquery';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { CreateIncidentCommand, CurrentCategoryTypesLto, IncidentdetailClient, IncidentVm, UpdateIncidentCommand } from '../../web-api-client';
import { DatePipe } from '@angular/common';

export interface DialogData {
  Incident_number: number;
  Mythis: any;
  offenderFullName: any;
  sin: any;
}

@Component({
  selector: 'app-incident-details',
  templateUrl: './incident-details.component.html',
  styleUrls: ['./incident-details.component.scss']
})
export class IncidentDetailsComponent implements OnInit {

  vm: IncidentVm;
  current_cat: number;
  newListEditor: any = {};
  ShowItem: boolean = false;

  datepipe: DatePipe = new DatePipe('en-US')

  private uiCommonService: UiCommonService;


  constructor(private IncidentdetailClient: IncidentdetailClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<IncidentDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    this.LoadVm(data.Incident_number);
    this.uiCommonService = uiService;

  }


  back() {
    debugger;
    //this.SaveCheckbox();
    for (var i = 0; i < this.vm.ref_incident_type_categories.length; i++) {
      if (this.current_cat == this.vm.ref_incident_type_categories[i].type_to_category_id) {
        if (i <= 0)
          this.current_cat = this.vm.ref_incident_type_categories[this.vm.ref_incident_type_categories.length - 1].type_to_category_id;
        else
          this.current_cat = this.vm.ref_incident_type_categories[i - 1].type_to_category_id;

        break;
      }
    }
    this.fill_currentcheckbox();
  }

  next() {
    debugger;
    //this.SaveCheckbox();
    for (var i = 0; i < this.vm.ref_incident_type_categories.length; i++) {
      if (this.current_cat == this.vm.ref_incident_type_categories[i].type_to_category_id) {
        if (i != this.vm.ref_incident_type_categories.length - 1)
          this.current_cat = this.vm.ref_incident_type_categories[i + 1].type_to_category_id;
        else
          this.current_cat = this.vm.ref_incident_type_categories[0].type_to_category_id;
        break;
      }
    }
    this.fill_currentcheckbox();
  }

  fill_currentcheckbox() {
    this.vm.currentcheckbox = [];
    for (var i = 0; i < this.vm.allCheckbox.length; i++) {
      if (this.vm.allCheckbox[i].category_Id == this.current_cat) {
        const c = new CurrentCategoryTypesLto();
        c.category_Id = this.vm.allCheckbox[i].category_Id;
        c.category_Name = this.vm.allCheckbox[i].category_Name;
        c.isSelected = this.vm.allCheckbox[i].isSelected;
        c.type_Desc = this.vm.allCheckbox[i].type_Desc;
        c.type_Id = this.vm.allCheckbox[i].type_Id;
        c.type_Name = this.vm.allCheckbox[i].type_Name;
        this.vm.currentcheckbox.push(c);

      }
    }
    debugger;
  }

  SaveCheckbox() {
 
    for (var i = 0; i < this.vm.allCheckbox.length; i++) {

      for (var ii = 0; ii < this.vm.currentcheckbox.length; ii++) {
        if (this.vm.allCheckbox[i].category_Id == this.vm.currentcheckbox[ii].category_Id
          && this.vm.allCheckbox[i].type_Id == this.vm.currentcheckbox[ii].type_Id) {
          this.vm.allCheckbox[i].isSelected = this.vm.currentcheckbox[ii].isSelected;
        }

      }
      

      
    }
    debugger;
  }

  
    //for (var i = 0; i < this.vm.ref_incident_type_categories.length; i++) {

    //  if (this.vm.ref_incident_type_categories[i].type_to_category_id == this.current_cat)
    //  for (var ii = 0; ii < this.vm.ref_incident_types.length; ii++) {
    //    if (this.vm.ref_incident_types[ii].type_to_Category_Id == this.vm.ref_incident_type_categories[i].type_to_category_id) {
    //      const c = new CurrentCategoryTypesLto();
    //      c.category_Id = this.vm.ref_incident_type_categories[i].type_to_category_id;
    //      c.category_Name = this.vm.ref_incident_type_categories[i].incident_category;
    //      c.type_Id = this.vm.ref_incident_types[ii].incident_type_id;
    //      c.type_Name = this.vm.ref_incident_types[ii].incident_type;
    //      c.type_Desc = this.vm.ref_incident_types[ii].incident_description;
    //      this.vm.currentcheckbox.push(c);
    //    }
    //  }
    //}

  LoadVm(Incident_number) {
    this.IncidentdetailClient.get(Incident_number).subscribe(
      result => {
        this.vm = result;
         this.current_cat = result.ref_incident_type_categories[0].type_to_category_id;
        debugger;
        this.fill_currentcheckbox();
       
    //    this.vm.currentCategoryTypesLtos=
        debugger;
      },
      error => console.error(error)
    );
  }


  UpdateItem(item: IncidentVm): void {
    debugger;
    this.newListEditor = [];
    const updateIncidentCommand = UpdateIncidentCommand.fromJS(item);
    updateIncidentCommand.incidentVm = item;
    this.IncidentdetailClient.update(updateIncidentCommand)
      .subscribe(

        result => {
          debugger;
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);

        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          //if (errors) {
          //  if (errors.errors["aliasesVm.AliasesDto.last_name"]) {
          //    this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.last_name"][0];
          //  }

          //  if (errors.errors["aliasesVm.AliasesDto.first_name"]) {
          //    this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.first_name"][0];
          //  }

          //  if (errors.errors["aliasesVm.AliasesDto.middle_name"]) {
          //    this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.middle_name"][0];
          //  }


          //}

        /// setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here

        }

      );
  }


  createItem(item: IncidentVm): void {
    debugger;
    this.newListEditor = [];
    item.incidentDto.sin = this.data.sin;
    const createIncidentCommand = CreateIncidentCommand.fromJS(item);
    createIncidentCommand.incidentVm = item;
    this.IncidentdetailClient.create(createIncidentCommand)
      .subscribe(

        result => {
          debugger;
          this.uiCommonService.toastNotification('create succeeded', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);

        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          //if (errors) {
          //  if (errors.errors["aliasesVm.AliasesDto.last_name"]) {
          //    this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.last_name"][0];
          //  }

          //  if (errors.errors["aliasesVm.AliasesDto.first_name"]) {
          //    this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.first_name"][0];
          //  }

          //  if (errors.errors["aliasesVm.AliasesDto.middle_name"]) {
          //    this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.middle_name"][0];
          //  }


          //}

         // setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here

        }

      );
  }

  openMulltipple() {
    debugger;

    this.ShowItem = true;
    debugger;
  }
  ClosePopup(): void {
    this.ShowItem = false;
  }



  ngOnInit(): void {
  }

}
