import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {
  CaseLoadClient, CaseLoadDto, CaseLoadVM
} from '../web-api-client';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-caseload',
  templateUrl: './caseload.component.html',
  styleUrls: ['./caseload.component.scss']
})
export class CaseloadComponent implements AfterViewInit, OnInit  {
  vm: CaseLoadVM;
  displayedColumns: string[] = ['name', 'date_of_birth', 'race', 'sex', 'sin'];
  dataSource: MatTableDataSource<CaseLoadDto>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private caseLoadClient: CaseLoadClient) {
    
  }
    ngOnInit(): void {
      this.caseLoadClient.get().subscribe(
        result => {
          const different = new CaseLoadDto();
          different.name = "Different Name";
          different.sex = "Female";
          different.date_of_birth = result.caseLoads[0].date_of_birth;
          different.sin = 444;
          result.caseLoads.push(different);
          for (let i = 0; i < 80; i++) {
            result.caseLoads.push(result.caseLoads[0]);
          }


          this.dataSource = new MatTableDataSource(result.caseLoads);

          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          
        },
        error => console.error(error)
      );
    }

  //datatable methods

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
