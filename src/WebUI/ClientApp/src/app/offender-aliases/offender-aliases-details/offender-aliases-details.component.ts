import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { AliasesVm, CreateOffenderAliaseCommand, OffenderAliasesClient, UpdateOffenderAliaseCommand } from '../../web-api-client';


export interface DialogData {
  alias_number: number;
  sin: number;
  Mythis: any;
}




@Component({
  selector: 'app-offender-aliases-details',
  templateUrl: './offender-aliases-details.component.html',
  styleUrls: ['./offender-aliases-details.component.scss']
})
export class OffenderAliasesDetailsComponent implements OnInit {
  vm: AliasesVm;
  newListEditor: any = {};
  private uiCommonService: UiCommonService;
  constructor(private offenderAliasesClient: OffenderAliasesClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderAliasesDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.LoadGrid(data.sin, data.alias_number);
    this.uiCommonService = uiService;
  }

  LoadGrid(sin, id) {
    this.offenderAliasesClient.get(sin, id).subscribe(
      result => {
        debugger;

        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }




  UpdateItem(item: AliasesVm): void {
    debugger;
    this.newListEditor = [];
    const updateOffenderAliaseCommand = UpdateOffenderAliaseCommand.fromJS(item);
    updateOffenderAliaseCommand.aliasesVm = item;
    this.offenderAliasesClient.update(updateOffenderAliaseCommand)
      .subscribe(

        result => {
          debugger;

          //this.IsDeleteflag =  false;
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          // this.ParentOnInit();
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);

        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors) {
            if (errors.errors["aliasesVm.AliasesDto.last_name"]) {
              this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.last_name"][0];
            }

            if (errors.errors["aliasesVm.AliasesDto.first_name"]) {
              this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.first_name"][0];
            }

            if (errors.errors["aliasesVm.AliasesDto.middle_name"]) {
              this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.middle_name"][0];
            }


          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here

        }

      );
  }


  createItem(item: AliasesVm): void {
    debugger;
    this.newListEditor = [];
    const createOffenderAliaseCommand = CreateOffenderAliaseCommand.fromJS(item);
    createOffenderAliaseCommand.aliasesVm = item;
    this.offenderAliasesClient.create(createOffenderAliaseCommand)
      .subscribe(

        result => {
          debugger;

          //this.IsDeleteflag =  false;
          this.uiCommonService.toastNotification('create succeeded', DialogLayoutDisplay.SUCCESS);
          // this.ParentOnInit();
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);

        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors) {
            if (errors.errors["aliasesVm.AliasesDto.last_name"]) {
              this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.last_name"][0];
            }

            if (errors.errors["aliasesVm.AliasesDto.first_name"]) {
              this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.first_name"][0];
            }

            if (errors.errors["aliasesVm.AliasesDto.middle_name"]) {
              this.newListEditor.comments = errors.errors["aliasesVm.AliasesDto.middle_name"][0];
            }


          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here

        }

      );
  }


  ngOnInit(): void {
  }

}
