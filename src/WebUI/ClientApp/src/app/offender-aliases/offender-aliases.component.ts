////import { EventEmitter } from '@angular/core';
////import { Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { OffenderLayoutComponent } from '../offender-layout/offender-layout.component';
import { AliasesDto, AliasesVm, OffenderAliasesClient } from '../web-api-client';
import { OffenderAliasesDetailsComponent } from './offender-aliases-details/offender-aliases-details.component';

@Component({
  selector: 'app-offender-aliases',
  templateUrl: './offender-aliases.component.html',
  styleUrls: ['./offender-aliases.component.scss']
})
export class OffenderAliasesComponent implements OnInit {

  _sin: string;
  newListEditor: any = {};
  vm: AliasesDto[];

  //@Input() currentSin: number;
  //@Output() countChanged: EventEmitter<number> = new EventEmitter();

  constructor(private route: ActivatedRoute, private offenderAliasesClient: OffenderAliasesClient, public dialog: MatDialog) {
    //this._sin = OffenderLayoutComponent.offender.offender.sin;
    this._sin = this.route.snapshot.paramMap.get('id');
    debugger;
    this.LoadGrid(this._sin);
    //const param = this.route.snapshot.paramMap.get('id');
  }


  DeleteItem(alias_number): void {
    if (confirm("Are you sure to delete this alias")) {



      this.offenderAliasesClient.delete(alias_number)
        .subscribe(

          result => {
            debugger;
            this.LoadGrid(this._sin);
            //this.IsDeleteflag = true;
            //this.uiCommonService.toastNotification('Delete succeeded', DialogLayoutDisplay.SUCCESS);
            //this.ClosePopup();
            // window.location.reload();
          },
          error => {
            debugger;
            let errors = JSON.parse(error.response);

          }
          ,
          () => {

            // this.LoadGrid(item.relationshipsDto2.sin, item.relationshipsDto2.individuaL_NUMBER);
          }

        );

    }
  }
  ngOnInit(): void {

  }


  public LoadGrid(id) {
    this.offenderAliasesClient.getAll(id).subscribe(
      result => {
        debugger;
        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }


  OpenItem(alias_number) {//Address
    debugger;
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '300px';
    //dialogConfig.width = "600px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { sin: this._sin, alias_number: alias_number, Mythis: this };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(OffenderAliasesDetailsComponent, dialogConfig);
  }
}
