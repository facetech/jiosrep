import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../ui-common/ui-common-service';
import { CreateOffenderCommentCommand, OffenderCommentsClient, Offender_commentDto, Offender_commentVm, UpdateOffenderCommentCommand } from '../web-api-client';


export interface DialogData {
  id: number;
  fullname: string;
}

@Component({
  selector: 'app-offender-comments',
  templateUrl: './offender-comments.component.html',
  styleUrls: ['./offender-comments.component.scss']
})
export class OffenderCommentsComponent implements OnInit {
  newListEditor: any = {};
  ShowItem: boolean = false;

  vm: Offender_commentVm;
  datepipe: DatePipe = new DatePipe('en-US')

  private uiCommonService: UiCommonService;
  constructor(private OffenderCommentsClient: OffenderCommentsClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderCommentsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { //, public dialog: MatDialog

    this.uiCommonService = uiService;
   // console.log('----------------- id is: ' + data.id);
    this.LoadGrid(data.id);
  }

  ngOnInit(): void {
  }

  LoadGrid(id) {
    debugger;
    this.OffenderCommentsClient.getAll(id).subscribe(
      result => {
        debugger;

        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }

  getitem(offender_comment_id) {
    debugger;
    this.OffenderCommentsClient.get(this.data.id, offender_comment_id).subscribe(
      result => {
        debugger;
        //  this.IsDeleteflag = false;
        this.vm.offender_commentDto = result.offender_commentDto;
        this.ShowItem = true;
        debugger;
      },
      error => console.error(error)
    );
  }

  ClosePopup(): void {
    this.ShowItem = false;
    this.LoadGrid(this.data.id);

  }

  UpdateItem(item: Offender_commentVm): void {
    debugger;
    this.newListEditor = [];
   const updateOffenderCommentCommand = UpdateOffenderCommentCommand.fromJS(item);
   updateOffenderCommentCommand.offender_commentVm = item;
   this.OffenderCommentsClient.update(updateOffenderCommentCommand)
      .subscribe(

        result => {
          debugger;

          //this.IsDeleteflag =  false;
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          // this.ParentOnInit();
          //this.dialogRef.close();
          this.ShowItem = false;
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors) {
            if (errors.errors["offender_commentVm.Offender_commentDto.Comments"]) {
              this.newListEditor.comments = errors.errors["offender_commentVm.Offender_commentDto.Comments"][0];
            }


          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(item.offender_commentDto.sin);
        }

      );
  }

  createItem(item: Offender_commentVm): void {
    debugger;
    this.newListEditor = [];
    const createOffenderCommentCommand = CreateOffenderCommentCommand.fromJS(item);
    createOffenderCommentCommand.offender_commentVm = item;
    this.OffenderCommentsClient.create(createOffenderCommentCommand)
      .subscribe(

        result => {
          debugger;

          //this.IsDeleteflag =  false;
          this.uiCommonService.toastNotification('Create succeeded', DialogLayoutDisplay.SUCCESS);
          // this.ParentOnInit();
          //this.dialogRef.close();
          this.ShowItem = false;
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors) {
            if (errors.errors["offender_commentVm.Offender_commentDto.Comments"]) {
              this.newListEditor.comments = errors.errors["offender_commentVm.Offender_commentDto.Comments"][0];
            }


          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(item.offender_commentDto.sin);
        }

      );
  }

  openitem(item: Offender_commentDto): void {
    debugger;
    this.vm.offender_commentDto = item;
    this.ShowItem = true;

  }

  DeleteItem(OFFENDER_COMMENT_ID): void {
    if (confirm("Are you sure to delete this comment")) {



      this.OffenderCommentsClient.delete(OFFENDER_COMMENT_ID)
        .subscribe(

          result => {
            debugger;
            this.LoadGrid(this.data.id);
            //this.IsDeleteflag = true;
            //this.uiCommonService.toastNotification('Delete succeeded', DialogLayoutDisplay.SUCCESS);
            //this.ClosePopup();
            // window.location.reload();
          },
          error => {
            debugger;
            let errors = JSON.parse(error.response);

          }
          ,
          () => {

            // this.LoadGrid(item.relationshipsDto2.sin, item.relationshipsDto2.individuaL_NUMBER);
          }

        );

    }
  }



}
