import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { CreateOffenderTattoosCommand, OffenderTattoosClient, Scars_marks_tattoosDto, Scars_marks_tattoosVm, UpdateOffenderTattoosCommand } from '../../web-api-client';

export interface DialogData {
  Smt_number: number;
  sin: number;
  Mythis: any;
}

@Component({
  selector: 'app-offender-tattoos-details',
  templateUrl: './offender-tattoos-details.component.html',
  styleUrls: ['./offender-tattoos-details.component.scss']
})
export class OffenderTattoosDetailsComponent implements OnInit {


  vm: Scars_marks_tattoosVm;
  newListEditor: any = {};
  private uiCommonService: UiCommonService;



  constructor(private OffenderTattoosClient: OffenderTattoosClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderTattoosDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.LoadGrid(data.sin, data.Smt_number);
    this.uiCommonService = uiService;
  }



  LoadGrid(sin, id) {
    this.OffenderTattoosClient.get(sin, id).subscribe(
      result => {
        debugger;

        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }




  UpdateItem(item: Scars_marks_tattoosVm): void {
    debugger;
    this.newListEditor = [];
    const updateOffenderTattoosCommand = UpdateOffenderTattoosCommand.fromJS(item);
    updateOffenderTattoosCommand.scars_marks_tattoosVm = item;
    this.OffenderTattoosClient.update(updateOffenderTattoosCommand)
      .subscribe(

        result => {
          debugger;

          //this.IsDeleteflag =  false;
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          // this.ParentOnInit();
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);

        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors) {
            if (errors.errors["scars_marks_tattoosVm.Scars_marks_tattoosDto.scar_mark_tattoo"]) {
              this.newListEditor.comments = errors.errors["scars_marks_tattoosVm.Scars_marks_tattoosDto.scar_mark_tattoo"][0];
            }

           

          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here

        }

      );
  }


  createItem(item: Scars_marks_tattoosVm): void {
    debugger;
    this.newListEditor = [];
    const createOffenderTattoosCommand = CreateOffenderTattoosCommand.fromJS(item);
    createOffenderTattoosCommand.scars_marks_tattoosVm = item;
    this.OffenderTattoosClient.create(createOffenderTattoosCommand)
      .subscribe(

        result => {
          debugger;

          //this.IsDeleteflag =  false;
          this.uiCommonService.toastNotification('create succeeded', DialogLayoutDisplay.SUCCESS);
          // this.ParentOnInit();
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);

        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);



          if (errors) {
            if (errors.errors["scars_marks_tattoosVm.Scars_marks_tattoosDto.scar_mark_tattoo"]) {
              this.newListEditor.comments = errors.errors["scars_marks_tattoosVm.Scars_marks_tattoosDto.scar_mark_tattoo"][0];
            }


          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here

        }

      );
  }
  ngOnInit(): void {
  }

}
