import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { OffenderTattoosClient, Scars_marks_tattoosDto } from '../web-api-client';
import { OffenderTattoosDetailsComponent } from './offender-tattoos-details/offender-tattoos-details.component';

@Component({
  selector: 'app-offender-tattoos',
  templateUrl: './offender-tattoos.component.html',
  styleUrls: ['./offender-tattoos.component.scss']
})
export class OffenderTattoosComponent implements OnInit {

  _sin: string;
  newListEditor: any = {};
  vm: Scars_marks_tattoosDto[];

  constructor(private route: ActivatedRoute, private OffenderTattoosClient: OffenderTattoosClient, public dialog: MatDialog) {
    this._sin = this.route.snapshot.paramMap.get('id');
    debugger;
    this.LoadGrid(this._sin);
  }

  ngOnInit(): void {
  }

  DeleteItem(Smt_number): void {
    if (confirm("Are you sure to delete this tattoo")) {



      this.OffenderTattoosClient.delete(Smt_number)
        .subscribe(

          result => {
            debugger;
            this.LoadGrid(this._sin);
            //this.IsDeleteflag = true;
            //this.uiCommonService.toastNotification('Delete succeeded', DialogLayoutDisplay.SUCCESS);
            //this.ClosePopup();
            // window.location.reload();
          },
          error => {
            debugger;
            let errors = JSON.parse(error.response);

          }
          ,
          () => {

            // this.LoadGrid(item.relationshipsDto2.sin, item.relationshipsDto2.individuaL_NUMBER);
          }

        );

    }
  }

  public LoadGrid(id) {
    this.OffenderTattoosClient.getAll(id).subscribe(
      result => {
        debugger;
        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }


  OpenItem(Smt_number) {//Address
    debugger;
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '350px';
    dialogConfig.width = "700px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { sin: this._sin, Smt_number: Smt_number, Mythis: this };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(OffenderTattoosDetailsComponent, dialogConfig);
  }

}
