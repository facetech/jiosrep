import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { OffenderLayoutComponent } from '../offender-layout/offender-layout.component';
import { OffenderSchoolDto, OffenderSchoolVM, OffenderSchoolHistoryClient } from '../web-api-client';
import { DatePipe } from '@angular/common';
import { OffenderSchoolhistoryDetailsComponent } from './offender-schoolhistory-details/offender-schoolhistory-details.component';

@Component({
  selector: 'app-offender-schoolhistory',
  templateUrl: './offender-schoolhistory.component.html',
  styleUrls: ['./offender-schoolhistory.component.scss']
})
export class OffenderSchoolhistoryComponent implements OnInit {
  _sin: string;
  _school_history_key: string
  vm: OffenderSchoolVM;
  datepipe: DatePipe = new DatePipe('en-US')
  newListEditor: any = {};

  constructor(private route: ActivatedRoute, private offenderSchoolHistoryClient: OffenderSchoolHistoryClient, public dialog: MatDialog) {
    this._sin = this.route.snapshot.paramMap.get('id');
    this.LoadGrid(this._sin);
  }

  public LoadGrid(id) {    
    this.offenderSchoolHistoryClient.getAll(id).subscribe(
      result => {
        this.vm = result;
      },
      error => console.error(error)
    );
  }

  openSchoolHistory(school_history_key) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '60%';
    dialogConfig.data = { sin: this._sin, school_history_key: school_history_key, Mythis: this };
    this.dialog.open(OffenderSchoolhistoryDetailsComponent, dialogConfig);
  }

  DeleteItem(school_history_key): void {
    if (confirm("Are you sure to delete this alias")) {
      this.offenderSchoolHistoryClient.delete(school_history_key)
        .subscribe(
          result => {
            this.LoadGrid(this._sin);
          },
          error => {
            debugger;
            let errors = JSON.parse(error.response);
          }
          ,
          () => {
          }
        );
    }
  }

  ngOnInit(): void {
  }

}
