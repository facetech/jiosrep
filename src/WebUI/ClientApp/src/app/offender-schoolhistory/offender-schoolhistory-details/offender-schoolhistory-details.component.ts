import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { OffenderSchoolVM, CreateSchool_HistoryCommand, OffenderSchoolHistoryClient, UpdateSchool_HistoryCommand } from '../../web-api-client';

export interface DialogData {
  school_history_key: number;
  sin: number;
  Mythis: any;
}

@Component({
  selector: 'app-offender-schoolhistory-details',
  templateUrl: './offender-schoolhistory-details.component.html',
  styleUrls: ['./offender-schoolhistory-details.component.scss']
})

export class OffenderSchoolhistoryDetailsComponent implements OnInit {

  vm: OffenderSchoolVM;
  newListEditor: any = {};
  private uiCommonService: UiCommonService;

  constructor(private offenderSchoolHistoryClient: OffenderSchoolHistoryClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderSchoolhistoryDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.LoadGrid(data.sin, data.school_history_key);
    this.uiCommonService = uiService;
  }

  LoadGrid(sin, id) {
    this.offenderSchoolHistoryClient.get(sin, id).subscribe(
      result => {
        this.vm = result;
      },
      error => console.error(error)
    );
  }

  UpdateItem(item: OffenderSchoolVM): void {
    this.newListEditor = [];
    const updateSchool_HistoryCommand = UpdateSchool_HistoryCommand.fromJS(item);
    updateSchool_HistoryCommand.offenderSchoolVM = item;
    this.offenderSchoolHistoryClient.update(updateSchool_HistoryCommand)
      .subscribe(
        result => {
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);
        },
        error => {
          let errors = JSON.parse(error.response);
          setTimeout(() => document.getElementById("title").focus(), 250);
        },
        () => {
        }
      );
  }

  createItem(item: OffenderSchoolVM): void {
    this.newListEditor = [];
    const updateSchool_HistoryCommand = UpdateSchool_HistoryCommand.fromJS(item);
    updateSchool_HistoryCommand.offenderSchoolVM = item;
    this.offenderSchoolHistoryClient.create(updateSchool_HistoryCommand)
      .subscribe(
        result => {
          this.uiCommonService.toastNotification('create succeeded', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);

        },
        error => {
          let errors = JSON.parse(error.response);
          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
        }

      );
  }

  schoolNameChanged(objch): void {
    this.newListEditor = [];
    const currentSchool_Key = this.vm.school_HistoryDto.school_key;
    const _schoolsDto = this.vm.schoolsDtoList.filter(o => o.school_key == currentSchool_Key);   
    this.vm.schoolsDto.address_line = _schoolsDto[0].address_line;
    this.vm.schoolsDto.city = _schoolsDto[0].city;
    this.vm.schoolsDto.contact_name = _schoolsDto[0].contact_name;
    this.vm.schoolsDto.fax = _schoolsDto[0].fax;
    this.vm.schoolsDto.phone = _schoolsDto[0].phone;
    this.vm.schoolsDto.school_type = _schoolsDto[0].school_type;
    this.vm.schoolsDto.state = _schoolsDto[0].state;
    this.vm.schoolsDto.zip = _schoolsDto[0].zip;
  }

  ngOnInit(): void {
  }

}
