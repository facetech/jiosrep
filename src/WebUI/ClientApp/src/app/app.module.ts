import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { TodoComponent } from './todo/todo.component';
import { OffenderComponent } from './offender/offender.component';
import { ApiAuthorizationModule } from 'src/api-authorization/api-authorization.module';
import { AuthorizeInterceptor } from 'src/api-authorization/authorize.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { TokenComponent } from './token/token.component';
import { UiCommonService } from './ui-common/ui-common-service';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSliderModule } from '@angular/material/slider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table'
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { PhysicalDescriptionComponent } from './offender/physical-description/physical-description.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxMaskModule, IConfig } from 'ngx-mask'


// Import from library
import {
  NgxAwesomePopupModule,
  DialogConfigModule,
  ConfirmBoxConfigModule,
  ToastNotificationConfigModule
} from '@costlydeveloper/ngx-awesome-popup';
import { OffenderLayoutComponent } from './offender-layout/offender-layout.component';
import { CaseLoadLayoutComponent } from './case-load-layout/case-load-layout.component';
import { CaseloadComponent } from './caseload/caseload.component';
import { JuvenileOverviewComponent } from './offender/juvenile-overview/juvenile-overview.component';
import { OffenderAddressComponent } from './offender-address/offender-address.component';
import { AdditionalDetailsComponent } from './offender/additional-details/additional-details.component';
import { PhotosDetailsComponent } from './offender/photos-details/photos-details.component';
import { OffenderstaffComponent } from './offenderstaff/offender-staff.component';
import { OffenderrelationshipComponent } from './offenderrelationship/offenderrelationship.component';
import { OffenderCommentsComponent } from './offender-comments/offender-comments.component';
import { OffenderAliasesComponent } from './offender-aliases/offender-aliases.component';
import { OffenderAliasesDetailsComponent } from './offender-aliases/offender-aliases-details/offender-aliases-details.component';
import { OffenderSchoolhistoryComponent } from './offender-schoolhistory/offender-schoolhistory.component';
import { OffenderSchoolhistoryDetailsComponent } from './offender-schoolhistory/offender-schoolhistory-details/offender-schoolhistory-details.component';
import { NgxPrintModule } from 'ngx-print';
import { OffenderTattoosComponent } from './offender-tattoos/offender-tattoos.component';
import { OffenderTattoosDetailsComponent } from './offender-tattoos/offender-tattoos-details/offender-tattoos-details.component';
import { OffenderSearchComponent } from './offender-search/offender-search.component';
import { OffenderGangComponent } from './offender-gang/offender-gang.component';
import { OffenderGangEditComponent } from './offender-gang/offender-gang-edit/offender-gang-edit.component';
import { OffenderGangNewComponent } from './offender-gang/offender-gang-new/offender-gang-new.component';

import { OffenderNoContactOrdersComponent } from './offender-no-contact-orders/offender-no-contact-orders.component';
import { OffenderNoContactOrdersDetailsComponent } from './offender-no-contact-orders/offender-no-contact-orders-details/offender-no-contact-orders-details.component';
import { OffenderNameHistComponent } from './offender-name-hist/offender-name-hist.component';
import { NewOffenderComponent } from './offender-search/new-offender/new-offender.component';
import { IncidentComponent } from './incident/incident.component';
import { AddendumComponent } from './addendum/addendum.component';
import { OffenderAlertComponent } from './offender-alert/offender-alert.component';
import { OffenderAlertEditComponent } from './offender-alert/offender-alert-edit/offender-alert-edit.component';
import { OffenderAlertNewComponent } from './offender-alert/offender-alert-new/offender-alert-new.component';
import { OffenderAlertDelComponent } from './offender-alert/offender-alert-del/offender-alert-del.component';
import { IncidentDetailsComponent } from './incident/incident-details/incident-details.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    TodoComponent,
    OffenderComponent,
  //  UiCommonService,
    TokenComponent,
    OffenderLayoutComponent,
  CaseLoadLayoutComponent,
  CaseloadComponent,
    JuvenileOverviewComponent,
    OffenderAddressComponent,
    AdditionalDetailsComponent,
    PhysicalDescriptionComponent,
    PhotosDetailsComponent,
    OffenderstaffComponent,
    OffenderrelationshipComponent,
    OffenderCommentsComponent,
    OffenderAliasesComponent,
    OffenderAliasesDetailsComponent,
    OffenderTattoosComponent,
    OffenderTattoosDetailsComponent,
    OffenderSearchComponent,
    OffenderSchoolhistoryComponent,
    OffenderSchoolhistoryDetailsComponent,
    OffenderGangComponent,
    OffenderGangEditComponent,
    OffenderGangNewComponent,
 
    OffenderNoContactOrdersComponent,
    OffenderNoContactOrdersDetailsComponent,
    OffenderNameHistComponent,
    NewOffenderComponent,
    IncidentComponent,
    AddendumComponent,
    OffenderAlertComponent,
    OffenderAlertEditComponent,
    OffenderAlertNewComponent,
    OffenderAlertDelComponent,
    IncidentDetailsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    FontAwesomeModule,
    HttpClientModule,
    FormsModule,
    ApiAuthorizationModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule,
    MatSliderModule, MatTreeModule, MatIconModule, MatButtonModule, MatSidenavModule,
    MatToolbarModule, MatDividerModule, MatListModule, MatExpansionModule, MatDialogModule,
    MatTableModule, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatInputModule,
    NgxAwesomePopupModule.forRoot(), 
    DialogConfigModule.forRoot(), 
    ConfirmBoxConfigModule.forRoot(),
    ToastNotificationConfigModule.forRoot(),
    NgxSmartModalModule.forRoot(), NgxPrintModule,
    NgMultiSelectDropDownModule.forRoot()
    , NgxMaskModule.forRoot()
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true }
    ,   UiCommonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
