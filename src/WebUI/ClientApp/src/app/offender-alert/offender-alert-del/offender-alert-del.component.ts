import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { OffenderAlertVM, OffenderAlertClient, DeleteOffenderAlertCommand } from '../../web-api-client';


export interface DialogData {
  juvenile_alert_key: number;
  CallBackReference: any;
  sin: number;
}

@Component({
  selector: 'app-offender-alert-del',
  templateUrl: './offender-alert-del.component.html',
  styleUrls: ['./offender-alert-del.component.scss']
})
export class OffenderAlertDelComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialogRef: MatDialogRef<OffenderAlertDelComponent>,
    private uiService: UiCommonService,
    private offenderAlertClient: OffenderAlertClient) { }

  DeleteOffenderAlertItem(): void {
    const deleteOffenderAlertCommand = DeleteOffenderAlertCommand.fromJS({ id: this.data.juvenile_alert_key});

    this.offenderAlertClient.delete(deleteOffenderAlertCommand)
      .subscribe(
        result => {
          this.uiService.toastNotification('The offender alert was successfully deleted', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();
          this.data.CallBackReference.LoadGrid(this.data.sin);
        },
        error => {
          let errors = JSON.parse(error.response);
        },
        () => {
        }
      );
  }

  ngOnInit(): void {
  }

}
