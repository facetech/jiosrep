import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { OffenderAlertVM, OffenderAlertClient, UpdateOffenderAlertCommand } from '../../web-api-client';
import { IDropdownSettings, } from 'ng-multiselect-dropdown';

export interface DialogData {
  juvenile_alert_key: number;
  sin: number;
  Mythis: any;
}

@Component({
  selector: 'app-offender-alert-edit',
  templateUrl: './offender-alert-edit.component.html',
  styleUrls: ['./offender-alert-edit.component.scss']
})
export class OffenderAlertEditComponent implements OnInit {
  _sin: string;
  vm: OffenderAlertVM;
  dropdownList = [];
  dropdownSettings: IDropdownSettings = {};
  newListEditor: any = {};
  public joinDtCal: boolean = false;
  public quitDtCal: boolean = false;
  private uiCommonService: UiCommonService;

  constructor(private offenderAlertClient: OffenderAlertClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderAlertEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, public dialog: MatDialog) {
    this.LoadGrid(data.sin, data.juvenile_alert_key);
    this.uiCommonService = uiService;
  }

  LoadGrid(sin, juvenile_alert_key) {
    this.offenderAlertClient.get(sin, juvenile_alert_key).subscribe(
      result => {
        this.vm = result;
      },
      error => console.error(error)
    );
  }

  UpdateOffenderAlertItem(item: OffenderAlertVM): void {
    this.newListEditor = [];
    const updateOffenderAlertCommand = UpdateOffenderAlertCommand.fromJS(item);
    updateOffenderAlertCommand.offenderAlertVM = item;
    this.offenderAlertClient.update(updateOffenderAlertCommand)
      .subscribe(
        result => {
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);
        },
        error => {
          let errors = JSON.parse(error.response);
          setTimeout(() => document.getElementById("Title").focus(), 250);
        },
        () => {
        }
      );
  }

  ngOnInit(): void {
  }

}
