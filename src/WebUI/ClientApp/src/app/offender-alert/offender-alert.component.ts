import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { OffenderLayoutComponent } from '../offender-layout/offender-layout.component';
import { DatePipe } from '@angular/common';
import { OffenderAlertVM, OffenderAlertClient } from '../web-api-client';
import { OffenderAlertEditComponent } from './offender-alert-edit/offender-alert-edit.component';
import { OffenderAlertNewComponent } from './offender-alert-new/offender-alert-new.component';
import { OffenderAlertDelComponent } from './offender-alert-del/offender-alert-del.component';

@Component({
  selector: 'app-offender-alert',
  templateUrl: './offender-alert.component.html',
  styleUrls: ['./offender-alert.component.scss']
})

export class OffenderAlertComponent implements OnInit {
  _sin: string;
  juvenile_alert_key: string
  vm: OffenderAlertVM;
  datepipe: DatePipe = new DatePipe('en-US')
  newListEditor: any = {};

  constructor(private route: ActivatedRoute, private offenderAlertClient: OffenderAlertClient, public dialog: MatDialog) {
    this._sin = this.route.snapshot.paramMap.get('id');
    this.LoadGrid(this._sin);
  }

  public LoadGrid(id) {
    this.offenderAlertClient.getAll(id).subscribe(
      result => {
        this.vm = result;
      },
      error => console.error(error)
    );
  }

  openEditOffenderAlertItem(juvenile_alert_key) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '85%';
    dialogConfig.width = '65%';
    dialogConfig.data = { sin: this._sin, juvenile_alert_key: juvenile_alert_key, Mythis: this };
    this.dialog.open(OffenderAlertEditComponent, dialogConfig);
  }

  openAddOffenderAlertItem(juvenile_alert_key) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '65%';
    dialogConfig.width = '65%';
    dialogConfig.data = { sin: this._sin, juvenile_alert_key: juvenile_alert_key, Mythis: this };
    this.dialog.open(OffenderAlertNewComponent, dialogConfig);
  }

  openDelOffenderAlertItem(juvenile_alert_key) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '45%';
    dialogConfig.width = '40%';
    dialogConfig.data = { sin: this._sin, juvenile_alert_key: juvenile_alert_key, CallBackReference: this };
    this.dialog.open(OffenderAlertDelComponent, dialogConfig);
  }

  ngOnInit(): void {
  }

}
