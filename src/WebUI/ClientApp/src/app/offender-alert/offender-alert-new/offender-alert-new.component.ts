import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { OffenderAlertVM, OffenderAlertClient, CreateOffenderAlertCommand } from '../../web-api-client';

export interface DialogData {
  juvenile_alert_key: number;
  sin: number;
  Mythis: any;
}

@Component({
  selector: 'app-offender-alert-new',
  templateUrl: './offender-alert-new.component.html',
  styleUrls: ['./offender-alert-new.component.scss']
})
export class OffenderAlertNewComponent implements OnInit {
  vm: OffenderAlertVM;
  newListEditor: any = {};
  private uiCommonService: UiCommonService;

  constructor(private offenderAlertClient: OffenderAlertClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderAlertNewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.LoadGrid(data.sin, data.juvenile_alert_key);
    this.uiCommonService = uiService;
  }

  LoadGrid(sin, juvenile_alert_key) {
    this.offenderAlertClient.get(sin, juvenile_alert_key).subscribe(
      result => {
        this.vm = result;
      },
      error => console.error(error)
    );
  }

  createAlertItem(item: OffenderAlertVM): void {
    this.newListEditor = [];
    const createOffenderAlertCommand = CreateOffenderAlertCommand.fromJS(item);
    createOffenderAlertCommand.offenderAlertVM = item;
    this.offenderAlertClient.create(createOffenderAlertCommand)
      .subscribe(
        result => {
          this.uiCommonService.toastNotification('Create Succeeded', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);
          setTimeout(() => document.getElementById("Title").focus(), 250);
        }
        ,
        () => {
        }
      );
  }

  alerttypeChanged(objch): void {
    this.newListEditor = [];
    const currentAlertType = this.vm.juvenile_AlertDto.alert_type;
    const _juvenile_Alert_TypeDto = this.vm.juvenile_Alert_TypeDtoList.filter(o => o.alert_type == currentAlertType);
    this.vm.juvenile_AlertDto.agency_name = _juvenile_Alert_TypeDto[0].agency_name;
  }

   

  ngOnInit(): void {
  }

}
