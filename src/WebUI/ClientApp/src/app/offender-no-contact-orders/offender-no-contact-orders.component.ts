import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { OffenderNoContactOrderClient, RelationshipsDto2 } from '../web-api-client';
import { OffenderNoContactOrdersDetailsComponent } from './offender-no-contact-orders-details/offender-no-contact-orders-details.component';

@Component({
  selector: 'app-offender-no-contact-orders',
  templateUrl: './offender-no-contact-orders.component.html',
  styleUrls: ['./offender-no-contact-orders.component.scss']
})
export class OffenderNoContactOrdersComponent implements OnInit {
  _sin: string;
  newListEditor: any = {};
  vm: RelationshipsDto2[];

  constructor(private route: ActivatedRoute, private offenderNoContactOrderClient: OffenderNoContactOrderClient, public dialog: MatDialog) {
    this._sin = this.route.snapshot.paramMap.get('id');
    debugger;
    this.LoadGrid(this._sin);
  }

  DeleteItem(No_contact_order_key): void {
    if (confirm("Are you sure to delete this contact")) {



      this.offenderNoContactOrderClient.delete(No_contact_order_key)
        .subscribe(

          result => {
            debugger;
            this.LoadGrid(this._sin);
            //this.IsDeleteflag = true;
            //this.uiCommonService.toastNotification('Delete succeeded', DialogLayoutDisplay.SUCCESS);
            //this.ClosePopup();
            // window.location.reload();
          },
          error => {
            debugger;
            let errors = JSON.parse(error.response);

          }
          ,
          () => {

            // this.LoadGrid(item.relationshipsDto2.sin, item.relationshipsDto2.individuaL_NUMBER);
          }

        );

    }
  }
  ngOnInit(): void {

  }


  public LoadGrid(id) {
    this.offenderNoContactOrderClient.getAll(id).subscribe(
      result => {
        debugger;
        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }


  OpenItem(relationship_id, fullName) {//Address

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '620px';
    dialogConfig.width = '850px';

    debugger;
    dialogConfig.data = { sin: this._sin, individual_number: relationship_id, Mythis: this, fullname: fullName };
    this.dialog.open(OffenderNoContactOrdersDetailsComponent, dialogConfig);

    
  }

}
