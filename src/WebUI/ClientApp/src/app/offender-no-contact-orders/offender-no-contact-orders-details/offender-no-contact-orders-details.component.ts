import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { CreateRelationshipCommand, RelationshipsClient, UpdateOffenderRelationshipCommand } from '../../web-api-client';
import { OffenderrelationshipComponent } from '../../offenderrelationship/offenderrelationship.component';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { CreateNoContactOrdersCommand, OffenderNoContactOrderClient, RelationshipsVM, UpdateNoContactOrdersCommand } from '../../web-api-client';

export interface DialogData {
  sin: number;
  individual_number: number;
  Mythis: any;
  fullname: any;
}

@Component({
  selector: 'app-offender-no-contact-orders-details',
  templateUrl: './offender-no-contact-orders-details.component.html',
  styleUrls: ['./offender-no-contact-orders-details.component.scss']
})
export class OffenderNoContactOrdersDetailsComponent implements OnInit {


  datepipe: DatePipe = new DatePipe('en-US')
  newListEditor: any = {};
  issameaddress: any = false;
  //disable: any = "";
  vm: RelationshipsVM;
  private uiCommonService: UiCommonService;


  constructor(private offenderNoContactOrderClient: OffenderNoContactOrderClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderNoContactOrdersDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    this.uiCommonService = uiService;
    console.log('----------------- sin is: ' + data.sin);

    //this.IsDeleteflag = data.IsDeleteForm;
    // this.individual_number_ToDelete = data.individual_number;

    debugger;
    // if (this.IsDeleteflag == false) 
    this.LoadGrid(data.sin, data.individual_number);

  }

  ngOnInit(): void {
  }

  onSubmit(contactForm) {
    console.log(contactForm.value);
  }

  LoadGrid(sin, individual_number) {
    debugger;
    this.offenderNoContactOrderClient.get(sin, individual_number).subscribe(
      result => {
        debugger;
        //  this.IsDeleteflag = false;
        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }


  IsSameAddress(address_line1, address_line2, zip, city, state) {
    if (this.issameaddress == false) {
      //  this.disable = "pointer-events: none;";
      this.vm.relationshipsDto2.mailing_address_line1 = address_line1;
      this.vm.relationshipsDto2.mailing_address_line2 = address_line2;
      this.vm.relationshipsDto2.mailing_zip = zip;
      this.vm.relationshipsDto2.mailing_city = city;
      this.vm.relationshipsDto2.mailing_state = state;
      this.issameaddress = true;
    } else {
      this.issameaddress = false;
      // this.disable = "";
    }

  }


  updateItem(item: RelationshipsVM): void {
    debugger;
    this.newListEditor = [];
    const updateOffenderNoContactOrdersCommand = UpdateNoContactOrdersCommand.fromJS(item);
    updateOffenderNoContactOrdersCommand.relationshipsVm = item;
    this.offenderNoContactOrderClient.update(updateOffenderNoContactOrdersCommand)
      .subscribe(

        result => {
          debugger;

          //this.IsDeleteflag =  false;
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          //this.ParentOnInit();
          this.data.Mythis.LoadGrid(item.relationshipsDto2.sin);
          this.dialogRef.close();
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors) {
            if (errors.errors["RelationshipsVm.RelationshipsDto2.Last_name"]) {
              this.newListEditor.last_name = errors.errors["RelationshipsVm.RelationshipsDto2.Last_name"][0];
            }
            if (errors.errors["RelationshipsVm.RelationshipsDto2.First_name"]) {
              this.newListEditor.first_name = errors.errors["RelationshipsVm.RelationshipsDto2.First_name"][0];
            }

            if (errors.errors["RelationshipsVm.RelationshipsDto2.Date_of_birth"]) {
              this.newListEditor.date_of_birth = errors.errors["RelationshipsVm.RelationshipsDto2.Date_of_birth"][0];
            }


            if (errors.errors["RelationshipsVm.RelationshipsDto2.Social_security_number"]) {
              this.newListEditor.social_security_number = errors.errors["RelationshipsVm.RelationshipsDto2.Social_security_number"][0];
            }

            if (errors.errors["RelationshipsVm.RelationshipsDto2.E_mail_address"]) {
              this.newListEditor.e_mail_address = errors.errors["RelationshipsVm.RelationshipsDto2.E_mail_address"][0];
            }


          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(item.relationshipsDto2.sin, item.relationshipsDto2.relationship_id);
        
        }

      );
  }



  createItem(item: RelationshipsVM): void {
    debugger;
    this.newListEditor = [];
    const createNoContactOrdersCommand = CreateNoContactOrdersCommand.fromJS(item);
    createNoContactOrdersCommand.relationshipsVm = item;
    this.offenderNoContactOrderClient.create(createNoContactOrdersCommand)
      .subscribe(

        result => {
          debugger;
          // this.IsDeleteflag = false;
          this.uiCommonService.toastNotification('created succeeded', DialogLayoutDisplay.SUCCESS);
          //this.ParentOnInit();
          this.data.Mythis.LoadGrid(item.relationshipsDto2.sin);
          this.dialogRef.close();

        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors.errors["RelationshipsVm.RelationshipsDto2.Last_name"]) {
            this.newListEditor.last_name = errors.errors["RelationshipsVm.RelationshipsDto2.Last_name"][0];
          }
          if (errors.errors["RelationshipsVm.RelationshipsDto2.First_name"]) {
            this.newListEditor.first_name = errors.errors["RelationshipsVm.RelationshipsDto2.First_name"][0];
          }

          if (errors.errors["RelationshipsVm.RelationshipsDto2.Date_of_birth"]) {
            this.newListEditor.date_of_birth = errors.errors["RelationshipsVm.RelationshipsDto2.Date_of_birth"][0];
          }


          if (errors.errors["RelationshipsVm.RelationshipsDto2.Social_security_number"]) {
            this.newListEditor.social_security_number = errors.errors["RelationshipsVm.RelationshipsDto2.Social_security_number"][0];
          }

          if (errors.errors["RelationshipsVm.RelationshipsDto2.E_mail_address"]) {
            this.newListEditor.e_mail_address = errors.errors["RelationshipsVm.RelationshipsDto2.E_mail_address"][0];
          }

          // setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(item.relationshipsDto2.sin, item.relationshipsDto2.relationship_id);
          this.data.Mythis.LoadGrid(item.relationshipsDto2.sin);
        }

      );
  }



  //ParentOnInit(): void {
  //  debugger

  //  this.data.Mythis.offenderClient.get2(this.data.sin).subscribe(
  //    result => {
  //      debugger;
  //      this.data.Mythis.vm = result;
  //      this.data.Mythis.fullName = result.offender.first_name + " " + result.offender.last_name;
  //    },
  //    error => console.error(error)
   // );

  //}



}
