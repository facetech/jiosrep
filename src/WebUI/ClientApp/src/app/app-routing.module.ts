import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { HomeComponent } from './home/home.component';
import { TodoComponent } from './todo/todo.component';
import { OffenderComponent } from './offender/offender.component';
import { TokenComponent } from './token/token.component';
import { OffenderLayoutComponent } from './offender-layout/offender-layout.component';
import { CaseLoadLayoutComponent } from './case-load-layout/case-load-layout.component';
import { CaseloadComponent } from './caseload/caseload.component';
import { JuvenileOverviewComponent } from './offender/juvenile-overview/juvenile-overview.component';
import { OffenderAddressComponent } from './offender-address/offender-address.component';
import { OffenderAliasesComponent } from './offender-aliases/offender-aliases.component';
import { OffenderTattoosComponent } from './offender-tattoos/offender-tattoos.component';
import { OffenderSearchComponent } from './offender-search/offender-search.component';
import { OffenderSchoolhistoryComponent } from './offender-schoolhistory/offender-schoolhistory.component';
import { OffenderGangComponent } from './offender-gang/offender-gang.component';
import { OffenderNoContactOrdersComponent } from './offender-no-contact-orders/offender-no-contact-orders.component';
import { OffenderNameHistComponent } from './offender-name-hist/offender-name-hist.component';
import { AddendumComponent } from './addendum/addendum.component';
import { IncidentComponent } from './incident/incident.component';
import { OffenderAlertComponent } from './offender-alert/offender-alert.component';


export const routes: Routes = [
  {
    path: 'offender',
    component: OffenderLayoutComponent,
    children: [
      { path: 'offender/:id', component: OffenderComponent, canActivate: [AuthorizeGuard] },
      { path: 'juvenile-overview/:id', component: JuvenileOverviewComponent, canActivate: [AuthorizeGuard] },
      { path: 'address/:id', component: OffenderAddressComponent, canActivate: [AuthorizeGuard] },
    ]
  }
  ,
  {
    path: 'offender-no-contact-orders',
    component: OffenderLayoutComponent,
    children: [
      { path: ':id', component: OffenderNoContactOrdersComponent, canActivate: [AuthorizeGuard] }
    ]
  }
  ,
 
  {
    path: 'offender-name-hist',
    component: OffenderLayoutComponent,
    children: [
      { path: ':id', component: OffenderNameHistComponent, canActivate: [AuthorizeGuard] }
    ]
  } ,
  {
    path: 'offender-aliases',
    component: OffenderLayoutComponent,
    children: [
      { path: ':id', component: OffenderAliasesComponent, canActivate: [AuthorizeGuard] }
    ]
  }
  ,
  {
    path: 'incident',
    component: OffenderLayoutComponent,
    children: [
      { path: ':id', component: IncidentComponent, canActivate: [AuthorizeGuard] }
    ]
  }
  ,
  {
    path: 'addendum',
    component: OffenderLayoutComponent,
    children: [
      { path: ':id', component: AddendumComponent, canActivate: [AuthorizeGuard] }
    ]
  }
  
  ,
  {
    path: 'offender-schoolhistory',
    component: OffenderLayoutComponent,
    children: [
      { path: ':id', component: OffenderSchoolhistoryComponent, canActivate: [AuthorizeGuard] }
    ]
  }
  ,
  {
    path: 'offender-gang',
    component: OffenderLayoutComponent,
    children: [
      { path: ':id', component: OffenderGangComponent, canActivate: [AuthorizeGuard] }
    ]
  }
  ,
  {
    path: 'offender-alert',
    component: OffenderLayoutComponent,
    children: [
      { path: ':id', component: OffenderAlertComponent, canActivate: [AuthorizeGuard] }
    ]
  }
  ,

  {
    path: 'offender-tattoos',
    component: OffenderLayoutComponent,
    children: [
      { path: ':id', component: OffenderTattoosComponent, canActivate: [AuthorizeGuard] }
    ]
  }
  ,
  {
    path: '',
    component: CaseLoadLayoutComponent,
    children: [
      { path: '', component: HomeComponent },

      { path: 'case-load', component: CaseloadComponent, canActivate: [AuthorizeGuard] },
      { path: 'offender-search', component: OffenderSearchComponent, canActivate: [AuthorizeGuard] },
      { path: 'todo', component: TodoComponent, canActivate: [AuthorizeGuard] },
      { path: 'token', component: TokenComponent, canActivate: [AuthorizeGuard] }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
