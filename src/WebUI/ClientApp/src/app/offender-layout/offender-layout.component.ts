import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MatAccordion } from '@angular/material/expansion';
import { Input } from '@angular/core';
import { OffenderVM } from '../web-api-client';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { OffenderNameHistComponent } from '../offender-name-hist/offender-name-hist.component';


@Component({
  selector: 'app-offender-layout',
  templateUrl: './offender-layout.component.html',
  styleUrls: ['./offender-layout.component.scss']
})
export class OffenderLayoutComponent {
  @ViewChild(MatAccordion) accordion: MatAccordion
  @ViewChild('sidenav') sidenav: MatSidenav;
  // @Input() currentSin: string;
  isExpanded = true;
  showSubmenu: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;




  currentSin = "0";
  currentfullName = "";
  //static Sin = "0";
  static offender: OffenderVM;

  constructor(public dialog: MatDialog) { }
  get VM() {


    if (OffenderLayoutComponent.offender != undefined) {

      this.currentSin = OffenderLayoutComponent.offender.offender.sin.toString();
      this.currentfullName = OffenderLayoutComponent.offender.offender.last_name + " " + OffenderLayoutComponent.offender.offender.first_name;
      return OffenderLayoutComponent.offender;
    }

    return null;
    //else {
    //  //const o = new OffenderVM;
    //  //return o;
    //}


  }

  countChangedHandler(c: string) {
    this.currentSin = c;
    debugger;
    console.log(c);

  }

  mouseenter() {
    if (!this.isExpanded) {
      this.isShowing = true;

    }
  }

  mouseleave() {
    if (!this.isExpanded) {
      this.isShowing = false;

    }
  }


  OpenOffenderNameModal() {

    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '500px';
    dialogConfig.width = '900px';
    //dialogConfig.width = "600px";
    //dialogConfig.scrollStrategy
    dialogConfig.data = { id: this.currentSin, fullname: this.currentfullName };
    // https://material.angular.io/components/dialog/overview
    this.dialog.open(OffenderNameHistComponent, dialogConfig);
  }


}
