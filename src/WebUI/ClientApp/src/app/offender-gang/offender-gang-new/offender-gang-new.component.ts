import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { OffenderGangVM, OffenderGangClient, CreateOffender_GangCommand } from '../../web-api-client';

export interface DialogData {
  offender_gang_id: number;
  sin: number;
  Mythis: any;
}

@Component({
  selector: 'app-offender-gang-new',
  templateUrl: './offender-gang-new.component.html',
  styleUrls: ['./offender-gang-new.component.scss']
})
export class OffenderGangNewComponent implements OnInit {
  vm: OffenderGangVM;
  newListEditor: any = {};
  private uiCommonService: UiCommonService;

  constructor(private offenderGangClient: OffenderGangClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderGangNewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.LoadGrid(data.sin, data.offender_gang_id);
    this.uiCommonService = uiService;
  }

  LoadGrid(sin, offender_gang_id) {
    this.offenderGangClient.get(sin, offender_gang_id).subscribe(
      result => {
        this.vm = result;
      },
      error => console.error(error)
    );
  }

  createItem(item: OffenderGangVM): void {
    this.newListEditor = [];
    const createGang_RelationshipCommand = CreateOffender_GangCommand.fromJS(item);
    createGang_RelationshipCommand.offenderGangVM = item;
    this.offenderGangClient.create(createGang_RelationshipCommand)
      .subscribe(
        result => {
          this.uiCommonService.toastNotification('create succeeded', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);
        },
        error => {
          let errors = JSON.parse(error.response);
          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
        }
      );
  }

  ngOnInit(): void {
  }

}
