import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { OffenderLayoutComponent } from '../offender-layout/offender-layout.component';
import { DatePipe } from '@angular/common';
import { OffenderGangVM, OffenderGangClient } from '../web-api-client';
import { OffenderGangEditComponent } from './offender-gang-edit/offender-gang-edit.component';
import { OffenderGangNewComponent } from './offender-gang-new/offender-gang-new.component';



@Component({
  selector: 'app-offender-gang',
  templateUrl: './offender-gang.component.html',
  styleUrls: ['./offender-gang.component.scss']
})

export class OffenderGangComponent implements OnInit {
  _sin: string;
  offender_gang_id: string
  vm: OffenderGangVM;
  datepipe: DatePipe = new DatePipe('en-US')
  newListEditor: any = {};

  constructor(private route: ActivatedRoute, private offenderGangClient: OffenderGangClient, public dialog: MatDialog) {
    this._sin = this.route.snapshot.paramMap.get('id');
    this.LoadGrid(this._sin);
  }

  public LoadGrid(id) {
    this.offenderGangClient.getAll(id).subscribe(
      result => {
        this.vm = result;
      },
      error => console.error(error)
    );
  }

  openEditOffenderGangItem(offender_gang_id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '55%';
    dialogConfig.width = '70%';
    dialogConfig.data = { sin: this._sin, offender_gang_id: offender_gang_id, Mythis: this };
    this.dialog.open(OffenderGangEditComponent, dialogConfig);
  }

  openAddOffenderGangItem(offender_gang_id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = '55%';
    dialogConfig.data = { sin: this._sin, offender_gang_id: offender_gang_id, Mythis: this };
    this.dialog.open(OffenderGangNewComponent, dialogConfig);
  }

  openDeleteOffenderGangItem(offender_gang_id): void {
    if (confirm("Are you sure to delete this alias")) {
      this.offenderGangClient.delete(offender_gang_id)
        .subscribe(
          result => {
            this.LoadGrid(this._sin);
          },
          error => {
            let errors = JSON.parse(error.response);
          }
          ,
          () => {
          }
        );
    }
  }

  ngOnInit(): void {
  }

}
