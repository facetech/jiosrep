import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../../ui-common/ui-common-service';
import { OffenderGangVM, OffenderGangClient, UpdateOffender_GangCommand } from '../../web-api-client';
import { IDropdownSettings, } from 'ng-multiselect-dropdown';

export interface DialogData {
  offender_gang_id: number;
  sin: number;
  Mythis: any;
}

@Component({
  selector: 'app-offender-gang-edit',
  templateUrl: './offender-gang-edit.component.html',
  styleUrls: ['./offender-gang-edit.component.scss']
})

export class OffenderGangEditComponent implements OnInit {
  _sin: string;
  vm: OffenderGangVM;
  dropdownList = [];
  dropdownSettings: IDropdownSettings = {};
  newListEditor: any = {};
  public joinDtCal: boolean = false;
  public quitDtCal: boolean = false;
  private uiCommonService: UiCommonService;

  constructor(private offenderGangClient: OffenderGangClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderGangEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, public dialog: MatDialog) {
    this.LoadGrid(data.sin, data.offender_gang_id);
    this.uiCommonService = uiService;
  }

  LoadGrid(sin, offender_gang_id) {
    this.offenderGangClient.get(sin, offender_gang_id).subscribe(
      result => {
        this.vm = result;
      },
      error => console.error(error)
    );
  }

  UpdateOffender_GangItem(item: OffenderGangVM): void {
    this.newListEditor = [];
    const updateOffender_GangCommand = UpdateOffender_GangCommand.fromJS(item);
    updateOffender_GangCommand.offenderGangsVM = item;
    this.offenderGangClient.update(updateOffender_GangCommand)
      .subscribe(
        result => {
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          this.dialogRef.close();
          this.data.Mythis.LoadGrid(this.data.sin);
        },
        error => {
          let errors = JSON.parse(error.response);
          setTimeout(() => document.getElementById("title").focus(), 250);
        },
        () => {
        }
      );
  }

  ngOnInit(): void {

    this.dropdownSettings = {
      idField: 'item_id',
      textField: 'item_text',
    };
  }

  joinDate() {
    if (this.joinDtCal == false)
      this.joinDtCal = true
    else
      this.joinDtCal = false
  }

  quitDate() {
    if (this.quitDtCal == false)
      this.quitDtCal = true
    else
      this.quitDtCal = false
  }

  
    
  }

  

