import { DatePipe } from '@angular/common';
import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { UiCommonService } from '../ui-common/ui-common-service';
import { AddendumClient, AddendumVm, CreateAddendumCommand, IAddendumClient, Incident_AddendumDto, UpdateAddendumCommand } from '../web-api-client';


export interface DialogData {
  Sin: number;
  incident_number: number;

}

@Component({
  selector: 'app-addendum',
  templateUrl: './addendum.component.html',
  styleUrls: ['./addendum.component.scss']
})
export class AddendumComponent implements OnInit {
  
  newListEditor: any = {};
  ShowItem: boolean = false;
  vm: AddendumVm;
  datepipe: DatePipe = new DatePipe('en-US')
  private uiCommonService: UiCommonService;

  constructor(private addendumClient: AddendumClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddendumComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData)
  { //, public dialog: MatDialog
    this.uiCommonService = uiService;
    debugger;
    this.LoadGrid(data.incident_number);
  }

  ngOnInit(): void {
  }


  LoadGrid(id) {
     debugger;
    this.addendumClient.getAll(id).subscribe(
      result => {
        debugger;
        this.vm = result;
        debugger;
      },
        
      error => console.error(error)
    );
  }

  getitem(Addendum_id) {
    debugger;
    this.addendumClient.get(Addendum_id, this.data.incident_number).subscribe(
      result => {
        debugger;
        this.vm.incident_AddendumDto = result.incident_AddendumDto;
        this.ShowItem = true;
        debugger;
      },
      error => console.error(error)
    );
  }

  ClosePopup(): void {
    this.ShowItem = false;
    this.LoadGrid(this.data.incident_number);

  }

  UpdateItem(item: AddendumVm): void {
    debugger;
    this.newListEditor = [];
    const updateAddendumCommand = UpdateAddendumCommand.fromJS(item);
    updateAddendumCommand.addendumVm = item;
    this.addendumClient.update(updateAddendumCommand)
      .subscribe(

        result => {
          debugger;
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
          this.ShowItem = false;
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);
          if (errors) {
            if (errors.errors["AddendumVm.Incident_AddendumDto.Comments"]) {
              this.newListEditor.comments = errors.errors["AddendumVm.Incident_AddendumDto.Comments"][0];
            }


          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(item.incident_AddendumDto.incident_number);
        }

      );
  }

  createItem(item: AddendumVm): void {
    debugger;
    this.newListEditor = [];
    const createAddendumCommand = CreateAddendumCommand.fromJS(item);
    createAddendumCommand.addendumVm = item;
    this.addendumClient.create(createAddendumCommand)
      .subscribe(

        result => {
          debugger;
          this.uiCommonService.toastNotification('Create succeeded', DialogLayoutDisplay.SUCCESS);
          this.ShowItem = false;
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors) {
            if (errors.errors["AddendumVm.Incident_AddendumDto.Comments"]) {
              this.newListEditor.comments = errors.errors["AddendumVm.Incident_AddendumDto.Comments"][0];
            }


          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(item.incident_AddendumDto.incident_number);
        }

      );
  }

  openitem(item: Incident_AddendumDto): void {
    debugger;
    this.vm.incident_AddendumDto = item;
    this.ShowItem = true;

  }

  DeleteItem(Addendum_id): void {
    if (confirm("Are you sure to delete this comment")) {



      this.addendumClient.delete(Addendum_id)
        .subscribe(

          result => {
            debugger;
            this.LoadGrid(this.data.incident_number);
          },
          error => {
            debugger;
            let errors = JSON.parse(error.response);

          }
          ,
          () => {
          }

        );

    }
  }



}
