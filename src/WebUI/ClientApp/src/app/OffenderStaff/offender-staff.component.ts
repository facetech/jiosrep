
import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UpdateOffenderStaffCommand, CreateOffenderStaffCommand,StaffClient, StaffDto, StaffVM} from '../web-api-client';
import { UiCommonService } from "../ui-common/ui-common-service";
import { DialogLayoutDisplay } from '@costlydeveloper/ngx-awesome-popup';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { StringMap } from '@angular/compiler/src/compiler_facade_interface';


export interface DialogData {
  id: number;
  fullname: string;
}


@Component({
  selector: 'app-offender-staff',
  templateUrl: './offender-staff.component.html',
  styleUrls: ['./offender-staff.component.scss']
})
export class OffenderstaffComponent{

  ShowAddress: boolean = false;
  ShowMail: boolean = false;
  ShowItem: boolean = false;
  county: String = "";
  agency: String = "";


   datepipe: DatePipe = new DatePipe('en-US')
    newListEditor: any = {};
  
  vm: StaffVM;
  private uiCommonService: UiCommonService;
  constructor(private StaffClient: StaffClient, uiService: UiCommonService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<OffenderstaffComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
 
    this.uiCommonService = uiService;
    console.log('----------------- id is: ' + data.id);
    this.LoadGrid(data.id);
  }
  onSubmit(contactForm) {
    console.log(contactForm.value);

   // this.vm.tcounty
  }

  LoadGrid(id) {
    debugger;
    this.StaffClient.get(id).subscribe(
      result => {
        debugger;

        this.vm = result;
        debugger;
      },
      error => console.error(error)
    );
  }
  ClosePopup(): void {
    this.ShowItem = false;
    this.LoadGrid(this.data.id);
  }

  openitem(item: StaffDto): void {
    debugger;
    this.vm.staffDto = item;
    this.StaffClient.get2(this.data.id, this.vm.staffDto.staff_key).subscribe(
      result => {
        debugger;
        //  this.IsDeleteflag = false;
        this.vm.staffDto = result.staffDto;
        this.vm.tcounty = result.tcounty;
        this.vm.tagency = result.tagency;
        this.ShowItem = true;

        debugger;
      },
      error => console.error(error)
    );

  }
  DeleteItem(staff_key): void {
    if (confirm("Are you sure to delete this Case Manager")) {



      this.StaffClient.delete(staff_key)
        .subscribe(

          result => {
            debugger;
            this.LoadGrid(this.data.id);
          },
          error => {
            debugger;
            let errors = JSON.parse(error.response);

          }
          ,
          () => {

            // this.LoadGrid(item.relationshipsDto2.sin, item.relationshipsDto2.individuaL_NUMBER);
          }

        );

    }
  }
 
  getitem(staff_key) {
    debugger;
    this.StaffClient.get2(this.data.id, staff_key).subscribe(
      result => {
        debugger;
        //  this.IsDeleteflag = false;
        this.vm.staffDto = result.staffDto;
        this.vm.staffDto.county_name = "Alx"
        this.vm.staffDto.agency_name = "Agency";
        this.vm.tcounty = result.tcounty;
        this.vm.tagency = result.tagency;
        
        this.ShowItem = true;
       
        debugger;
      },
      error => console.error(error)
    );
  }

  createItem(item: StaffVM): void {
    debugger;
    this.newListEditor = [];
    const createOffenderStaffCommand = CreateOffenderStaffCommand.fromJS(item);
    createOffenderStaffCommand.staffVm = item;
    createOffenderStaffCommand.sin = this.data.id;
    
    this.StaffClient.create(createOffenderStaffCommand)
      .subscribe(

        result => {
          debugger;
          this.uiCommonService.toastNotification('Create succeeded', DialogLayoutDisplay.SUCCESS);
          this.ShowItem = false;
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);


          if (errors) {
            if (errors.errors["staffVm.StaffDto.Login_name"]) {
              this.newListEditor.login_name = "Login Name is required";
            }
            else if (errors.errors["staffVm.StaffDto.County_name"]) {
              this.newListEditor.county_name = "County Name is required";
            }
            else if (errors.errors["staffVm.StaffDto.Agency_name"]) {
              this.newListEditor.agency_name = "Agency Name is required";
            }
            else if (errors.errors["staffVm.StaffDto.First_name"]) {
              this.newListEditor.first_name = "First Name is required";
            }
            else if (errors.errors["staffVm.StaffDto.Last_name"]) {
              this.newListEditor.last_name = "Last Name is required";
            }
            else if (errors.errors["staffVm.StaffDto.E_mail_address"]) {
              this.newListEditor.e_mail_address = "Email Address is required";
            }

          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(this.data.id);
        }


      );
  }

  updateItem(item: StaffVM): void {
    debugger;
    this.newListEditor = [];
    const updateOffenderStaffCommand = UpdateOffenderStaffCommand.fromJS(item);
    updateOffenderStaffCommand.staffVm = item;
    this.StaffClient.update(updateOffenderStaffCommand)
      .subscribe(

        result => {
          debugger;
          this.uiCommonService.toastNotification('Update succeeded', DialogLayoutDisplay.SUCCESS);
         // this.ParentOnInit();
         // this.dialogRef.close();
          this.ShowItem = false;
        },
        error => {
          debugger;
          let errors = JSON.parse(error.response);
          if (errors) {
            if (errors.errors["StaffVm.StaffDto.Login_name"]) {
              this.newListEditor.login_name = "Login Name is required";
            }
            else if (errors.errors["StaffVm.StaffDto.County_name"]) {
              this.newListEditor.county_name = "County Name is required";
            }
            else if (errors.errors["StaffVm.StaffDto.Agency_name"]) {
              this.newListEditor.agency_name = "Agency Name is required";
            }
            else if (errors.errors["StaffVm.StaffDto.First_name"]) {
              this.newListEditor.first_name = "First Name is required";
            }
            else if (errors.errors["StaffVm.StaffDto.Last_name"]) {
              this.newListEditor.last_name = "Last Name is required";
            }
            else if (errors.errors["StaffVm.StaffDto.E_mail_address"]) {
              this.newListEditor.e_mail_address = "Email Address is required";
            }

          }

          setTimeout(() => document.getElementById("title").focus(), 250);
        }
        ,
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.LoadGrid(this.data.id);
        }

      );
  }



}
