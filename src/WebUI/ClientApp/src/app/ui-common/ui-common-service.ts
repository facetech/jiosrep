import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
  DialogLayoutDisplay,
  ToastNotificationInitializer,
  ToastPositionEnum,
  ToastUserViewTypeEnum,  
} from '@costlydeveloper/ngx-awesome-popup';

@Injectable({
  providedIn: "root",
})

export class UiCommonService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  public toastNotification(message: string, type: DialogLayoutDisplay, title: string = null) {
    const newToastNotification = new ToastNotificationInitializer();
    if (title) {
      newToastNotification.setTitle(title);
    }
    newToastNotification.setMessage(message);

    // Choose layout color type
    newToastNotification.setConfig({
      LayoutType: type, // SUCCESS | INFO | NONE | DANGER | WARNING
      AutoCloseDelay: 4000, // optional
      TextPosition: 'center', // optional
      ToastUserViewType: ToastUserViewTypeEnum.SIMPLE, // STANDARD | SIMPLE
      // TOP_LEFT | TOP_CENTER | TOP_RIGHT | TOP_FULL_WIDTH | BOTTOM_LEFT | BOTTOM_CENTER | BOTTOM_RIGHT | BOTTOM_FULL_WIDTH
      ToastPosition: ToastPositionEnum.TOP_CENTER,
    });

    // Simply open the toast
    newToastNotification.openToastNotification$();
  }
}
