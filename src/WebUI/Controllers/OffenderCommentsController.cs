﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class OffenderCommentsController : ApiControllerBase
    {
        [HttpGet("{sin},{offender_comment_id}")]
        public async Task<ActionResult<Offender_commentVm>> Get(int sin ,long offender_comment_id)
        {
            var offadd = await Mediator.Send(new GetCommentsById() { Offender_comment_id = offender_comment_id, Sin= sin });
            return offadd;
        }

        [HttpGet("{sin}")]
        public async Task<ActionResult<Offender_commentVm>> GetAll(int sin)
        {
            var offadd = await Mediator.Send(new GetCommentsBySin() {  Sin = sin });
            return offadd;
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateOffenderCommentCommand command)
        {
            if (command.offender_commentVm == null || command.offender_commentVm.Offender_commentDto == null )
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
            await Mediator.Send(new DeleteOffenderCommentCommand { Offender_comment_id = id });

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateOffenderCommentCommand command)
        {
            if (command.offender_commentVm == null || command.offender_commentVm.Offender_commentDto == null)
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }

        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}


        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteOffenderRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}

    }
}
