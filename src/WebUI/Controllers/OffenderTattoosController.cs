﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class OffenderTattoosController : ApiControllerBase
    {
        [HttpGet("{sin},{Smt_number}")]
        public async Task<ActionResult<Scars_marks_tattoosVm>> Get(int sin ,long Smt_number)
        {
            var offadd = await Mediator.Send(new GetTattoosById() { Smt_number = Smt_number, Sin= sin });
            return offadd;
        }

        [HttpGet("{sin}")]
        public async Task<ActionResult<List<Scars_marks_tattoosDto>>> GetAll(int sin)
        {
            var offadd = await Mediator.Send(new GetTattoosBySin() {  Sin = sin });
            return offadd;
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateOffenderTattoosCommand command)
        {
            if (command.Scars_marks_tattoosVm == null || command.Scars_marks_tattoosVm.Scars_marks_tattoosDto == null )
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
            await Mediator.Send(new DeleteOffenderTattoosCommand { Smt_number = id });

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateOffenderTattoosCommand command)
        {
            if (command.Scars_marks_tattoosVm == null || command.Scars_marks_tattoosVm.Scars_marks_tattoosDto == null)
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }

        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}


        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteOffenderRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}

    }
}
