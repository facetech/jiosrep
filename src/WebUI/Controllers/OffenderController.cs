﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class OffenderController : ApiControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<OffenderListVM>> Get()
        {
            // return await Mediator.Send(new GetOffendersQuery());
            return await Mediator.Send(new GetOffendersQuery());
        }

        [HttpGet("{first_name},{last_name},{searchType}")]
        public async Task<ActionResult<OffenderSearchVM>> Search(string first_name, string last_name, string searchType)
        {
            // return await Mediator.Send(new GetOffendersQuery());
            return await Mediator.Send(new GetOffendersSearch() { firstName = first_name, lastName = last_name, searchType = searchType });
        }

        [HttpGet("{id}")]
        public async Task<OffenderVM> Get(int id)
        {
            return await Mediator.Send(new GetOffenderByIdQuery() { Sin = id });
        }




        [HttpPut]
        public async Task<ActionResult> CreateNewOffender(CreateNewOffenderCommand command)
        {
            if (command.OffenderDto == null)
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }


        [HttpPost]
        public async Task<ActionResult<long>> Create(CreateOffenderCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(long id, UpdateOffenderCommand command)
        {
            if (command.OffenderVM == null || command.OffenderVM.Offender == null || command.OffenderVM.Offender.Sin == 0)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteTodoListCommand { Id = id });

            return NoContent();
        }
    }
}
