﻿using IJOS.Application.School_History.Commands;
using IJOS.Application.School_History.Queries.GetSchool_History;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    public class OffenderSchoolHistoryController : ApiControllerBase
    {
        [HttpGet("{sin},{school_history_key}")]
        public async Task<ActionResult<OffenderSchoolVM>> Get(int sin, long school_history_key)
        {
            var offadd = await Mediator.Send(new GetSchool_HistoryByHistoryKey() { School_History_Key = school_history_key, Sin = sin });
            return offadd;
        }

        [HttpGet("{sin}")]
        public async Task<ActionResult<OffenderSchoolVM>> GetAll(int sin)
        {
            var offadd = await Mediator.Send(new GetSchool_HistoryBySin() { Sin = sin });
            return offadd;
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateSchool_HistoryCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateSchool_HistoryCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        public async Task<ActionResult> Delete(long id)
        {
            await Mediator.Send(new DeleteSchool_HistoryCommand { School_History_Key = id });

            return NoContent();
        }
    }
}
