﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class IncidentdetailController : ApiControllerBase
    {


        [HttpGet("{id}")]
        public async Task<IncidentVm> Get(long id)
        {
            return await Mediator.Send(new GetIncidentById() { INCIDENT_NUMBER = id });
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateIncidentCommand command)
        {
            if (command.IncidentVm == null || command.IncidentVm.IncidentDto == null )
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }


        [HttpPut]
        public async Task<ActionResult> Update(UpdateIncidentCommand command)
        {
            if (command.IncidentVm == null || command.IncidentVm.IncidentDto == null || command.IncidentVm.IncidentDto.Incident_number == 0)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }


    }
}
