﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class AddressController : ApiControllerBase
    {
        [HttpGet("{id}")]
        public async Task<ActionResult<OffenderAddressVM>> Get(int id)
        {
            var offadd = await Mediator.Send(new GetOffenderAddressesByOffenderId() { Sin = id });
            return offadd;
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateOffenderAddressCommand command)
        {
            if (command.OffenderAddressVM == null || command.OffenderAddressVM.OffenderAddress == null || command.OffenderAddressVM.OffenderAddress.Sin == 0)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }
        [HttpPost]
        public async Task<ActionResult> Create(CreateOffenderAddressCommand command)
        {
            if (command.OffenderAddressVM == null || command.OffenderAddressVM.OffenderAddress == null)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }


    }
}
