﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class IncidentController : ApiControllerBase
    {

        [HttpGet("{sin}")]
        public async Task<ActionResult<List<IncidentDto>>> GetAll(int sin)
        {
            var offadd = await Mediator.Send(new GetIncidentBySin() {  Sin = sin });
            return offadd;
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
            await Mediator.Send(new DeleteIncidentCommand { INCIDENT_NUMBER = id });

            return NoContent();
        }









    }
}
