﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class OffenderAliasesController : ApiControllerBase
    {
        [HttpGet("{sin},{Alias_number}")]
        public async Task<ActionResult<AliasesVm>> Get(int sin ,long Alias_number)
        {
            var offadd = await Mediator.Send(new GetAliasesById() { Alias_number = Alias_number, Sin= sin });
            return offadd;
        }

        [HttpGet("{sin}")]
        public async Task<ActionResult<List<AliasesDto>>> GetAll(int sin)
        {
            var offadd = await Mediator.Send(new GetAliasesBySin() {  Sin = sin });
            return offadd;
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateOffenderAliaseCommand command)
        {
            if (command.AliasesVm == null || command.AliasesVm.AliasesDto == null )
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
            await Mediator.Send(new DeleteOffenderAliaseCommand { Alias_number = id });

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateOffenderAliaseCommand command)
        {
            if (command.AliasesVm == null || command.AliasesVm.AliasesDto == null)
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }

        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}


        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteOffenderRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}

    }
}
