﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class OffenderNoContactOrderController : ApiControllerBase
    {

        [HttpGet("{sin}")]
        public async Task<ActionResult<List<RelationshipsDto2>>> GetAll(int sin)
        {
            var offadd = await Mediator.Send(new GetNoContactOrdersBySin() { Sin = sin });
            return offadd;
        }

        [HttpGet("{sin},{individual_number}")]
        public async Task<ActionResult<RelationshipsVM>> Get(int sin ,long individual_number)
        {
            var offadd = await Mediator.Send(new GetNoContactOrdersById() { IndividualNumber = individual_number, Sin= sin });
            return offadd;
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateNoContactOrdersCommand command)
        {
            if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null )
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteNoContactOrdersCommand { INDIVIDUAL_NUMBER = id });

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateNoContactOrdersCommand command)
        {
            if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}


        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteOffenderRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}

    }
}
