﻿using IJOS.Application.Alert.Commands;
using IJOS.Application.Alert.Queries.GetAlerts;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace IJOS.WebUI.Controllers
{
    public class OffenderAlertController : ApiControllerBase
    {
        [HttpGet("{sin}")]
        public async Task<ActionResult<OffenderAlertVM>> GetAll(int sin)
        {
            var offadd = await Mediator.Send(new GetOffenderAlertBySin() { Sin = sin });
            return offadd;
        }

        [HttpGet("{sin},{juvenile_alert_key}")]
        public async Task<ActionResult<OffenderAlertVM>> Get(int sin, long juvenile_alert_key)
        {
            var offadd = await Mediator.Send(new GetAlertByOffenderAlertID() { Juvenile_Alert_Key = juvenile_alert_key, Sin = sin });
            return offadd;
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateOffenderAlertCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(DeleteOffenderAlertCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateOffenderAlertCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
