﻿using IJOS.Application.Common.Security;
using IJOS.Application;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class AdditionalDetailsController : ApiControllerBase
    {
        [HttpGet("{id}")]
        public async Task<ActionResult<OffenderADVM>> Get(int id)
        {

            var offadd = await Mediator.Send(new GetOffenderADByIdQuery { Sin = id });
            return offadd;
        }
        [HttpPut]
        public async Task<ActionResult> Update(UpdateAdditionalDetailsCommand command)
        {
            if (command.offenderADVM == null || command.offenderADVM.Offender == null || command.offenderADVM.Offender.Sin == 0)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }
    }
}
