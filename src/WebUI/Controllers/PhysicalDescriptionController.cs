﻿using IJOS.Application.PhysicalDescription.Commands.UpdateOffenderPhysicalDescription;
using IJOS.Application.PhysicalDescription.Queries.GetPhysicalDescription;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    public class PhysicalDescriptionController : ApiControllerBase
    {
        [HttpGet("{id}")]
        public async Task<ActionResult<OffenderPhysicalDescriptionVM>> Get(int id)
        {
            var offadd = await Mediator.Send(new GetOffenderPhysicalDescriptionByIdQuery() { Sin = id });
            return offadd;
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(long id, UpdateOffenderPhysicalDescriptionCommand command)
        {
            if (command.OffenderPhysicalDescriptionVM == null || command.OffenderPhysicalDescriptionVM.OffenderPhysicalDescription == null || command.OffenderPhysicalDescriptionVM.OffenderPhysicalDescription.Sin == 0)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }        
    }
}
