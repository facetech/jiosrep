﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class PhotosController : ApiControllerBase
    {
        [HttpGet("{id}")]
        public async Task<ActionResult<PhotoVM>> Get(int id)
        {
            var offadd = await Mediator.Send(new GetPhotosQuery { Sin=id });
            return offadd;
        }

        [HttpGet("{id},{type}")]
        public async Task<ActionResult<PhotosTypeVM>> GetByType(int id,int type)
        {
            var offadd = await Mediator.Send(new GetPhotoByTypeQuery { Sin = id ,Photo_type_id=type});
            return offadd;
        }

        [HttpPost]
        public async Task<ActionResult> Create(
            int photo_id,
         int? Photo_type_id 
        , DateTime? Date_taken 
        , string Location_taken 
        , int? Sin 
        , string Login_name 
        , string Taken_by 
        , string Comments 
        , string Login_created 
        , string Login_edited 
        , bool? Setasschoolid 

            , IFormFileCollection files)
        {


            //if (command == null || command.PhotoVM == null || command.PhotoVM.Photo == null)
            //{
            //    return BadRequest();
            //}

            byte[] fileBytes=null;

            long size = files.Sum(f => f.Length);
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        file.CopyTo(ms);
                        fileBytes = ms.ToArray();
                      
                    }
                }
            }
            var Photo = new Domain.Entities.PhotoDto()
            {
                Photo_id = photo_id,
                Photo_type_id = Photo_type_id,
                Date_taken = Date_taken,
                Location_taken = Location_taken,
                Sin = Sin,
                Login_name = Login_name,
                Taken_by = Taken_by,
                Comments = Comments,
                Login_created = Login_created,
                Login_edited = Login_edited,
                Setasschoolid = Setasschoolid
            };
            if (fileBytes!=null)
            {
                Photo.Photo_data = fileBytes;
            }
            var PhotoVM = new PhotoVM()
            {
                Photo = Photo
            };
            if (photo_id == 0)
            {
                CreatePhotosCommand CreateCommand = new CreatePhotosCommand();
                CreateCommand.PhotoVM = PhotoVM;
                var id = await Mediator.Send(CreateCommand);
            }
            else
            {
                UpdatePhotosCommand UpdateCommand = new UpdatePhotosCommand();
                UpdateCommand.PhotoVM = PhotoVM;
                var id = await Mediator.Send(UpdateCommand);
            }


            return Ok();
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeletePhotosCommand { Id = id });

            return NoContent();
        }

    }
 
}
