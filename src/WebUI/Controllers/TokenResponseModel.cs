﻿namespace IJOS.WebUI.Controllers
{
    internal class TokenResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}