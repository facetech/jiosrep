﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class OffendernamehistController : ApiControllerBase
    {


        [HttpGet("{sin}")]
        public async Task<ActionResult<Offender_name_histVm>> GetAll(int sin)
        {
            var offadd = await Mediator.Send(new GetOffenderNamesHistBySin() {  Sin = sin });
            return offadd;
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateOffenderNameCommand command)
        {
            if (command.Offender_name_histVm == null || command.Offender_name_histVm.Offender_name_histDto == null )
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }




    }
}
