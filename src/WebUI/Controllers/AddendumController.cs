using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class AddendumController : ApiControllerBase
    {
        [HttpGet("{Addendum_id},{IncidentId}")]
        public async Task<ActionResult<AddendumVm>> Get(long Addendum_id, long IncidentId)
        {
            var offadd = await Mediator.Send(new GetAddendumById() { Addendum_id = Addendum_id,IncidentId= IncidentId });
            return offadd;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AddendumVm>> GetAll(long id)
        {
            var offadd = await Mediator.Send(new GetAddendumBYIncidentId() { IncidentId = id });
            return offadd;
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateAddendumCommand command)
        {
            if (command.AddendumVm == null || command.AddendumVm.Incident_AddendumDto == null )
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete("{AddendumId}")]
        public async Task<ActionResult> Delete(long AddendumId)
        {
            await Mediator.Send(new DeleteAddendumCommand { AddendumId = AddendumId });

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateAddendumCommand command)
        {
            if (command.AddendumVm == null || command.AddendumVm.Incident_AddendumDto == null)
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }

        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}


        //[HttpPut]
        //public async Task<ActionResult> Delete(DeleteOffenderRelationshipCommand command)
        //{
        //    if (command.RelationshipsVm == null || command.RelationshipsVm.RelationshipsDto2 == null)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}

    }
}
