﻿using IJOS.Application;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class StaffController : ApiControllerBase
    {
        [HttpGet("{id}")]
        public async Task<ActionResult<StaffVM>> Get(int id)
        {
            var offadd = await Mediator.Send(new GetStaffByOffenderId() { Sin = id });
            return offadd;
        }
        [HttpPost("{sin},{staff_key}")]
        public async Task<ActionResult<StaffVM>> Get2(int sin, long staff_key)
        {
            var offadd = await Mediator.Send(new GetStaffBystaffkey() { Sin = sin,staff_key = staff_key });
            return offadd;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
            await Mediator.Send(new DeleteOffenderStaffCommand { STAFF_KEY = id });

            return NoContent();
        }
        [HttpPost]
        public async Task<ActionResult> Create(CreateOffenderStaffCommand command)
        {
            if (command.staffVm == null || command.staffVm.StaffDto == null)
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }
        [HttpPut]
        public async Task<ActionResult> Update(UpdateOffenderStaffCommand command)
        {
            if (command.StaffVm == null || command.StaffVm.StaffDto == null)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }
        //[HttpPut]
        //public async Task<ActionResult> Update(UpdateOffenderAddressCommand command)
        //{
        //    if (command.OffenderAddressVM == null || command.OffenderAddressVM.OffenderAddress == null || command.OffenderAddressVM.OffenderAddress.sin == 0)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}


    }
}
