﻿using IJOS.Application.Gang.Commands;
using IJOS.Application.Gang.Queries.GetGangs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    public class OffenderGangController : ApiControllerBase
    {
        [HttpGet("{sin}")]
        public async Task<ActionResult<OffenderGangVM>> GetAll(int sin)
        {
            var offadd = await Mediator.Send(new GetOffenderGangBySin() { Sin = sin });
            return offadd;
        }

        [HttpGet("{sin},{offender_gang_id}")]
        public async Task<ActionResult<OffenderGangVM>> Get(int sin, long offender_gang_id)
        {
            var offadd = await Mediator.Send(new GetOffenderGangByGangNumber() { Offender_Gang_Id = offender_gang_id, Sin = sin });
            return offadd;
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateOffender_GangCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateOffender_GangCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
            await Mediator.Send(new DeleteOffender_GangCommand { Offender_Gang_Id = id });
            return NoContent();
        }
    }
}