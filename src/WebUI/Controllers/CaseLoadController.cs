﻿using IJOS.Application.CaseLoad.Queries.GetCaseLoads;
using IJOS.Application.Offender.Commands.UpdateOffender;
using IJOS.Application.Offender.Queries.GetOffender;
using IJOS.Application.TodoLists.Commands.CreateTodoList;
using IJOS.Application.TodoLists.Commands.DeleteTodoList;
using IJOS.Application.TodoLists.Commands.UpdateTodoList;
using IJOS.Application.TodoLists.Queries.ExportTodos;
using IJOS.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IJOS.WebUI.Controllers
{
    [Authorize]
    public class CaseLoadController : ApiControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<CaseLoadVM>> Get()
        {
            return await Mediator.Send(new GetAllCaseLoadsQuery());
        }

        //[HttpGet("{id}")]
        //public async Task<OffenderVM> Get(int id)
        //{
        //    return await Mediator.Send(new GetOffenderByIdQuery() { Sin = id });
        //}
    }
}
