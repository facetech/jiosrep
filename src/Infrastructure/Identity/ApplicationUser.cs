﻿using Microsoft.AspNetCore.Identity;

namespace IJOS.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
