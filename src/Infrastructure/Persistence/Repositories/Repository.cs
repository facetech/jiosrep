﻿using Dapper;
using IJOS.Application.Common.Interfaces;
using IJOS.Application.Persistence_Interfaces;
using IJOS.Domain.Common;
using IJOS.Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Infrastructure.Persistence.Repositories
{
    public class Repository<T> : IRepository<T> where T : AuditableEntity
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly DapperContext _DapperContext;

        public Repository(ApplicationDbContext dbContext, DapperContext dapperContext)
        {
            _dbContext = dbContext;
            _DapperContext = dapperContext;
        }

        public virtual T GetById(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }
        public virtual IQueryable<T> GetAll()
        {
            return _dbContext.Set<T>();
        }
        public async Task<T> GetById(long id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public async Task<IEnumerable<T>> QueryAsync(string query, object parameters = null)
        {
            using (var connection = _DapperContext.CreateConnection())
            {
                return await connection.QueryAsync<T>(query, parameters);
            }
        }

        public async Task<T> QuerySingleAsync(string query, object parameters = null)
        {
            using (var connection = _DapperContext.CreateConnection())
            {
                return await connection.QuerySingleAsync<T>(query, parameters);
            }
        }

        public void Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            _dbContext.SaveChanges();
        }

        public async Task Insert(T entity, CancellationToken cancellationToken = new CancellationToken())
        {
            try
            {
                await _dbContext.Set<T>().AddAsync(entity);
                await _dbContext.SaveChangesAsync(cancellationToken);
            }
            catch (Exception e)
            {
                var aaaaaaaaaaaaaaaaaaaaaaaa = e;
            }
        }
        public async Task Update(T entity, CancellationToken cancellationToken = new CancellationToken())
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
