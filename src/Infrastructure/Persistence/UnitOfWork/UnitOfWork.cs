﻿using IJOS.Application.Persistence_Interfaces;
using IJOS.Domain.Entities;
using IJOS.Infrastructure.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Infrastructure.Persistence.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly DapperContext _dapperContext;
        public UnitOfWork(ApplicationDbContext dbContext, DapperContext dapperContext)
        {
            _dapperContext = dapperContext;
            _dbContext = dbContext;
        }

        private IRepository<IncidentOffenders> _IncidentOffendersRepository;
        public IRepository<IncidentOffenders> IncidentOffendersRepository => _IncidentOffendersRepository ?? (_IncidentOffendersRepository = new Repository<IncidentOffenders>(_dbContext, _dapperContext));


        private IRepository<Jj_links> _JJlinksRepository;
        public IRepository<Jj_links> JJlinksRepository => _JJlinksRepository ?? (_JJlinksRepository = new Repository<Jj_links>(_dbContext, _dapperContext));
        private IRepository<Agency> _agencyRepository;
        public IRepository<Agency> AgencyRepository => _agencyRepository ?? (_agencyRepository = new Repository<Agency>(_dbContext, _dapperContext));


        private IRepository<Incident_detail> _incident_detailRepository;
        public IRepository<Incident_detail> Incident_detailRepository => _incident_detailRepository ?? (_incident_detailRepository = new Repository<Incident_detail>(_dbContext, _dapperContext));

        private IRepository<Incident_approvals> _incident_approvalsRepository;
        public IRepository<Incident_approvals> Incident_approvalsRepository => _incident_approvalsRepository ?? (_incident_approvalsRepository = new Repository<Incident_approvals>(_dbContext, _dapperContext));

        private IRepository<Ref_incident_type> _ref_incident_typeRepository;
        public IRepository<Ref_incident_type> Ref_incident_typeRepository => _ref_incident_typeRepository ?? (_ref_incident_typeRepository = new Repository<Ref_incident_type>(_dbContext, _dapperContext));

        private IRepository<Ref_incident_type_category> _ref_incident_type_categoryRepository;
        public IRepository<Ref_incident_type_category> Ref_incident_type_categoryRepository => _ref_incident_type_categoryRepository ?? (_ref_incident_type_categoryRepository = new Repository<Ref_incident_type_category>(_dbContext, _dapperContext));



        private IRepository<Incident_Addendum> _incident_AddendumRepository;
        public IRepository<Incident_Addendum> Incident_AddendumRepository => _incident_AddendumRepository ?? (_incident_AddendumRepository = new Repository<Incident_Addendum>(_dbContext, _dapperContext));


        private IRepository<Incident> _incidentRepository;
        public IRepository<Incident> IncidentRepository => _incidentRepository ?? (_incidentRepository = new Repository<Incident>(_dbContext, _dapperContext));


        private IRepository<Offender_name_hist> _offender_name_histRepository;
        public IRepository<Offender_name_hist> Offender_name_histRepository => _offender_name_histRepository ?? (_offender_name_histRepository = new Repository<Offender_name_hist>(_dbContext, _dapperContext));


        private IRepository<No_contact_order> _no_contact_orderRepository;
        public IRepository<No_contact_order> No_contact_orderRepository => _no_contact_orderRepository ?? (_no_contact_orderRepository = new Repository<No_contact_order>(_dbContext, _dapperContext));

        private IRepository<Scars_marks_tattoos> _scars_marks_tattoosRepository;
        public IRepository<Scars_marks_tattoos> Scars_marks_tattoosRepository => _scars_marks_tattoosRepository ?? (_scars_marks_tattoosRepository = new Repository<Scars_marks_tattoos>(_dbContext, _dapperContext));


        private IRepository<Smt_codes> _smt_codesRepository;
        public IRepository<Smt_codes> Smt_codesRepository => _smt_codesRepository ?? (_smt_codesRepository = new Repository<Smt_codes>(_dbContext, _dapperContext));


        private IRepository<Aliases> _aliasesRepository;
        public IRepository<Aliases> AliasesRepository => _aliasesRepository ?? (_aliasesRepository = new Repository<Aliases>(_dbContext, _dapperContext));

        private IRepository<Satellite_office> _satellite_officeRepository;
        public IRepository<Satellite_office> Satellite_officeRepository => _satellite_officeRepository ?? (_satellite_officeRepository = new Repository<Satellite_office>(_dbContext, _dapperContext));



        private IRepository<Aspnetusers> _aspnetusersRepository;
        public IRepository<Aspnetusers> AspnetusersRepository => _aspnetusersRepository ?? (_aspnetusersRepository = new Repository<Aspnetusers>(_dbContext, _dapperContext));


        private IRepository<Offender> _offenderRepository;
        private IRepository<Case_load> _caseLoadRepository;
        private IRepository<OffenderPhysicalDescription> _offenderPhysicalDescriptionRepository;

        public IRepository<Offender> OffenderRepository => _offenderRepository ?? (_offenderRepository = new Repository<Offender>(_dbContext, _dapperContext));
        public IRepository<Case_load> CaseLoadRepository => _caseLoadRepository ?? (_caseLoadRepository = new Repository<Case_load>(_dbContext, _dapperContext));

        private IRepository<Offender_comments> _offender_commentRepository;
        public IRepository<Offender_comments> Offender_commentRepository => _offender_commentRepository ?? (_offender_commentRepository = new Repository<Offender_comments>(_dbContext, _dapperContext));

        private IRepository<Offender_comment_history> _offender_comment_historyRepository;
        public IRepository<Offender_comment_history> Offender_comment_historyRepository => _offender_comment_historyRepository ?? (_offender_comment_historyRepository = new Repository<Offender_comment_history>(_dbContext, _dapperContext));



        private IRepository<OffenderOverview> _offenderOverviewRepository;
        public IRepository<OffenderOverview> OffenderOverviewRepository => _offenderOverviewRepository ?? (_offenderOverviewRepository = new Repository<OffenderOverview>(_dbContext, _dapperContext));


        private IRepository<Photo> _photoRepository;
        public IRepository<Photo> PhotoRepository => _photoRepository ?? (_photoRepository = new Repository<Photo>(_dbContext, _dapperContext));


        private IRepository<Photo_types> _phototypeRepository;
        public IRepository<Photo_types> PhototypeRepository => _phototypeRepository ?? (_phototypeRepository = new Repository<Photo_types>(_dbContext, _dapperContext));



        private IRepository<OffenderAddress> _offenderAddressRepository;

        public IRepository<OffenderAddress> OffenderAddressRepository => _offenderAddressRepository ?? (_offenderAddressRepository = new Repository<OffenderAddress>(_dbContext, _dapperContext));

        private IRepository<DriversLicenseNumbers> _driversLicenseNumbers;

        public IRepository<DriversLicenseNumbers> DriversLicenseNumbersRepository => _driversLicenseNumbers ?? (_driversLicenseNumbers = new Repository<DriversLicenseNumbers>(_dbContext, _dapperContext));


        private IRepository<OffenderAddressAudit> _offenderAddressAuditRepository;

        public IRepository<OffenderAddressAudit> OffenderAddressAuditRepository => _offenderAddressAuditRepository ?? (_offenderAddressAuditRepository = new Repository<OffenderAddressAudit>(_dbContext, _dapperContext));



        private IRepository<Staff> _StaffRepository;

        public IRepository<Staff> StaffRepository => _StaffRepository ?? (_StaffRepository = new Repository<Staff>(_dbContext, _dapperContext));


        private IRepository<LivingArrangements> _livingArrangementRepository;

        public IRepository<LivingArrangements> LivingArrangementRepository => _livingArrangementRepository ?? (_livingArrangementRepository = new Repository<LivingArrangements>(_dbContext, _dapperContext));

        private IRepository<ZipCityStateCounty> _zipCityStateCountyRepository;

        public IRepository<ZipCityStateCounty> ZipCityStateCountyRepository => _zipCityStateCountyRepository ?? (_zipCityStateCountyRepository = new Repository<ZipCityStateCounty>(_dbContext, _dapperContext));

        private IRepository<MaritalStatus> _maritalStatusRepository;

        public IRepository<MaritalStatus> MaritalStatusRepository => _maritalStatusRepository ?? (_maritalStatusRepository = new Repository<MaritalStatus>(_dbContext, _dapperContext));



        private IRepository<LanguageTypes> _languageTypesRepository;

        public IRepository<LanguageTypes> LanguageTypesRepository => _languageTypesRepository ?? (_languageTypesRepository = new Repository<LanguageTypes>(_dbContext, _dapperContext));



        private IRepository<Relationships> _relationshipsRepository;
        public IRepository<Relationships> RelationshipsRepository => _relationshipsRepository ?? (_relationshipsRepository = new Repository<Relationships>(_dbContext, _dapperContext));

        public IRepository<OffenderPhysicalDescription> OffenderPhysicalDescriptionRepository => _offenderPhysicalDescriptionRepository ?? (_offenderPhysicalDescriptionRepository = new Repository<OffenderPhysicalDescription>(_dbContext, _dapperContext));



        private IRepository<Relationship_types> _Relationship_typesRepository;
        public IRepository<Relationship_types> Relationship_typesRepository => _Relationship_typesRepository ?? (_Relationship_typesRepository = new Repository<Relationship_types>(_dbContext, _dapperContext));


        private IRepository<Relationship_linkage> _Relationship_linkageRepository;
        public IRepository<Relationship_linkage> Relationship_linkageRepository => _Relationship_linkageRepository ?? (_Relationship_linkageRepository = new Repository<Relationship_linkage>(_dbContext, _dapperContext));


        private IRepository<Ed_ethnicity> _EdethnicityRepository;
        public IRepository<Ed_ethnicity> EdEthnicityRepository => _EdethnicityRepository ?? (_EdethnicityRepository = new Repository<Ed_ethnicity>(_dbContext, _dapperContext));


        private IRepository<Race_types> _RacetypesRepository;
        public IRepository<Race_types> RaceTypesRepository => _RacetypesRepository ?? (_RacetypesRepository = new Repository<Race_types>(_dbContext, _dapperContext));


        private IRepository<Relationship_status> _RelationshipstatusRepository;
        public IRepository<Relationship_status> RelationshipStatusRepository => _RelationshipstatusRepository ?? (_RelationshipstatusRepository = new Repository<Relationship_status>(_dbContext, _dapperContext));

        private IRepository<School_History> _school_HistoryRepository;
        public IRepository<School_History> School_HistoryRepository => _school_HistoryRepository ?? (_school_HistoryRepository = new Repository<School_History>(_dbContext, _dapperContext));

        private IRepository<Schools> _schoolsRepository;
        public IRepository<Schools> SchoolsRepository => _schoolsRepository ?? (_schoolsRepository = new Repository<Schools>(_dbContext, _dapperContext));
        private IRepository<Reason_Left_School> _reason_Left_SchoolRepository;
        public IRepository<Reason_Left_School> Reason_Left_SchoolRepository => _reason_Left_SchoolRepository ?? (_reason_Left_SchoolRepository = new Repository<Reason_Left_School>(_dbContext, _dapperContext));

        private IRepository<OffenderSchool> _offenderSchoolRepository;
        public IRepository<OffenderSchool> OffenderSchoolRepository => _offenderSchoolRepository ?? (_offenderSchoolRepository = new Repository<OffenderSchool>(_dbContext, _dapperContext));


        private IRepository<Gang> _gangRepository;
        public IRepository<Gang> GangRepository => _gangRepository ?? (_gangRepository = new Repository<Gang>(_dbContext, _dapperContext));

        private IRepository<Offender_Gang> _offender_GangRepository;
        public IRepository<Offender_Gang> Offender_GangRepository => _offender_GangRepository ?? (_offender_GangRepository = new Repository<Offender_Gang>(_dbContext, _dapperContext));

        public IRepository<Master_Column> _master_ColumnRepository;
        public IRepository<Master_Column> Master_ColumnRepository => _master_ColumnRepository ?? (_master_ColumnRepository = new Repository<Master_Column>(_dbContext, _dapperContext));

        private IRepository<GangOffender> _gangOffenderRepository;
        public IRepository<GangOffender> GangOffenderRepository => _gangOffenderRepository ?? (_gangOffenderRepository = new Repository<GangOffender>(_dbContext, _dapperContext));

        private IRepository<Juvenile_Alert_Type> _juvenile_Alert_TypeRepository;
        public IRepository<Juvenile_Alert_Type> Juvenile_Alert_TypeRepository => _juvenile_Alert_TypeRepository ?? (_juvenile_Alert_TypeRepository = new Repository<Juvenile_Alert_Type>(_dbContext, _dapperContext));
        
        private IRepository<Juvenile_Alert> _juvenile_AlertRepository;
        public IRepository<Juvenile_Alert> Juvenile_AlertRepository => _juvenile_AlertRepository ?? (_juvenile_AlertRepository = new Repository<Juvenile_Alert>(_dbContext, _dapperContext));
    }
}
