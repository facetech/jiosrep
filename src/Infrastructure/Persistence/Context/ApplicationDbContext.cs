﻿using IJOS.Application.Common.Interfaces;
using IJOS.Domain.Common;
using IJOS.Domain.Entities;
using e = IJOS.Domain.Entities;
using IJOS.Infrastructure.Identity;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace IJOS.Infrastructure.Persistence
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>, IApplicationDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;
        private readonly IDomainEventService _domainEventService;

        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions,
            ICurrentUserService currentUserService,
            IDomainEventService domainEventService,
            IDateTime dateTime) : base(options, operationalStoreOptions)
        {
            _currentUserService = currentUserService;
            _domainEventService = domainEventService;
            _dateTime = dateTime;
        }

        public DbSet<IncidentOffenders> IncidentOffenders { get; set; }
        public DbSet<Staff> Staff { get; set; }
        public DbSet<Agency> Agency { get; set; }
        public DbSet<Jj_links> Jj_links { get; set; }


        public DbSet<Incident_Addendum> INCIDENT_ADDENDUM { get; set; }
        public DbSet<Incident> Incident { get; set; }
        public DbSet<Offender_name_hist> Offender_name_hist { get; set; }
        public DbSet<Incident_detail> Incident_detail { get; set; }
        public DbSet<Incident_approvals> Incident_approvals { get; set; }
        public DbSet<Ref_incident_type> Ref_incident_type { get; set; }
        public DbSet<Ref_incident_type_category> Ref_incident_type_category { get; set; }
 

      
        public DbSet<No_contact_order> No_contact_order { get; set; }
        public DbSet<Scars_marks_tattoos> Scars_marks_tattoos { get; set; }
        public DbSet<Smt_codes> Smt_codes { get; set; }
        public DbSet<Satellite_office> Satellite_office { get; set; }
        public DbSet<Aliases> Aliases { get; set; }
        public DbSet<Aspnetusers> Aspnetusers { get; set; }
        public DbSet<Offender_comments> Offender_comments { get; set; }
        public DbSet<Offender_comment_history> Offender_comment_history { get; set; }

        public DbSet<TodoItem> TodoItems { get; set; }

        public DbSet<TodoList> TodoLists { get; set; }

        public DbSet<Offender> Offender { get; set; }
        public DbSet<Photo> Photo { get; set; }
        public DbSet<OffenderAddress> Offender_Address { get; set; }
        public DbSet<OffenderPhysicalDescription> OffenderPhysicalDesc { get; set; }
        public DbSet<OffenderAddressAudit> Offender_Address_Audit { get; set; }
        public DbSet<LivingArrangements> Living_Arrangements { get; set; }
    

        public DbSet<Photo_types> Photo_types { get; set; }
        public DbSet<ZipCityStateCounty> Zip_City_State_County { get; set; }
        public DbSet<DriversLicenseNumbers> Drivers_License_Numbers { get; set; }
        
        public DbSet<LanguageTypes> Language_Types { get; set; }

        public DbSet<MaritalStatus> Marital_Status { get; set; }

        public DbSet<Relationship_linkage> Relationship_linkage { get; set; }
        public DbSet<Relationship_types> Relationship_types { get; set; }
        public DbSet<Ed_ethnicity> Ed_ethnicity { get; set; }
        public DbSet<Race_types> Race_types { get; set; }
        public DbSet<Relationship_status> Relationship_status { get; set; }

        public DbSet<School_History> School_History { get; set; }
        public DbSet<Schools> Schools { get; set; }
        public DbSet<Reason_Left_School> Reason_Left_School { get; set; }
        public DbSet<OffenderSchool> OffenderSchool { get; set; }
        public DbSet<Gang> Gang { get; set; }
        public DbSet<Offender_Gang> Offender_Gang { get; set; }
        public DbSet<GangOffender> GangOffender { get; set; }
        public DbSet<Juvenile_Alert_Type> Juvenile_Alert_Type { get; set; }
        public DbSet<Juvenile_Alert> Juvenile_Alert { get; set; }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.Created_by = _currentUserService.UserId;
                        entry.Entity.Created_date = _dateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.Modified_by = _currentUserService.UserId;
                        entry.Entity.Modified_date = _dateTime.Now;
                        break;
                }
            }
            base.Database.EnsureCreated();
            var result = await base.SaveChangesAsync(cancellationToken);

            await DispatchEvents();

            return result;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            builder.HasDefaultSchema("IJOS");
           
  builder.Entity<Relationship_linkage>()
      .HasKey(p => new { p.Relationship_id, p.Sin });

            builder.Entity<Satellite_office>()
          .HasKey(p => new { p.Agency_branch, p.Agency_name });

            builder.Entity<Relationships>().ToTable("Relationships");


            base.OnModelCreating(builder);
        }

        private async Task DispatchEvents()
        {
            while (true)
            {
                var domainEventEntity = ChangeTracker.Entries<IHasDomainEvent>()
                    .Select(x => x.Entity.DomainEvents)
                    .SelectMany(x => x)
                    .Where(domainEvent => !domainEvent.IsPublished)
                    .FirstOrDefault();
                if (domainEventEntity == null) break;

                domainEventEntity.IsPublished = true;
                await _domainEventService.Publish(domainEventEntity);
            }
        }
    }
}
