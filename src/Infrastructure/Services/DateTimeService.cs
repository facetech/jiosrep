﻿using IJOS.Application.Common.Interfaces;
using System;

namespace IJOS.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
