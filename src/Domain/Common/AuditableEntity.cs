﻿using System;

namespace IJOS.Domain.Common
{
    public abstract class AuditableEntity 
    {
        public DateTime ? Created_date { get; set; }

        public string Created_by { get; set; }

        public DateTime? Modified_date { get; set; }

        public string Modified_by { get; set; }
    }
}
