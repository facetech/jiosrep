﻿using IJOS.Domain.Common;
using IJOS.Domain.Entities;

namespace IJOS.Domain.Events
{
    public class TodoItemCreatedEvent : DomainEvent
    {
        public TodoItemCreatedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
