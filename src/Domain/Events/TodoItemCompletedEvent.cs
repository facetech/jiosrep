﻿using IJOS.Domain.Common;
using IJOS.Domain.Entities;

namespace IJOS.Domain.Events
{
    public class TodoItemCompletedEvent : DomainEvent
    {
        public TodoItemCompletedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
