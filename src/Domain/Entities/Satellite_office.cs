﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Satellite_office : AuditableEntity
    {
        public string Agency_branch { get; set; }
        public string Agency_name { get; set; }
        public string County_name { get; set; }
        public string Address { get; set; }
        public long? Branch_id { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Support_1 { get; set; }
        public string Support_2 { get; set; }
        public string Is_active { get; set; }
        public string Mailing_address { get; set; }
        public string Agency_type { get; set; }

    } // class Satellite_office





}

