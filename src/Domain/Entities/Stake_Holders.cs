﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Stake_Holders
    {
        [Key]
        public int STAKE_HOLDER_KEY { get; set; }
        public Nullable<double> STAFF_KEY { get; set; }
        public string STAKE_HOLDER_TYPE { get; set; }
        public string AGENCY_BRANCH { get; set; }
        public string AGENCY_NAME { get; set; }
        public Nullable<double> CASE_LOAD_NUMBER { get; set; }
        public Nullable<double> SIN { get; set; }
        public string GET_EMAIL { get; set; }
        public string STAFF_NAME { get; set; }
        public Nullable<System.DateTime> START_DATE { get; set; }
        public string LAST_USER { get; set; }
        public Nullable<System.DateTime> DATE_CHANGED { get; set; }
        public string SUPERVISION_LEVEL { get; set; }
        public string PHONE { get; set; }
        public virtual Offender Offender { get; set; }
    }
}
