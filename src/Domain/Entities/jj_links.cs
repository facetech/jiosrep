﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{

    public class Jj_links : AuditableEntity
    {
        [Key]
     
            public long Jj_links_id { get; set; }
            public long Sin { get; set; }
            public long Staff_key { get; set; }
            public DateTime Update_date { get; set; }
            public string Updated_by { get; set; }
            public DateTime? Started_date { get; set; }
            public string Relationship { get; set; }
            public long? Relationship_id { get; set; }
            public DateTime? Sup_level_date { get; set; }
            public string Supervision_level { get; set; }
            public string Group_type { get; set; }
            public string Agency_name { get; set; }
            public string Agency_branch { get; set; }
            public string Is_guardian { get; set; }
            public string Paper_file_location { get; set; }
            public long? Restitution_number { get; set; }
            public long? Disposition_key { get; set; }
            public string Countyoffendernumber { get; set; }
            public string Jj_comment { get; set; }
            public DateTime? Sup_level_end { get; set; }
            public DateTime? End_date { get; set; }
            public string Macro_level { get; set; }

        } // class jj_links
    }
