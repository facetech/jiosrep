﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Ref_incident_type_category : AuditableEntity
    {
        [Key]
        public long Type_to_category_id { get; set; }
        public string Incident_category { get; set; }
        public string Agency_name { get; set; }
        public string Agency_branch { get; set; }
        public long? Category_order { get; set; }
        public long? Type_order { get; set; }

        public DateTime? Create_date { get; set; }
        public string Is_active { get; set; }
      
    } // class REF_INCIDENT_TYPE_CATEGORY

}
