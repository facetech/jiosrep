﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Incident_Addendum : AuditableEntity
    {
        [Key]
        public long Addendum_id { get; set; }
        public long Incident_number { get; set; }
        public string Comments { get; set; }
        public long? Staff_key { get; set; }
        public DateTime Update_date { get; set; }
        public string Is_active { get; set; }
        public long? Group_addendum_id { get; set; }
        public DateTime? Create_date { get; set; }

    }
}
