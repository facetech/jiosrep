﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{

    public class LivingArrangements : AuditableEntity
    {
        [Key]
        public string living_arrangement { get; set; }
        public long? order_by { get; set; }
        public DateTime created_date { get; set; }
        public DateTime? modified_date { get; set; }
        public string modified_by { get; set; }
        public string created_by { get; set; }
    }
}

