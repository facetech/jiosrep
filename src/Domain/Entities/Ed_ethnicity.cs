﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{


    public class Ed_ethnicity : AuditableEntity
    {
        [Key]
        public float Ethnicity_id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string Ethnicity { get; set; }
    } // class ED_ETHNICITY
}