﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{


    public class Relationship_status : AuditableEntity
    {
        [Key]
        public string Status { get; set; }
    } // class RELATIONSHIP_STATUS
}