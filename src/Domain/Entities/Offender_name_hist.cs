﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Offender_name_hist : AuditableEntity
    {

        [Key]
        public long Offender_name_hist_id { get; set; }
        public long Sin { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Middle_name { get; set; }
        public string Suffix { get; set; }
        public string Reason { get; set; }

    }   

}
