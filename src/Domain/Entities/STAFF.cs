﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
        public class Staff : AuditableEntity
        {
   
            [Key]
            public long Staff_key { get; set; }
            public string Login_name { get; set; }
            public string County_name { get; set; }
            public string Agency_name { get; set; }
            public string First_name { get; set; }
            public string Last_name { get; set; }
            public string Job_title { get; set; }
            public string Supervisor_name { get; set; }
            public string Phone_number { get; set; }
            public string Extension { get; set; }
            public string Fax_number { get; set; }
            public string E_mail_address { get; set; }
            public string Is_active { get; set; }
            public string Agency_branch { get; set; }
            public long? Pass_change_freq { get; set; }
            public long? Pass_change_grace { get; set; }
            public DateTime? Pass_change_date { get; set; }
            public string Mobile_number { get; set; }
            public string Home_number { get; set; }
            public string Account_status { get; set; }
            public string Nick_name { get; set; }
            public string Middle_name { get; set; }
            public long? Supervisor_key { get; set; }
            public string Ora_account { get; set; }

        } // class STAFF

    

        
}

