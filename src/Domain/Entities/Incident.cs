﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Incident : AuditableEntity
    {

        [Key]
        public long Incident_number { get; set; }
        public DateTime Date_of_incident { get; set; }
        public long Sin { get; set; }
        public string Description { get; set; }
        public string Location_of_incident { get; set; }
        public string Incident_status { get; set; }
        public long? Group_incident_id { get; set; }
         public string Witnesses { get; set; }
    

    } 

}
