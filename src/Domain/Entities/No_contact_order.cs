﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class No_contact_order : AuditableEntity
    { 
        [Key]
        public long No_contact_order_key { get; set; }
        public string Relationship_to_offender { get; set; }
        public long Sin { get; set; }
        public long Individual_number { get; set; }
        public long? Offense_number { get; set; }
        public long? Disposition_key { get; set; }
        public string Comments { get; set; }
        public long? Individual_number_contact { get; set; }
        public string Status { get; set; }
        public DateTime? Create_date { get; set; }
        public long? Last_user { get; set; }
        public DateTime? Last_edit { get; set; }
 
    } // class NO_CONTACT_ORDER


}

