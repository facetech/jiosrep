﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{

    public class IncidentOffenders : AuditableEntity
    {             
            public string Room { get; set; }
            public string Group { get; set; }
            public string Juvenile { get; set; }
            [Key]
            public long Ijos { get; set; }
            public string Gender { get; set; }
            public DateTime Dob { get; set; }
            public decimal? Age { get; set; }
        } // class IncidentOffenders
    
}
