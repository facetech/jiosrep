﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Incident_approvals : AuditableEntity
    {
        [Key]
        public long Approval_number { get; set; }
        public long Incident_number { get; set; }
        public string Sent_to { get; set; }
        public string Sent_as { get; set; }
        public string Action_type { get; set; }
        public string Comments { get; set; }
       
 

    } // class INCIDENT_APPROVALS

}
