﻿using IJOS.Domain.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace IJOS.Domain.Entities
{
    public class Master_Column : AuditableEntity
    {
        [Key]
        public long Masterkey { get; set; }
        public string Rank_offendergang { get; set; }
        public string Initiatedby_offendergang { get; set; }
        public string Status_offendergang { get; set; }
        public int? Grade_schoolhistory { get; set; }
    }
}