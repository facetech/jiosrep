﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class MaritalStatus : AuditableEntity
    {
        [Key]
        public int Marital_id { get; set; }
        public string Status { get; set; }
        public string Is_active { get; set; }
        public int? Order_by { get; set; }
    } // class Marital_Status
}