﻿using System;
using IJOS.Domain.Common;
using System.ComponentModel.DataAnnotations;

namespace IJOS.Domain.Entities
{
    public class Juvenile_Alert_Type : AuditableEntity
    {
        [Key]
        public long Alert_type_id { get; set; }
        public string Alert_type { get; set; }
        public long Critical_level { get; set; }
        public string Agency_name { get; set; }
        public bool? Is_active { get; set; }
        public long? Order_by { get; set; }
        public DateTime Created_date { get; set; }
        public string Created_by { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }

    }
}
