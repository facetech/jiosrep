﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    [Table("OFFENDER", Schema = "IJOS")]
    public class OffenderPhysicalDescription : AuditableEntity
    {
        [Key]
        public long SIN { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public System.DateTime DATE_OF_BIRTH { get; set; }
        public Nullable<decimal> HEIGHT { get; set; }
        public Nullable<decimal> WEIGHT { get; set; }
        public string HAIR_COLOR { get; set; }
        public string EYE_COLOR { get; set; }
        public string SEX_3 { get; set; }
        public string RACE { get; set; }
        public string ETHNICITY { get; set; }
       
    }
}
