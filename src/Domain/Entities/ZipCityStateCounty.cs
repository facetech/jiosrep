﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{

    public class ZipCityStateCounty : AuditableEntity
    {
        [Key]
        public long? Zipcode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County { get; set; }

    }
}
