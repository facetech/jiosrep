﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{


    public class Race_types : AuditableEntity
    {
        [Key]
        public string Race { get; set; }
        public float Order_by { get; set; }
    } // class RACE_TYPES
}