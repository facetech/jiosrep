﻿using IJOS.Domain.Common;
using IJOS.Domain.ValueObjects;
using System;
using System.Collections.Generic;

namespace IJOS.Domain.Entities
{
    public class TodoList : AuditableEntity
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public DateTime Created { get; set; }        

        public Colour Colour { get; set; } = Colour.White;

        public IList<TodoItem> Items { get; private set; } = new List<TodoItem>();
    }
}
