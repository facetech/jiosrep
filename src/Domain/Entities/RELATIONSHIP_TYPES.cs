﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{


    public class Relationship_types : AuditableEntity
    {
        [Key]
        public string Relationship_to_offender { get; set; }
        public string Sex { get; set; }
        public string Includes { get; set; }
        public string Relationship_type { get; set; }
        public long? Order_by { get; set; }


    } // class RELATIONSHIP_TYPES
}