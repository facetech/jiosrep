﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Aliases : AuditableEntity
    {
        [Key]
        public long Alias_number { get; set; }
        public long Sin { get; set; }
        public string Last_name { get; set; }
        public string First_name { get; set; }
        public string Middle_name { get; set; }
        public DateTime? Date_of_birth { get; set; }
        public string Alias_type { get; set; }
        public string Agency_name { get; set; }
        public string Status { get; set; }


    } // class ALIASES


}

