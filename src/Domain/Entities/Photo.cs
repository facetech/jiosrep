﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Photo : AuditableEntity
    {
       
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Photo_id { get; set; }
        public byte[] Photo_data { get; set; }
        public int? Photo_type_id { get; set; }
        public DateTime? Date_taken { get; set; }
        public DateTime? Date_edited { get; set; }
        public string Location_taken { get; set; }
        public int? Sin { get; set; }
        public string Login_name { get; set; }

        public string Taken_by { get; set; }
        public string Comments { get; set; }
        public string Login_created { get; set; }
        public string Login_edited { get; set; }
        public byte[] Photo_data_thumbnail { get; set; }
        public bool? Setasschoolid { get; set; }
  
    } // class photo
}
