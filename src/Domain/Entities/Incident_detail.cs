﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Incident_detail : AuditableEntity
    {
        [Key]
        public long Incident_detail_key { get; set; }
        public long? Incident_number { get; set; }
        public long? Incident_type_id { get; set; }
        public long? Category_id { get; set; }
        public string Description_1 { get; set; }
        public string Description_2 { get; set; }
        public long? Staff_key { get; set; }
        public string Status { get; set; }

    } // class INCIDENT_DETAIL

}
