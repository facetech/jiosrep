﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class OffenderOverview : Offender 
    {
        public string STAFF_COUNTY_NAME { get; set; }
        public string STAFF_FIRST_NAME { get; set; }
        public string STAFF_LAST_NAME { get; set; }
        public string STAFF_JOB_TITLE { get; set; }
        public string STAFF_AGENCY_NAME { get; set; }


        public string SUPERVISION_LEVEL { get; set; }
        public string STREET_ADDRESS { get; set; }
        public string CITY { get; set; }
        public string STATE{ get; set; }
        public string ZIP{ get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
        public string UserName { get; set; }
        public string DRIVERS_LICENSE_NUMBER { get; set; }
        public string DRIVERS_LICENSE_STATE { get; set; }

        public string JPO_LAST_NAME { get; set; }
        public string JPO_FIRST_NAME { get; set; }
        public string JPO_AGENCY_NAME { get; set; }
        public string JPO_PHONE_NUMBER_2 { get; set; }

        public string JSC_LAST_NAME { get; set; }
        public string JSC_FIRST_NAME { get; set; }
        public string JSC_AGENCY_NAME { get; set; }
        public string JSC_PHONE_NUMBER_2 { get; set; }

        public string GL_LAST_NAME { get; set; }
        public string GL_FIRST_NAME { get; set; }
        public string GL_AGENCY_NAME { get; set; }
        public string GL_GROUP_NAME { get; set; }
        public string EMAIL_ADDRESS { get; set; }
    }
}
