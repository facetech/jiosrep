﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{

    public class Relationship_linkage : AuditableEntity
    {




        public string Relationship_to_offender { get; set; }
        public long Sin { get; set; }
        public long Relationship_id { get; set; }
    
        public long? Restitution_number { get; set; }
        public long? Family_number_4 { get; set; }
        public string Table_name { get; set; }
        public long? Victim_key { get; set; }
        public long? Case_key { get; set; }
        public string Status { get; set; }
        public string Is_guardian { get; set; }
        public string Parent_support_ordered { get; set; }
        public string Other_children_in_home { get; set; }
        public long? Second_sin { get; set; }
       


    } // class RELATIONSHIP_LINKAGE
    
   
}