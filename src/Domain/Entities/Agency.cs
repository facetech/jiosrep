﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{

    public class Agency : AuditableEntity
    {
        [Key]
        public string Agency_name { get; set; }
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone_number { get; set; }
        public string Fax_number { get; set; }
        public string Judicial_district { get; set; }
        public string Supervisor { get; set; }
        public string Intake_contact_name { get; set; }
        public string Victim_coordinator_name { get; set; }
        public string Emergency_phone_number { get; set; }
        public string Djc_e_mail { get; set; }
        public string County_name { get; set; }
        public string Agency_type { get; set; }
    } // class AGENCY
}
