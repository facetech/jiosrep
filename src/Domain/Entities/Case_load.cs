﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Case_load : AuditableEntity
    {
        [Key]
        public long CASE_LOAD_NUMBER { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string PROBATION_STATUS { get; set; }
        public System.DateTime DATE_OF_BIRTH { get; set; }
        public string SEX { get; set; }
        public string RACE { get; set; }
        public DateTime STARTING_DATE { get; set; }
        public DateTime? ENDING_DATE { get; set; }        
        public long STAFF_KEY { get; set; }
        public string AGENCY_BRANCH { get; set; }
        public string AGENCY_NAME { get; set; }
        public long SIN { get; set; }
    }
}