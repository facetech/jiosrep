﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{

      

        public class Scars_marks_tattoos : AuditableEntity
        {
        
            [Key]
            public long Smt_number { get; set; }
            public long? Sin { get; set; }
            public string Scar_mark_tattoo { get; set; }
            public string Description { get; set; }
            public long? Photo_id { get; set; }
                 } // class SCARS_MARKS_TATTOOS


    }

