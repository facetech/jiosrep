﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{

    public class Photo_types : AuditableEntity
    {
        [Key]
        public long Photo_type_id { get; set; }
        public string Photo_type { get; set; }
        public string Tab_category { get; set; }
        public long? Display_order { get; set; }
        
    } // class PHOTO_TYPES
}
