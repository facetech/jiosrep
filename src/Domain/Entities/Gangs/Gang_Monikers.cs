﻿using IJOS.Domain.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace IJOS.Domain.Entities
{
    public class Gang_Monikers : AuditableEntity
    {
        [Key]
        public long Moniker_number { get; set; }
        public long? Relationship_number { get; set; }
        public string Moniker { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    }
}
