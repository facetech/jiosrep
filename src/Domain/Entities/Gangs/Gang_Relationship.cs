﻿using IJOS.Domain.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace IJOS.Domain.Entities
{
    public class Gang_Relationship : AuditableEntity
    {
        [Key]
        public long Relationship_number { get; set; }
        public long Sin { get; set; }
        public long Gang_number { get; set; }
        public DateTime? Join_date { get; set; }
        public DateTime? Quit_date { get; set; }
        public string Status { get; set; }
        public string Colors { get; set; }
        public bool? Uses_gang_signs { get; set; }
        public string Initiated_by { get; set; }
        public string Rank { get; set; }
        public string Created_by { get; set; }
        public DateTime? Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
    }
}
