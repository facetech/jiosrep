﻿using System;
using IJOS.Domain.Common;
using System.ComponentModel.DataAnnotations;

namespace IJOS.Domain.Entities
{
    public class Gang : AuditableEntity
    {
        [Key]
        public long Gang_number { get; set; }
        public string Gang_name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County_name { get; set; }
        public string Location { get; set; }
        public string Gang_type { get; set; }
        public string Colors { get; set; }
        public string Is_active { get; set; }
        public string Last_user { get; set; }
        public DateTime? Last_update { get; set; }
    }
}
