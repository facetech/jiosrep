﻿using IJOS.Domain.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace IJOS.Domain.Entities 
{
    public class Known_Gangs : AuditableEntity
    {
        [Key]
        public long Gang_number { get; set; }
        public string Gang_name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Location { get; set; }
        public DateTime Created_date { get; set; }
        public DateTime? Modified_date { get; set; }
        public string Modified_by { get; set; }
        public string Created_by { get; set; }
    }
}
