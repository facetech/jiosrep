﻿using IJOS.Domain.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace IJOS.Domain.Entities
{
    public class GangOffender : AuditableEntity
    {
        //IJOS.Offender_Gang
        [Key]
        public long Offender_gang_id { get; set; }
        public long Sin { get; set; }
        public long Gang_number { get; set; }
        public DateTime? Join_date { get; set; }
        public DateTime? Quit_date { get; set; }
        public string Status { get; set; }
        public bool? Uses_gang_signs { get; set; }
        public string Initiated_by { get; set; }
        public string Rank { get; set; }
        public string Moniker { get; set; }

        //IJOS.Gang
        public string Gang_name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County_name { get; set; }
        public string Location { get; set; }
        public string Gang_type { get; set; }
        public string Colors { get; set; }
        public string Is_active { get; set; }
        public string Last_user { get; set; }
        public DateTime? Last_update { get; set; }
    }
}
