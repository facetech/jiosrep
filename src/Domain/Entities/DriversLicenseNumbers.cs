﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
   public class DriversLicenseNumbers : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Dln_id { get; set; }
        public long? Sin { get; set; }
        public string Drivers_license_number { get; set; }
        public string Drivers_license_state { get; set; }

    } // class DRIVERS_LICENSE_NUMBERS
}

