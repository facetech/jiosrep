﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{
    public class Ref_incident_type : AuditableEntity
    {
        [Key]
        public long Incident_type_id { get; set; }
        
        public long Type_to_Category_Id { get; set; }
        public string Incident_type { get; set; }
        public string Incident_description { get; set; }

    } // class REF_INCIDENT_TYPE

}
