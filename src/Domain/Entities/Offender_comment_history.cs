﻿using IJOS.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IJOS.Domain.Entities
{

    public class Offender_comment_history : AuditableEntity
    {
        [Key]
        public long Offender_comment_history_id { get; set; }
        public long? Offender_comment_id { get; set; }
        public long Sin { get; set; }
        public string Comments { get; set; }
        public string Status { get; set; }
     


    }


}
