﻿using IJOS.Domain.Common;
using System.ComponentModel.DataAnnotations;

namespace IJOS.Domain.Entities 
{
    public class Schools : AuditableEntity
    {
        //IJOS.Schools
        [Key]
        public long? School_key { get; set; }
        public string School_name { get; set; }
        public string School_type { get; set; }
        public string Address_line { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Contact_name { get; set; }
    }
}
